*** Variables ***

${XPATH_CLOSE_POPUP_EXPIRE}                                         //span[text()='×']

${XPATH_CLOSE_POPUP}                                                //div[@id="marketingcontentModal"]//div[@role="document"]//button[@class="close"]

${XPATH_REPORTMENU}                                                 //i[@id='reportmenu']

${XPATH_SELL_ORDER_MENU}                                            //i[@id='sellmenu']

${XPATH_SELL_ORDER_MENU_ADD_ORDER}                                  //li//a[@class][text()='สร้างรายการขาย']

${XPATH_SELL_ORDER_MENU_SELL_ORDER}                                 //li//a[@class][text()='ดูรายการขาย']

${XPATH_BUYMENU}                                                    //i[@id='buymenu']

${XPATH_PRODUCTMENU}                                                //i[@id='productmenu']

${XPATH_CUSTOMERMENU}                                               //i[@id='customermenu']

${XPATH_SALEPAGEMENU}                                               //i[@id='salepagemenu']

${XPATH_FINANCEMENU}                                                //i[@id='financemenu']

${XPATH_DOCUMENTMENU}                                               //i[@id='documentmenu']

${XPATH_SETTINGMENU}                                                //i[@id='settingmenu']

${XPATH_USERPROFILEMENU}                                            //li[@id='profiledropdown']

${XPATH_LOGOUTBUTTON}                                               //a[contains(text(),'Logout')]

${XPATH_SELL_ORDER_MENU}                                            //i[@id='sellmenu']

${XPATH_SELL_ORDER_MENU_ADD_ORDER}                                  //li//a[@class][text()='สร้างรายการขาย']