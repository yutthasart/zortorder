*** Variables ***
#SEARCHORDERDATE & LENGTH

${XPATH_FIELD_SEARCH_SELL_ORDER}                                     //input[@id='quicksearchtext']

${XPATH_DROPDOWN_DATE_SELL_ORDER}                                    //button[@class='button button-default button-sm mb-0 button-dropdown button-merge-right']

${XPATH_SAMPLE_DROPDOWN_DATE_SELL_ORDER_DATE}                        //a[@class='pl-2 pr-2'][text()='วันที่']

${XPATH_SAMPLE_DROPDOWN_DATE_SELL_ORDER_PAYMENT_DATE}                //a[contains(text(),'วันที่ชำระเงิน')]

${XPATH_SAMPLE_DROPDOWN_DATE_SELL_ORDER_SENT_DATE}                   //a[@class='pl-2 pr-2'][text()='วันส่งสินค้า']

${XPATH_SAMPLE_DROPDOWN_DATE_SELL_ORDER_SHIPPING_DATE}               //a[@class='pl-2 pr-2'][text()='วันที่โอนสินค้า']

${XPATH_DROPDOWN_DATE_LENGTH}                                        //span[@class='date-length-label']

${XPATH_SAMPLE_DROPDOWN_DATE_LENGTH_TODAY}                           //a[contains(text(),'วันนี้')]

${XPATH_SAMPLE_DROPDOWN_DATE_LENGTH_THIS_WEEK}                       //a[contains(text(),'สัปดาห์นี้')]

${XPATH_SAMPLE_DROPDOWN_DATE_LENGTH_THIS_MONTH}                      //a[contains(text(),'เดือนนี้')]

${XPATH_SAMPLE_DROPDOWN_DATE_LENGTH_PAST_ONE_MONTH}                  //a[contains(text(),'ย้อนหลัง 1 เดือน')]

${XPATH_SAMPLE_DROPDOWN_DATE_LENGTH_PAST_THREE_MONTH}                //a[contains(text(),'ย้อนหลัง 3 เดือน')]

${XPATH_FIELD_DATE_LENGTH_CUSTOM_START_DATE}                         //input[@id='customfromdate']

${XPATH_CALENDAR_CHOOSE_MONTH_LENGTH_CUSTOM_START_DATE}              //div[13]/div[3]/table[1]/thead[1]/tr[1]/th[@class='switch']

${XPATH_SAMPLE_CHOOSE_MONTH_LENGTH_CUSTOM_START_DATE}                //div[13]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/span[@class][text()='ก.พ.']

${XPATH_CALENDAR_CHOOSE_YEAR_LENGTH_CUSTOM_START_DATE}               //div[13]/div[4]/table[1]/thead[1]/tr[1]/th[@class='switch'] 

${XPATH_SAMPLE_CHOOSE_YEAR_LENGTH_CUSTOM_START_DATE}                 //div[13]/div[5]/table[1]/tbody[1]/tr[1]/td[1]/span[@class][text()='2565']

${XPATH_SAMPLE_CHOOSE_DATE_LENGTH_CUSTOM_START_DATE}                 //div[13]/div/table/tbody/tr/td[@class='day today active']

${XPATH_FIELD_DATE_LENGTH_CUSTOM_END_DATE}                           //input[@id='customtodate']

${XPATH_CALENDAR_CHOOSE_MONTH_LENGTH_CUSTOM_END_DATE}                //div[14]/div[3]/table[1]/thead[1]/tr[1]/th[@class='switch']

${XPATH_SAMPLE_CHOOSE_MONTH_LENGTH_CUSTOM_END_DATE}                  //div[14]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/span[@class][text()='ก.พ.']

${XPATH_CALENDAR_CHOOSE_YEAR_LENGTH_CUSTOM_END_DATE}                 //div[14]/div[4]/table[1]/thead[1]/tr[1]/th[@class='switch'] 

${XPATH_SAMPLE_CHOOSE_YEAR_LENGTH_CUSTOM_END_DATE}                   //div[14]/div[5]/table[1]/tbody[1]/tr[1]/td[1]/span[@class][text()='2565']

${XPATH_SAMPLE_CHOOSE_DATE_LENGTH_CUSTOM_END_DATE}                   //div[14]/div/table/tbody/tr/td[@class='day today active']

${XPATH_BUTTON_OK_DATE_LENGTH_CUSTOM}                                //li[7]/div[1]/button[1][text()='ตกลง']

#FILTER_SEARCH

${XPATH_BUTTON_OPEN_MODAL_FILTER_SEARCH_SELL_ORDER}                   //a[@href='#searchAdvance'][text()=' ตัวกรอง']

${XPATH_BUTTON_SEARCH_MODAL_FILTER_SEARCH_SELL_ORDER}                 //input[@onclick='advanceSearchNew();']

${XPATH_SWITCH_REMEMBER_FILTER_SEARCH_SELL_ORDER}                     //label[@for='remember_filter']

${XPATH_BUTTON_CLEAR_FILTER_SETTING_SEARCH_SELL_ORDER}                //a[contains(text(),'เคลียร์')]

${XPATH_CHECKBOX_WAIT_SHIPPING_FILTER_SEARCH_SELL_ORDER}              //div[2]/label[1]/span[1]/input[@name='status-new']

${XPATH_CHECKBOX_COMPLETE_SHIPPING_FILTER_SEARCH_SELL_ORDER}          //div[2]/label[2]/span[1]/input[@name='status-new']

${XPATH_CHECKBOX_PARTIAL_SHIPPING_FILTER_SEARCH_SELL_ORDER}           //div[2]/label[3]/span[1]/input[@name='status-new']

${XPATH_CHECKBOX_CANCEL_SHIPPING_FILTER_SEARCH_SELL_ORDER}            //div[2]/label[4]/span[1]/input[@name='status-new']

${XPATH_CHECKBOX_EXPIRE_SHIPPING_FILTER_SEARCH_SELL_ORDER}            //div[2]/label[5]/span[1]/input[@name='status-new']

${XPATH_CHECKBOX_WAIT_SENT_FILTER_SEARCH_SELL_ORDER}                  //div[3]/label[1]/span[1]/input[@name='status-new']

${XPATH_CHECKBOX_SENDING_FILTER_SEARCH_SELL_ORDER}                    //div[3]/label[2]/span[1]/input[@name='status-new']

${XPATH_CHECKBOX_CANCEL_SENT_FILTER_SEARCH_SELL_ORDER}                //div[3]/label[3]/span[1]/input[@name='status-new']

${XPATH_CHECKBOX_WAIT_PAY_FILTER_SEARCH_SELL_ORDER}                   //div[4]/label[1]/span[1]/input[@name='paymentstatus-new']

${XPATH_CHECKBOX_PARTIAL_PAY_FILTER_SEARCH_SELL_ORDER}                //div[4]/label[2]/span[1]/input[@name='paymentstatus-new']

${XPATH_CHECKBOX_COMPLETE_PAY_FILTER_SEARCH_SELL_ORDER}               //div[4]/label[3]/span[1]/input[@name='paymentstatus-new']

${XPATH_CHECKBOX_CANCEL_PAY_FILTER_SEARCH_SELL_ORDER}                 //div[4]/label[4]/span[1]/input[@name='paymentstatus-new']

${XPATH_DROPDOWN_WAREHOUSE_FILTER_SEARCH_SELL_ORDER}                  //button[@data-id='searchwarehouseid-new']

${XPATH_SAMPLE_WAREHOUSE_FROM_DROPDOWN_WAREHOUSE}                     //div[2]/div[1]/div[1]/div[1]/ul[1]/li[1]/a[1]/span[@class='text'][text()='คลัง / สาขา - ทั้งหมด']

${XPATH_SAMPLE_WAREHOUSE_FROM_DROPDOWN_WAREHOUSE2}                    //div[2]/div[1]/div[1]/div[1]/ul[1]/li[2]/a[1]/span[@class='text'][text()='คลังสินค้าหลัก']

${XPATH_FIELD_SEARCH_PRODUCT_FILTER_SEARCH_SELL_ORDER}                //input[@id='searchproduct-new']

${XPATH_FIELD_SEARCH_SERIAL_NUMBER_FILTER_SEARCH_SELL_ORDER}          //input[@id='searchserialno-new']

${XPATH_FIELD_SEARCH_PRICE_START_FILTER_SEARCH_SELL_ORDER}            //input[@id='fromamount-new']

${XPATH_FIELD_SEARCH_PRICE_END_FILTER_SEARCH_SELL_ORDER}              //input[@id='toamount-new']

${XPATH_CHECKBOX_SHOW_ONLY_COD_FILTER_SEARCH_SELL_ORDER}              //input[@id='iscod-advancesearch-new']

${XPATH_CHECKBOX_SHOW_HIDDEN_SO_FILTER_SEARCH_SELL_ORDER}             //input[@id='checkshowarchive-new']

${XPATH_FIELD_SEARCH_AGENT_NAME_FILTER_SEARCH_SELL_ORDER}             //input[@id='searchagentname-new']

${XPATH_DROPDOWN_SEARCH_USER_FILTER_SEARCH_SELL_ORDER}                //select[@id='searchcreateuserid-new']

${XPATH_SAMPLE_USER_FROM_DROPDOWN_SEARCH_AGENT}                       //option[@value='0'][text()='ทั้งหมด']

${XPATH_SAMPLE_USER_FROM_DROPDOWN_SEARCH_AGENT2}                      //option[@value='82530'][text()='Demo11']

#Order List

${XPATH_TAB_STATUS_ALL}                                               //a[@href='javascript:getTransaction(0)']

${XPATH_TAB_STATUS_WAIT_TO_SHIPPING}                                  //a[@href='javascript:getTransaction(1)']

${XPATH_TAB_STATUS_WAIT_TO_PAY}                                       //a[@href='javascript:getTransaction(2)']

${XPATH_TAB_STATUS_COMPLETE}                                          //a[@href='javascript:getTransaction(3)']

${XPATH_BUTTON_REFRESH_ORDER_LIST}                                    //button[@onclick='javascript: refreshTable(true);']

${XPATH_SORT_DATE_ASC}                                                //a[@href="/Sell/list?&page=1&mysort=transactiondate&sortdir=ASC"]

${XPATH_SORT_ORDER_NO_ASC}                                            //a[@href="/Sell/list?&page=1&mysort=number&sortdir=ASC"]

${XPATH_SORT_CUSTOMER_ASC}                                            //a[@href="/Sell/list?&page=1&mysort=customername&sortdir=ASC"]

${XPATH_SORT_SALE_CHANNEL_ASC}                                        //a[@href="/Sell/list?&page=1&mysort=saleschannel&sortdir=ASC"]

${XPATH_SORT_SHIPPING_DATE_ASC}                                       //a[@href="/Sell/list?&page=1&mysort=shippingdate&sortdir=ASC"]

${XPATH_SORT_AMOUNT_ASC}                                              //a[@href="/Sell/list?&page=1&mysort=amount&sortdir=ASC"]

${XPATH_SORT_STATUS_SHIPPING_ASC}                                     //a[@href="/Sell/list?&page=1&mysort=status&sortdir=ASC"]

${XPATH_SORT_STATUS_PAYMENT_ASC}                                      //a[@href="/Sell/list?&page=1&mysort=paymentstatus&sortdir=ASC"]

${XPATH_SORT_DATE_DESC}                                                //a[@href="/Sell/list?&page=1&mysort=transactiondate&sortdir=DESC"]

${XPATH_SORT_ORDER_NO_DESC}                                            //a[@href="/Sell/list?&page=1&mysort=number&sortdir=DESC"]

${XPATH_SORT_CUSTOMER_DESC}                                            //a[@href="/Sell/list?&page=1&mysort=customername&sortdir=DESC"]

${XPATH_SORT_SALE_CHANNEL_DESC}                                        //a[@href="/Sell/list?&page=1&mysort=saleschannel&sortdir=DESC"]

${XPATH_SORT_SHIPPING_DATE_DESC}                                       //a[@href="/Sell/list?&page=1&mysort=shippingdate&sortdir=DESC"]

${XPATH_SORT_AMOUNT_DESC}                                              //a[@href="/Sell/list?&page=1&mysort=amount&sortdir=DESC"]

${XPATH_SORT_STATUS_SHIPPING_DESC}                                     //a[@href="/Sell/list?&page=1&mysort=status&sortdir=DESC"]

${XPATH_SORT_STATUS_PAYMENT_DESC}                                      //a[@href="/Sell/list?&page=1&mysort=paymentstatus&sortdir=DESC"]

${XPATH_SAMPLE_SORT_DATE}                                              //div[2]/table[1]/tbody[1]/tr[1]/td[@class='date']

${XPATH_SAMPLE_SORT_ORDER_NO}                                          //div[2]/table[1]/tbody[1]/tr[1]/td[@class='name']

${XPATH_SAMPLE_SORT_CUSTOMER}                                          //div[2]/table[1]/tbody[1]/tr[1]/td[@class='customer']

${XPATH_SAMPLE_SORT_SALE_CHANNEL}                                      //div[2]/table[1]/tbody[1]/tr[1]/td[@class='channel']

${XPATH_SAMPLE_SORT_SHIPPING_DATE}                                     //div[2]/table[1]/tbody[1]/tr[1]/td[@class='shipping']

${XPATH_SAMPLE_SORT_AMOUNT}                                            //div[2]/table[1]/tbody[1]/tr[1]/td[@class='amount font-lato text-right']

${XPATH_SAMPLE_STATUS_SHIPPING_WAITING}                                //tr[1]/td[@class='status text-center']

${XPATH_SAMPLE_STATUS_SHIPPING_SUCCESS}                                //tr[2]/td[@class='status text-center']/span[1]

${XPATH_SAMPLE_STATUS_SHIPPING_VOID}                                   //tr[12]/td[@class='status text-center']/span[1]

${XPATH_SAMPLE_STATUS_PAYMENT}                                         //tr[1]/td[@class='payment text-center']

${XPATH_SAMPLE_STATUS_PAYMENT_VOID}                                    //tr[12]/td[@class='payment text-center']

${XPATH_SAMPLE_CHECK_BOX}                                              //tr[1]/td[2]/div[1]/label[1]/input[@class='checkboxrow mt-0']

${XPATH_SAMPLE_CHECK_BOX2}                                              //tr[2]/td[2]/div[1]/label[1]/input[@class='checkboxrow mt-0']

${XPATH_SAMPLE_CHECK_BOX3}                                              //tr[3]/td[2]/div[1]/label[1]/input[@class='checkboxrow mt-0']

${XPATH_SAMPLE_CHECK_BOX4}                                              //tr[4]/td[2]/div[1]/label[1]/input[@class='checkboxrow mt-0']

${XPATH_SAMPLE_CHECK_BOX5}                                              //tr[5]/td[2]/div[1]/label[1]/input[@class='checkboxrow mt-0']

${XPATH_SAMPLE_CHECK_BOX6}                                              //tr[6]/td[2]/div[1]/label[1]/input[@class='checkboxrow mt-0']

${XPATH_SAMPLE_CHECK_BOX7}                                              //tr[7]/td[2]/div[1]/label[1]/input[@class='checkboxrow mt-0']

${XPATH_SAMPLE_CHECK_BOX8}                                              //tr[8]/td[2]/div[1]/label[1]/input[@class='checkboxrow mt-0']

${XPATH_SAMPLE_CHECK_BOX9}                                              //tr[9]/td[2]/div[1]/label[1]/input[@class='checkboxrow mt-0']

${XPATH_SAMPLE_CHECK_BOX10}                                              //tr[10]/td[2]/div[1]/label[1]/input[@class='checkboxrow mt-0']

${XPATH_SAMPLE_CHECK_BOX11}                                              //tr[11]/td[2]/div[1]/label[1]/input[@class='checkboxrow mt-0']

${XPATH_SAMPLE_CHECK_BOX12}                                              //tr[12]/td[2]/div[1]/label[1]/input[@class='checkboxrow mt-0']

${XPATH_SAMPLE_CHECK_BOX13}                                              //tr[13]/td[2]/div[1]/label[1]/input[@class='checkboxrow mt-0']

${XPATH_SAMPLE_CHECK_BOX14}                                              //tr[14]/td[2]/div[1]/label[1]/input[@class='checkboxrow mt-0']

${XPATH_SAMPLE_TEXT_LINK_SO_NUMBER}                                      //tr[1]/td[4]/div[@class='sell-tooltip tooltip-hover']

${XPATH_SAMPLE_TEXT_LINK_SO_NUMBER2}                                     //tr[2]/td[4]/div[@class='sell-tooltip tooltip-hover']

${XPATH_SAMPLE_TEXT_LINK_SO_NUMBER3}                                     //tr[3]/td[4]/div[@class='sell-tooltip tooltip-hover']

${XPATH_SAMPLE_TEXT_LINK_SO_NUMBER14}                                    //tr[14]/td[4]/div[@class='sell-tooltip tooltip-hover']

${XPATH_SAMPLE_ICON_SHARE_LINK_SO_NUMBER}                                //tr[1]/td[4]/span[1]/span[1]/i[@class='fal fa-link']

${XPATH_SAMPLE_ICON_SHARE_LINK_SO_NUMBER2}                               //tr[2]/td[4]/span[1]/span[1]/i[@class='fal fa-link']

${XPATH_SAMPLE_ICON_SHARE_LINK_SO_NUMBER3}                               //tr[3]/td[4]/span[1]/span[1]/i[@class='fal fa-link']

${XPATH_SAMPLE_TEXT_LINK_CUSTOMER_NAME}                                  //tr[1]/td[5]/span[1]/a[@href]

${XPATH_SAMPLE_TEXT_LINK_CUSTOMER_NAME2}                                 //tr[2]/td[5]/span[1]/a[@href]

${XPATH_SAMPLE_TEXT_LINK_CUSTOMER_NAME3}                                 //tr[3]/td[5]/span[1]/a[@href]

${XPATH_SAMPLE_BUTTON_EDIT_SHIPPING_DATE}                                //tr[1]/td[7]/a[@href='javascript:void(0);']

${XPATH_SAMPLE_BUTTON_EDIT_SHIPPING_DATE2}                               //tr[2]/td[7]/a[@href='javascript:void(0);']

${XPATH_SAMPLE_BUTTON_EDIT_SHIPPING_DATE3}                               //tr[3]/td[7]/a[@href='javascript:void(0);']

${XPATH_SAMPLE_TEXT_LINK_EDIT_TRANFER}                                   //tr[1]/td[9]/a/u

${XPATH_SAMPLE_TEXT_LINK_EDIT_TRANFER2}                                  //tr[2]/td[9]/a/u

${XPATH_SAMPLE_TEXT_LINK_EDIT_PAYMENT}                                   //tr[1]/td[10]/span/a/u

${XPATH_SAMPLE_TEXT_LINK_EDIT_PAYMENT2}                                  //tr[2]/td[10]/span/a/u

${XPATH_BUTTON_COUNT_CHOOSE_SO}                                        //span[@id='checkboxrecordareacount']

${XPATH_TEXTLINK_DELETE_CHOOSE_SO}                                     //a[contains(text(),'ล้างข้อมูล')]

${XPATH_TEXT_CHOOSE_SO}                                                //ol[@class='p1-4 pt-2']

${XPATH_BUTTON_PRINT}                                                  //span[contains(text(),'พิมพ์เอกสาร')]

${XPATH_BUTTON_PRINT_DOCUMENT}                                         //a[@href='javascript:openMultiPDF();']

${XPATH_RADIO_BUTTON_PRINT_DOCUMENT_SIZE_A4}                           //input[@id='multisizepaper0']

${XPATH_RADIO_BUTTON_PRINT_DOCUMENT_SIZE_A5}                           //input[@id='multisizepaper2']

${XPATH_RADIO_BUTTON_PRINT_DOCUMENT_SIZE_SLIP}                         //input[@id='multisizepaper1']

${XPATH_DROPDOWN_CHOOSE_FORMAT_PAPER}                                  //select[@id='multiformatpaper']

${XPATH_CHECKBOX_PRINT_ORIGINAL_DOCUMENT}                              //input[@id='multi-printdocument-original']

${XPATH_CHECKBOX_PRINT_COPY_DOCUMENT}                                  //input[@id='multi-printdocument-copy']

${XPATH_CHECKBOX_CUSTOM_NUMBER_DOCUMENT}                               //input[@id='adjusthavetransactionnumbermulti']

${XPATH_TEXTLINK_EDIT_PREFIX_DOCUMENT}                                 //a[@href='javascript:openAdjustDocument(1);']

${XPATH_SWITCH_REFER_DATE}                                             //label[@class='custom-checkbox'][@for='chkAdjustTransactionNumberIsTransactionDateMultiple']

${XPATH_CALENDAR_FOR_CHOOSE_DATE_PRINT_DOCUMENT}                       //input[@id='adjusttransactiondatemulti-66147807']

${XPATH_BUTTON_ACTION_PRINT_DOCUMENT}                                  //button[@onclick='doDownloadMultiPdf();']

${XPATH_BUTTON_CLOSE_PRINT_DOCUMENT_MODAL}                             //div[7]/div[1]/div[1]/div[1]/button[1]/i[@class='fad fa-times-circle']

${XPATH_BUTTON_PRINT_LETTER_BOX_LABEL}                                 //a[@href='javascript:showMultiLetter();']

${XPATH_RADIO_BUTTON_PRINT_LETTER_BOX_LABEL_SIZE_A5}                   //input[@id='multisizeletter0']

${XPATH_RADIO_BUTTON_PRINT_LETTER_BOX_LABEL_SIZE_A4}                   //input[@id='multisizeletter1']

${XPATH_RADIO_BUTTON_PRINT_LETTER_BOX_LABEL_SIZE_LETTER}               //input[@id='multisizeletter2']

${XPATH_RADIO_BUTTON_PRINT_LETTER_BOX_LABEL_SIZE_STICKER}              //input[@id='multisizeletter4']

${XPATH_RADIO_BUTTON_PRINT_LETTER_BOX_LABEL_SIZE_BOX_LABEL}            //input[@id='multisizeletter3']

${XPATH_BUTTON_ACTION_PRINT_LETTER_BOX_LABEL}                          //button[@onclick='doDownloadMultiLetter()']

${XPATH_BUTTON_CLOSE_PRINT_LETTER_BOX_LABEL_MODAL}                     //div[19]/div[1]/div[1]/div[1]/button[1]/i[@class='fad fa-times-circle']

${XPATH_BUTTON_PRINT_PREPARATION}                                      //a[@href='javascript:doDownloadPickingPdf();']

${XPATH_BUTTON_CALL_SHIPPING}                                          //span[contains(text(),'บริการส่งสินค้า')]

${XPATH_BUTTON_CALL_SHIPPING_GROUP}                                    //a[@href='javascript:openLogisticsGroup();'] 

${XPATH_BUTTON_CLOSE_SHIPPING_GROUP_MODAL}                             //*[@id="newUI"]/div/div/div[1]/button/i

${XPATH_BUTTON_CALL_SHIPPING_SEPARATE}                                 //a[@href='javascript:openLogisticsSeparate();'] 

${XPATH_BUTTON_CLOSE_SHIPPING_SEPARATE_MODAL}                          //*[@id="newUI"]/div/div/div[1]/button/i

${XPATH_BUTTON_EXPORT_EXCEL_KERRY_EASY_SHIP}                           //a[@href='javascript:getKerry(0,0);']

${XPATH_TEXT_EXPORT_EXCEL_KERRY_EASY_SHIP}                             //span[contains(text(),'kerryEasyship')]

${XPATH_BUTTON_EXPORT_EXCEL_KERRY_EASY_SHIP_COD}                       //a[@href='javascript:getKerry(0,1);']

${XPATH_TEXT_EXPORT_EXCEL_KERRY_EASY_SHIP_COD}                         //span[contains(text(),'kerryEasyshipCod')]

${XPATH_BUTTON_EXPORT_EXCEL_KERRY_BUSINESS}                            //a[@href='javascript:getKerry(1,0);']

${XPATH_TEXT_EXPORT_EXCEL_KERRY_BUSINESS}                              //span[contains(text(),'kerryBusiness')]

${XPATH_BUTTON_EXPORT_EXCEL_KERRY_BUSINESS_COD}                        //a[@href='javascript:getKerry(1,1);']

${XPATH_TEXT_EXPORT_EXCEL_KERRY_BUSINESS_COD}                          //span[contains(text(),'kerryBusinessCod')]

${XPATH_BUTTON_USE_COMMAND}                                            //span[contains(text(),'คำสั่ง')]

${XPATH_BUTTON_COMMAND_PIN_FAV_SO}                                     //a[@href='javascript:doMultiFavTransaction();']   

${XPATH_TEXTBOX_CONFIRM_PIN_FAV_SO}                                    //input[@id='confirmdeletetext']

${XPATH_BUTTON_CONFIRM_PIN_FAV_SO}                                     //button[@onclick='confirmDeletePopup();']

${XPATH_SAMPLE_IMG_FAV_SO}                                             //tr[1]/td[4]/img[@src='/Content/pics/staricon.png'] 

${XPATH_BUTTON_COMMAND_EDIT_SHIPPING}                                  //a[@href="javascript:openeditshipping(1,'','',0);"]

${XPATH_BUTTON_CLOSE_COMMAND_EDIT_SHIPPING}                            //*[@id="shippingModal"]/div/div/div[1]/button/i

${XPATH_BUTTON_COMMAND_COMPLETE_SHIPPING}                              //a[@href='javascript:openWarehouseMultiPopup();'] 

${XPATH_BUTTON_CLOSE_COMMAND_COMPLETE_SHIPPING}                        //*[@id="multitransferModal"]/div/div/div[1]/button/i

${XPATH_BUTTON_COMMAND_COMPLETE_PAYMENT}                               //a[@href='javascript:openAddMultiPayment();'] 

${XPATH_BUTTON_CLOSE_COMMAND_COMPLETE_PAYMENT}                         //*[@id="multipaymentModal"]/div/div/div[1]/button/i

${XPATH_BUTTON_COMMAND_MERGE_SO}                                       //a[@href='javascript:doMergeTransaction();']

${XPATH_TEXTBOX_CONFIRM_MERGE_SO}                                      //input[@id='confirmdeletetext']

${XPATH_BUTTON_CONFIRM_MERGE_SO}                                       //button[@onclick='confirmDeletePopup();']

${XPATH_BUTTON_CLOSE_ERROR_MERGE_SO}                                   //div[@id='errorModal']/div[1]/div[1]/div[1]/button[1]/i[@class='fad fa-times-circle']

${XPATH_BUTTON_CLOSE_COMMAND_MERGE_SO}                                 //*[@id="confirmdeleteModal"]/div/div/div[1]/button/i

${XPATH_BUTTON_CLOSE_SUCCESS_MERGE_SO}                                 //*[@id="successModal"]/div/div/div[1]/button/i

${XPATH_BUTTON_COMMAND_HIDE_SO}                                        //a[@href='javascript:doMultiArchiveTransaction();']

${XPATH_TEXTBOX_CONFIRM_HIDE_SO}                                       //input[@id='confirmdeletetext']

${XPATH_BUTTON_CONFIRM_HIDE_SO}                                        //button[@onclick='confirmDeletePopup();']

${XPATH_BUTTON_CLOSE_SUCCESS_HIDE_SO}                                  //*[@id="successModal"]/div/div/div[1]/button/i

${XPATH_BUTTON_COMMAND_DELETE_SO}                                      //a[@href='javascript:doMultiDeleteTransaction();'] 

${XPATH_TEXTBOX_CONFIRM_DELETE_SO}                                     //input[@id='confirmdeletetext']

${XPATH_BUTTON_CONFIRM_DELETE_SO}                                      //button[@onclick='confirmDeletePopup();']

${XPATH_BUTTON_CLOSE_SUCCESS_DELETE_SO}                                //*[@id="successModal"]/div/div/div[1]/button/i

${XPATH_BUTTON_COMMAND_CANCEL_SO}                                      //a[@href='javascript:doMultiVoidTransaction();']

${XPATH_TEXTBOX_CONFIRM_CANCEL_SO}                                     //input[@id='confirmdeletetext']

${XPATH_BUTTON_CONFIRM_CANCEL_SO}                                      //button[@onclick='confirmDeletePopup();']

${XPATH_BUTTON_CLOSE_SUCCESS_CANCEL_SO}                                //*[@id="successModal"]/div/div/div[1]/button/i

${XPATH_BUTTON_CLOSE_EDIT_SHIPPING_DATE_MODAL}                         //*[@id="shippingModal"]/div/div/div[1]/button/i

${XPATH_BUTTON_CLOSE_EDIT_TRANFER_MODAL}                               //*[@id="transferModal"]/div/div/div[1]/button/i

${XPATH_BUTTON_CLOSE_EDIT_PAYMENT_MODAL}                               //*[@id="paymentModal"]/div/div/div[1]/button/i

#KEBAB MENU

${XPATH_SAMPLE_BUTTON_KEBAB_MENU}                                     //tr[1]/td[11]/div[contains(@class,'btn-etc')]/i

${XPATH_BUTTON_PIN_FAV_SO_IN_KEBAB_MENU}                              //tr[1]/td[11]/div/ul/li/a[contains(@href,'javascript:favTransaction')]  

${XPATH_BUTTON_UNPIN_FAV_SO_IN_KEBAB_MENU}                            //tr[1]/td[11]/div/ul/li/a[contains(@href,'javascript:unfavTransaction')]

${XPATH_BUTTON_COPY_NEW_SO_IN_KEBAB_MENU}                             //tr[1]/td[11]/div/ul/li/a[contains(@href,'/Sell/Add?&cpid')]

${XPATH_BUTTON_EDIT_SO_IN_KEBAB_MENU}                                 //tr[1]/td[11]/div/ul/li/a[contains(@href,'/Sell/Edit?&tid')]

${XPATH_BUTTON_EDIT_SHIPMENT_IN_KEBAB_MENU}                           //tr[1]/td[11]/div/ul/li/a[contains(@href,'javascript:void')]

${XPATH_BUTTON_CLOSE_EDIT_SHIPMENT_IN_KEBAB_MENU}                     //*[@id="shippingModal"]/div/div/div[1]/button/i

${XPATH_BUTTON_PRINT_IN_KEBAB_MENU}                                   //tr[1]/td[11]/div/ul/li/a[contains(@href,'javascript:openPDF2')]

${XPATH_BUTTON_CLOSE_PRINT_IN_KEBAB_MENU}                             //*[@id="pdfModal"]/div/div/div[1]/button/i

${XPATH_BUTTON_PRINT_SHIPPING_LABEL_IN_KEBAB_MENU}                    //tr[1]/td[11]/div/ul/li/a[contains(@href,'javascript:openLetter2')]

${XPATH_BUTTON_CLOSE_PRINT_SHIPPING_LABEL_IN_KEBAB_MENU}              //*[@id="letterModal"]/div/div/div[1]/button/i

${XPATH_BUTTON_PACK_IN_KEBAB_MENU}                                    //tr[1]/td[11]/div/ul/li/a[contains(@href,'/Pickandpack/Details?&tid')]

${XPATH_BUTTON_SEND_SMS_ORDER_CONFIRM_IN_KEBAB_MENU}                  //tr[1]/td[11]/div/ul/li/a[contains(@href,'javascript:openSMSmodal("confirmpurchase"')]

${XPATH_BUTTON_CLOSE_SEND_SMS_ORDER_CONFIRM_IN_KEBAB_MENU}            //*[@id="errorModal"]/div/div/div[1]/button/i

${XPATH_BUTTON_SEND_SMS_SHIPPING_CONFIRM_IN_KEBAB_MENU}               //tr[1]/td[11]/div/ul/li/a[contains(@href,'javascript:openSMSmodal("transaction"')]

${XPATH_BUTTON_CLOSE_SEND_SMS_SHIPPING_CONFIRM_IN_KEBAB_MENU}         //*[@id="errorModal"]/div/div/div[1]/button/i

${XPATH_BUTTON_HIDE_SO_IN_KEBAB_MENU}                                 //tr[1]/td[11]/div/ul/li/a[contains(@href,'javascript:doArchiveTransaction')]

${XPATH_BUTTON_CONFIRM_HIDE_SO_IN_KEBAB_MENU}                         //div[36]/div/div/div[3]/div/button[@onclick='confirmPopup();']

${XPATH_BUTTON_UNHIDE_SO_IN_KEBAB_MENU}                               //tr[1]/td[11]/div/ul/li/a[contains(@href,'javascript:doUnarchiveTransaction')]

${XPATH_BUTTON_CONFIRM_UNHIDE_SO_IN_KEBAB_MENU}                       //div[36]/div/div/div[3]/div/button[@onclick='confirmPopup();']

${XPATH_BUTTON_VOID_IN_KEBAB_MENU}                                    //tr[1]/td[11]/div/ul/li/a[contains(@href,'javascript:doVoidTransaction')]

${XPATH_BUTTON_CONFIRM_VOID_SO_IN_KEBAB_MENU}                         //div[36]/div/div/div[3]/div/button[@onclick='confirmPopup();']

${XPATH_BUTTON_DELETE_IN_KEBAB_MENU}                                  //tr[1]/td[11]/div/ul/li/a[contains(@href,'javascript:doDeleteTransaction')]

${XPATH_BUTTON_CONFIRM_DELETE_SO_IN_KEBAB_MENU}                       //div[36]/div/div/div[3]/div/button[@onclick='confirmPopup();']

${XPATH_SAMPLE_BUTTON_KEBAB_MENU2}                                    //tr[2]/td[11]/div[contains(@class,'btn-etc')]/i

${XPATH_BUTTON_DELETE_IN_KEBAB_MENU2}                                 //tr[2]/td[11]/div/ul/li/a[contains(@href,'javascript:doDeleteTransaction')]

${XPATH_SAMPLE_BUTTON_KEBAB_MENU3}                                    //tr[3]/td[11]/div[contains(@class,'btn-etc')]/i

${XPATH_BUTTON_DELETE_IN_KEBAB_MENU3}                                 //tr[3]/td[11]/div/ul/li/a[contains(@href,'javascript:doDeleteTransaction')]

${XPATH_SAMPLE_BUTTON_KEBAB_MENU12}                                   //tr[12]/td[11]/div[contains(@class,'btn-etc')]/i

${XPATH_BUTTON_VOID_IN_KEBAB_MENU12}                                  //tr[12]/td[11]/div/ul/li/a[contains(@href,'javascript:doVoidTransaction')]

${XPATH_SAMPLE_BUTTON_KEBAB_MENU14}                                   //tr[14]/td[11]/div[contains(@class,'btn-etc')]/i

${XPATH_BUTTON_UNHIDE_SO_IN_KEBAB_MENU14}                             //tr[14]/td[11]/div/ul/li/a[contains(@href,'javascript:doUnarchiveTransaction')]



