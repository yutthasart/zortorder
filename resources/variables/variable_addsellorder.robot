*** Variables ***
#DATAXPATH

${XPATH_FIELD_SO_NUMBER}                                             //input[@id='number']

${XPATH_FIELD_DATE}                                                  //input[@id='transactiondate']

${XPATH_SAMPLE_DATE}                                                 //tr//td[@class='day'][text()='25']

${XPATH_REMOVE_DATE}                                                 //div[3]/div[1]/div[1]/div[1]//span[1]//span[@class='glyphicon glyphicon-remove']

${XPATH_FIELD_EXPIRE_DATE}                                           //input[@id='expiredate']

${XPATH_REMOVE_EXPIRE_DATE}                                          //div[4]/div[1]/div[1]/div[1]/div[1]//span[1]//span[@class='glyphicon glyphicon-remove']

${XPATH_FIELD_REF}                                                   //input[@id='refname']

${XPATH_REMOVE_REF}                                                  //div[5]/div[1]/div[1]/div[1]/div[1]//span[@class='typeahead__cancel-button']

${XPATH_FIELD_SALE_CHANNEL}                                          //input[@id='saleschannel']

${XPATH_SAMPLE_SALE_CHANNEL}                                         //span[@class='name'][text()='Facebook']

${XPATH_REMOVE_SALE_CHANNEL}                                         //div[6]/div[1]/div[1]/div[1]/div[1]/span[1]/span[@class='typeahead__cancel-button']

${XPATH_DROPDOWN_VATTYPE}                                            //select[@id='vattypeid']  

${XPATH_SAMPLE_VATTYPE_1}                                            //option[@value='1:0:1']

${XPATH_SAMPLE_VATTYPE_2}                                            //option[@value='2:7:0']

${XPATH_SAMPLE_VATTYPE_3}                                            //option[@value='3:7:1']

${XPATH_TEXTLINK_AGENT}                                              //span[@id='agentzone']//a

${XPATH_FIELD_SEARCH_AGENT}                                          //input[@id='quicksearchagenttext']

${XPATH_BUTTON_ADD_AGENT}                                            //a[@class='button button-primary rounded'][text()='เพิ่มตัวแทนจำหน่าย']

${XPATH_FIELD_NAME_AGENT}                                            //input[@id='tmpagentname']

${XPATH_FIELD_TEL_AGENT}                                             //input[@id='tmpagentphone']      

${XPATH_FIELD_EMAIL_AGENT}                                           //input[@id='tmpagentemail']

${XPATH_FIELD_ADDRESS_AGENT}                                         //textarea[@id='tmpagentaddress']

${XPATH_BUTTON_OK_AGENT}                                             //button[@onclick='insertAgent();'][text()='ตกลง']

${XPATH_SAMPLE_CHOOSE_AGENT}                                         //tr[1]//td[4]//a[@class='button button-default button-sm'][text()='เลือก']

${XPATH_BUTTON_CLOSE_MODAL_AGENT}                                    //div[@id='marketplace-connect-modal']//button[@class='close']

${XPATH_TEXTLINK_DELETE_AGENT}                                       //a[@href='javascript:removeAgent();'][text()='ลบ']

${XPATH_BUTTON_OK_ERROR_DATA_AGENT}                                  //button[@onclick='closeErrorPopup();']  

${XPATH_BUTTON_CLOSE_MODAL_ADD_AGENT}                                //section/div[10]/div[1]/div[1]/div[1]//button[@type='button']

#CUSTOMERXPATH

${XPATH_FIELD_CUSTOMER_NAME}                                                   //input[@id='customername']

${XPATH_TEXT_SAMPLE_CUSTOMER_NAME}                                             //span[text()='C0002 : ข้อมูลตัวอย่าง 2']

${XPATH_BUTTON_CUSTOMER_CHOOSE_CUSTOMER}                                       //span[@class='typeahead__button']//a[@class='button button-link button-md']

${XPATH_FIELD_SEARCH_CUSTOMER_IN_CHOSSE_CUSTOMER_MODAL}                        //input[@id='quicksearchcontacttext']

${XPATH_TAB_ALL_IN_CHOOSE_CUSTOMER_MODAL}                                      //a[text()='ทั้งหมด']

${XPATH_TAB_CUSTOMER_IN_CHOOSE_CUSTOMER_MODAL}                                 //a[@href="javascript:getContact(1)"]

${XPATH_ICON_FACEBOOK_IN_CHOOSE_CUSTOMER_MODAL}                                //tr[1]/td[1]/i[@class='fab fa-facebook-square mr-1 bg-fb']

${XPATH_ICON_LINE_IN_CHOOSE_CUSTOMER_MODAL}                                    //tr[1]/td[1]/i[@class='fab fa-line mr-1 bg-line']

${XPATH_BUTTON_CUSTOMER_CHOOSE_SAMPLE_CUSTOMER}                                //tr[1]//td[@class='text-right vertical-align']//a[text()='เลือก']

${XPATH_FIELD_CUSTOMER_CODE}                                                   //input[@id='customercode']

${XPATH_FIELD_CUSTOMER_PHONE}                                                  //input[@id='customerphone']

${XPATH_FIELD_CUSTOMER_EMAIL}                                                  //input[@id='customeremail']

${XPATH_FIELD_CUSTOMER_ADDRESS}                                                //textarea[@id='customeraddress']

${XPATH_CHECKBOX_CUSTOMER_MERCHANT}                                               //input[@id='tmpmerchantstatus']

${XPATH_FIELD_CUSTOMER_ID_NUMBER}                                              //input[@id='customeridnumber']

${XPATH_FIELD_CUSTOMER_BRANCH_NAME}                                            //input[@id='customerbranchname']

${XPATH_FIELD_CUSTOMER_BRANCH_NO}                                              //input[@id='customerbranchno']

#PRODUCTXPATH

${XPATH_ฺBUTTON_MULTICHOOSE_PRODUCT}                                            //div//a[text()='เลือกสินค้า']

${XPATH_ฺFIELD_SEARCH_MULTIPRODUCT}                                             //input[@id='quicksearchMultiproducttext']

${XPATH_BUTTON_CHOOSE_MULTIPRODUCT}                                            //button[@class='button button-primary button-md']

${XPATH_TAB_PRODUCT_FOR_CHOOSE_MULTIPRODUCT}                                   //a[@href='javascript:gotoMultiProductPage(1, 0)']

${XPATH_TAB_BUNDLE_FOR_CHOOSE_MULTIPRODUCT}                                    //a[@href='javascript:gotoMultiProductPage(1, 1)']

${XPATH_CHECKBOX_FOR_CHOOSE_ALL_MULTIPRODUCT}                                  //input[@id='allcheck']

${XPATH_CHECKBOX_FOR_CHOOSE_MULTIPRODUCT_ROW_1}                                //tr[1]/td[1]/div[1]/label[1]/span[1]/input[@class='checkboxrow']

${XPATH_PICTURE_MULTIPRODUCT_ROW_1}                                            //tr[1]/td[2]/div[1]/div[1]/div[1]/div[@style='background:url(https://secure.zortout.com/Content/themes/base/images/defaultproduct-img.png) center;']

${XPATH_TEXTLINK_REMAIN_PRODUCT_IN_MULTI_CHOOSE_PRODUCT_MODAL}                 //tr[1]//td[3]/a[1]/u[1]/span[1]/u[1]/span[@class='green-400 fw-600 font-lato']

${XPATH_TEXTLINK_READY_FOR_SALE_PRODUCT_IN_MULTI_CHOOSE_PRODUCT_MODAL}         //tr[1]/td[4]/a[1]/u[1]/span[1]/span[@class='green-400 fw-600 font-lato']

${XPATH_BUTTON_CLOSE_REMAIN_AND_READY_FOR_SALE_PRODUCT_IN_MULTI_CHOOSE_PRODUCT_MODAL}             //section[1]/div[11]/div[1]/div[1]/div[1]/button[@class='close']

${XPATH_BUTTON_CHOOSE_PRODUCT}                                                 //tr[@id='prow1']//a[text()='เลือก']

${XPATH_BUTTON_CHOOSE_PRODUCT2}                                                 //tr[@id='prow2']//a[text()='เลือก']

${XPATH_BUTTON_CHOOSE_PRODUCT3}                                                 //tr[@id='prow3']//a[text()='เลือก']

${XPATH_BUTTON_CHOOSE_PRODUCT4}                                                 //tr[@id='prow4']//a[text()='เลือก']

${XPATH_BUTTON_CHOOSE_PRODUCT5}                                                 //tr[@id='prow5']//a[text()='เลือก']

${XPATH_ฺFIELD_SEARCH_PRODUCT}                                                  //input[@id='quicksearchproducttext']

${XPATH_PICTURE_PRODUCT_ROW_1}                                                 //tr[1]/td[1]/div[1]/div[1]/div[1]/div[@style='background:url(https://secure.zortout.com/Content/themes/base/images/defaultproduct-img.png) center;']

${XPATH_BUTTON_CHOOSE_PRODUCT_ROW_1}                                           //tr[1]/td[5]/a[@class='button button-default button-sm button-sm-nowrap']

${XPATH_BUTTON_CHOOSE_PRODUCT_ROW_2}                                           //tr[2]/td[5]/a[@class='button button-default button-sm button-sm-nowrap']

${XPATH_BUTTON_CHOOSE_PRODUCT_ROW_3}                                           //tr[3]/td[5]/a[@class='button button-default button-sm button-sm-nowrap']

${XPATH_BUTTON_CHOOSE_PRODUCT_ROW_4}                                           //tr[4]/td[5]/a[@class='button button-default button-sm button-sm-nowrap']

${XPATH_BUTTON_CHOOSE_PRODUCT_ROW_5}                                           //tr[5]/td[5]/a[@class='button button-default button-sm button-sm-nowrap']

${XPATH_TAB_PRODUCT_FOR_CHOOSE_PRODUCT}                                        //a[@href='javascript:gotoProductPage(1, 0)']

${XPATH_TAB_BUNDLE_FOR_CHOOSE_PRODUCT}                                         //a[@href='javascript:gotoProductPage(1, 1)']

${XPATH_TEXTLINK_REMAIN_PRODUCT_IN_CHOOSE_PRODUCT_MODAL}                       //tr[1]/td[2]/a[1]/u[1]/span[1]/span[@class='green-400 fw-600 font-lato']

${XPATH_TEXTLINK_READY_FOR_SALE_PRODUCT_IN_CHOOSE_PRODUCT_MODAL}               //tr[1]/td[3]/a[1]/u[1]/span[1]/span[@class='green-400 fw-600 font-lato']

${XPATH_BUTTON_CLOSE_REMAIN_AND_READY_FOR_SALE_PRODUCT_IN_CHOOSE_PRODUCT_MODAL}             //section[1]/div[11]/div[1]/div[1]/div[1]/button[@class='close']

${XPATH_ELEMENT_SAMPLE_ROW_2_IN_CHOOSE_PRODUCT_MODAL}                          //div[3]/div[1]/div[1]/div[2]/div[1]/div[3]/div[2]/table[1]/tbody[1]/tr[2]/td[@class='col-sm-6']

${XPATH_FIELD_PRODUCT_CODE}                                                    //input[@id='productcode1']

${XPATH_FIELD_PRODUCT_CODE2}                                                   //input[@id='productcode2']

${XPATH_FIELD_PRODUCT_CODE3}                                                   //input[@id='productcode3']

${XPATH_FIELD_PRODUCT_CODE4}                                                   //input[@id='productcode4']

${XPATH_FIELD_PRODUCT_CODE5}                                                   //input[@id='productcode5']

${XPATH_SAMPLE_CODE_IN_DROPDOWN_AUTO_SEARCH}                                   //span[contains(text(),'P0003 : ข้อมูลตัวอย่าง 2')]

${XPATH_SAMPLE_PRODUCT_NAME_IN_DROPDOWN_AUTO_SEARCH}                           //td[3]/div[1]/div[2]/ul[1]/li[7]/a[1]/span[1]/span[contains(text(),'P0003 : ข้อมูลตัวอย่าง 2')]

${XPATH_FIELD_PRODUCT_NAME}                                                    //input[@id='productname1']

${XPATH_FIELD_PRODUCT_NAME2}                                                    //input[@id='productname2']

${XPATH_FIELD_PRODUCT_NAME3}                                                    //input[@id='productname3']

${XPATH_FIELD_PRODUCT_NAME4}                                                    //input[@id='productname4']

${XPATH_FIELD_PRODUCT_NAME5}                                                    //input[@id='productname5']

${XPATH_FIELD_PRODUCT_QUANTITY}                                                //input[@id='productnumber1']

${XPATH_FIELD_PRODUCT_QUANTITY2}                                                //input[@id='productnumber2']

${XPATH_FIELD_PRODUCT_QUANTITY3}                                                //input[@id='productnumber3']

${XPATH_FIELD_PRODUCT_QUANTITY4}                                                //input[@id='productnumber4']

${XPATH_FIELD_PRODUCT_QUANTITY5}                                                //input[@id='productnumber5']

${XPATH_FIELD_PRODUCT_PRICE}                                                   //input[@id='productpricepernumber1']

${XPATH_FIELD_PRODUCT_PRICE2}                                                   //input[@id='productpricepernumber2']

${XPATH_FIELD_PRODUCT_PRICE3}                                                   //input[@id='productpricepernumber3']

${XPATH_FIELD_PRODUCT_PRICE4}                                                   //input[@id='productpricepernumber4']

${XPATH_FIELD_PRODUCT_PRICE5}                                                   //input[@id='productpricepernumber5']

${XPATH_FIELD_PRODUCT_DISCOUNT}                                                 //input[@id='discountpernumber1']

${XPATH_FIELD_PRODUCT_DISCOUNT2}                                                //input[@id='discountpernumber2']

${XPATH_FIELD_PRODUCT_DISCOUNT3}                                                //input[@id='discountpernumber3']

${XPATH_FIELD_PRODUCT_DISCOUNT4}                                                //input[@id='discountpernumber4']

${XPATH_FIELD_PRODUCT_DISCOUNT5}                                                //input[@id='discountpernumber5']

${XPATH_TEXT_TOTAL_PRICE}                                                       //p[@id='totalprice1']

${XPATH_TEXT_TOTAL_PRICE2}                                                       //p[@id='totalprice2']

${XPATH_TEXT_TOTAL_PRICE3}                                                       //p[@id='totalprice3']

${XPATH_TEXT_TOTAL_PRICE4}                                                       //p[@id='totalprice4']

${XPATH_TEXT_TOTAL_PRICE5}                                                       //p[@id='totalprice5']

${XPATH_BUTTON_DELETE_PRODUCT}                                                  //a[@href='javascript:deleteRow(1);']

${XPATH_BUTTON_DELETE_PRODUCT2}                                                 //a[@href='javascript:deleteRow(2);']

${XPATH_BUTTON_DELETE_PRODUCT3}                                                 //a[@href='javascript:deleteRow(3);']

${XPATH_BUTTON_DELETE_PRODUCT4}                                                 //a[@href='javascript:deleteRow(4);']

${XPATH_BUTTON_DELETE_PRODUCT5}                                                 //a[@href='javascript:deleteRow(5);']

${XPATH_BUTTON_ADD_PRODUCT}                                                    //span[text()='เพิ่มสินค้า']

${XPATH_FIELD_SHIPPING_CHANNEL}                                                //input[@id='shippingchannel']

${XPATH_SAMPLE_CHOOSE_SHIPPING}                                                //div[1]/div[1]/div[2]/ul[1]/li[2]/a[@href='javascript:;']

${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}                                     //input[@id='discounttext']

${XPATH_FIELD_DESCRIPTION}                                                     //textarea[@id='description']

${XPATH_CHECKBOX_VAT_SHIPPING}                                                 //input[@id='isshippingchk']

${XPATH_FIELD_SHIPPING_PRICE}                                                  //input[@id='shippingamount']

${XPATH_TEXT_SUMMARY_AMOUNT}                                                   //span[@id='amounttext']

#RECIPIENTXPATH

${XPATH_FIELD_SHIPPING_NAME}                                                   //input[@id='shippingname']

${XPATH_BUTTON_COPY_SHIPPING_INFO_FROM_CUSTOMER}                               //span[text()='คัดลอกจากข้อมูลลูกค้า']

${XPATH_FIELD_SHIPPING_PHONE}                                                  //input[@id='shippingphone']

${XPATH_FIELD_SHIPPING_EMAIL}                                                  //input[@id='shippingemail']

${XPATH_FIELD_SHIPPING_ADDRESS}                                                //textarea[@id='shippingaddress']

${XPATH_BUTTON_VALIDATE_SHIPPING_ADDRESS}                                       //a[text()='ตรวจสอบที่อยู่']

${XPATH_DROPDOWN_SHIPPING_PROVINCE}                                               //select[@id='shippingprovince']

${XPATH_DROPDOWN_SHIPPING_DISTRICT}                                               //select[@id='shippingdistrict']

${XPATH_DROPDOWN_SHIPPING_SUBDISTRICT}                                            //select[@id='shippingsubdistrict']

${XPATH_FIELD_SHIPPING_POSTCODE}                                                  //input[@id='shippingpostcode']

${XPATH_SAMPLE_DROPDOWN_PROVINCE}                                                 //option[@value='เชียงราย']

${XPATH_SAMPLE_DROPDOWN_DISTRICT}                                                 //option[@value='บางใหญ่']

${XPATH_SAMPLE_DROPDOWN_SUBDISTRICT}                                              //option[@value='บางรักใหญ่']

#SHIPPINGXPATH

${XPATH_FIELD_SHIPPING_DATE}                                                       //input[@id='shippingdate']

${XPATH_CHOOSE_SHIPPING_YEAR}                                                      //div[16]/div[4]/table[1]/thead[1]/tr[1]/th[@class='switch']

${XPATH_CHOOSE_SHIPPING_MONTH}                                                     //div[16]/div[3]/table[1]/thead[1]/tr[1]/th[@class='switch']

${XPATH_SAMELE_SHIPPING_YEAR}                                                      //div[16]/div[5]/table[1]/tbody[1]/tr[1]/td[1]/span[@class='year'][text()='2564']

${XPATH_SAMELE_SHIPPING_MONTH}                                                     //div[16]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/span[@class='month'][text()='พ.ค.']

${XPATH_SAMPLE_SHIPPING_DATE}                                                      //div[16]/div[3]/table[1]/tbody[1]/tr[5]/td[@class='day'][text()='25']

${XPATH_SAMPLE_SHIPPING_HOUR}                                                      //div[16]/div[2]/table[1]/tbody[1]/tr[1]/td[1]/span[@class='hour'][text()='17:00']

${XPATH_SAMPLE_SHIPING_MINUTE}                                                     //div[16]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/span[@class='minute'][text()='17:50']

${XPATH_BUTTON_REMOVE_SHIPPING_DATE}                                               //div[1]/div[1]/div[1]/div[1]/div[1]/span[1]/span[@class='glyphicon glyphicon-remove']

${XPATH_FIELD_SHIPPING_TRACKINGNO}                                                 //input[@id='trackingno']

#PAYMENTXPATH

${XPATH_BUTTON_ADD_PAYMENT}                                                        //a[text()='เพิ่มการชำระเงิน']

${XPATH_FIELD_PAYMENTAMOUNT_IN_ADD_PAYMENT_MODAL}                                  //input[@id='tmppaymentamount']

${XPATH_FIELD_PAYMENTNAME_IN_ADD_PAYMENT_MODAL}                                    //input[@id='tmppaymentname']

${XPATH_SAMPLE_CHOOSE_PAYMENYNAME_IN_ADD_PAYMENT_MODAL}                            //div[3]/ul[1]/li[2]/a[@href='javascript:;']

${XPATH_FIELD_PAYMENTDATETIME_IN_ADD_PAYMENT_MODAL}                                //input[@id='tmppaymentdatetime']

${XPATH_CHOOSE_PAYMENT_YEAR}                                                       //*/body/div[16]/div[4]/table/thead/tr/th[2]

${XPATH_CHOOSE_PAYMENT_MONTH}                                                      //*/body/div[16]/div[3]/table/thead/tr[1]/th[2]

${XPATH_SAMELE_PAYMENT_YEAR}                                                      //div[16]/div[5]/table[1]/tbody[1]/tr[1]/td[1]/span[@class='year'][text()='2564']

${XPATH_SAMELE_PAYMENT_MONTH}                                                     //div[16]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/span[@class='month'][text()='พ.ค.']

${XPATH_SAMPLE_PAYMENT_DATE}                                                      //div[16]/div[3]/table[1]/tbody[1]/tr[5]/td[@class='day'][text()='25']

${XPATH_SAMPLE_PAYMENT_HOUR}                                                      //div[16]/div[2]/table[1]/tbody[1]/tr[1]/td[1]/span[@class='hour'][text()='17:00']

${XPATH_SAMPLE_PAYMENT_MINUTE}                                                     //span[contains(text(),'17:50')]

${XPATH_BUTTON_REMOVE_PAYMENT_DATE}                                                //div[2]/div[3]/div[1]/div[1]/div[1]/span[1]/span[@class='glyphicon glyphicon-remove']

${XPATH_CHECKBOX_TAXSTATUS_IN_ADD_PAYMENT_MODAL}                                   //input[@id='withholdingtaxstatus']

${XPATH_DROPDOWN_PERCENT_TAX_IN_ADD_PAYMENT_MODAL}                                 //select[@id='withholdingtaxnumber']

${XPATH_SAMPLE_CHOOSE_PERCENT_TAX_IN_ADD_PAYMENT_MODAL}                             //select[@id='withholdingtaxnumber']/option[@value='3']

${XPATH_FIELD_AMOUNT_TAX_IN_ADD_PAYMENT_MODAL}                                      //input[@id='withholdingtaxamount']

${XPATH_FIELD_AMOUNT_TOTAL_IN_ADD_PAYMENT_MODAL}                                    //input[@id='withholdingtaxnet']

${XPATH_BUTTON_OK_IN_ADD_PAYMENT_MODAL}                                            //div[8]/div[1]/div[1]/div[3]/button[@type='button'][text()='ตกลง']

${XPATH_TEXTLINK_DELETE_PAYMENT}                                                   //a[contains(text(),'ลบ')]

${XPATH_TEXTLINK_ADD_PAYMENT}                                                      //a[contains(text(),'เพิ่มการชำระเงิน')]

#WAREHOUSE/BRANCHXPATH

${XPATH_RADIO_BUTTON_WAIT_SHIPPING}                                                    //input[@id='warehousetype0']

${XPATH_DROPDOWN_WAIT_SHIPPING}                                                        //div[@id='warehouseidarea0']

${XPATH_SAMPLE_WAREHOUSE_WAIT_SHIPPING}                                                //li[3]/a[1]/span[@class='text'][text()='ข้อมูลตัวอย่าง 1']

${XPATH_DROPDOWN_WAIT_SHIPPING_GET_VALUE}                                              //div[1]/div[1]/div[2]/div[1]/div[1]/button[1]/span[@class='filter-option pull-left'][text()='ข้อมูลตัวอย่าง 1']

${XPATH_RADIO_BUTTON_SHIPPING_NOW}                                                     //input[@id='warehousetype1']

${XPATH_DROPDOWN_SHIPPING_NOW}                                                         //div[@id='warehouseidarea']

${XPATH_SAMPLE_WAREHOUSE_SHIPPING_NOW}                                                 //li[2]/a[1]/span[@class='text'][text()='ข้อมูลตัวอย่าง 1']

${XPATH_DROPDOWN_SHIPPING_NOW_GET_VALUE}                                               //div[2]/div[1]/div[2]/div[1]/div[1]/button[1]/span[@class='filter-option pull-left'][text()='ข้อมูลตัวอย่าง 1']

#OTHERXPATH

${XPATH_BUTTON_BACK_TO_ORDER_LIST_1}                                                   //span[@class='font-kanit fs-sm']

${XPATH_BUTTON_BACK_TO_ORDER_LIST_2}                                                   //a[text()='กลับ']

${XPATH_BUTTON_SAVE_AND_CREATE_ORDER}                                                  //button[text()='บันทึก + สร้างใหม่']

${XPATH_BUTTON_SAVE_ORDER}                                                             //button[text()='บันทึก']

${XPATH_BUTTON_OK_SUCCESS_MODAL}                                                       //button[@onclick='closeSuccessPopup();'][text()='ตกลง']

${XPATH_BUTTON_CONFIRM_NEW_SO}                                                          //div[12]/div[1]//div[1]/div[3]/div[1]/button[@onclick='confirmPopup();'][text()='ยืนยัน']

${XPATH_TEXT_NEW_SO_IN_POPUP}                                                           //strong

${XPATH_BUTTON_OK_ERROR_POPUP}                                                          //button[@onclick='closeErrorPopup();'][text()='ตกลง']


                                                   











  


   

