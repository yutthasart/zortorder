*** Settings ***
Resource          ${CURDIR}/../resources/testdata/testdata.robot
Resource          ${CURDIR}/../resources/variables/variable_config.robot
Resource          ${CURDIR}/../resources/variables/variable_login.robot
Resource          ${CURDIR}/../resources/variables/variable_home.robot
Resource          ${CURDIR}/../resources/variables/variable_addsellorder.robot
Resource          ${CURDIR}/../keywords/common/common_keywords.robot
Suite Teardown    Close All Browsers


*** Test Cases ***
TC_139_Test Click Radio Button Shipping Now
     [Documentation]   ทดสอบกด radio button โอนทันทีออกจากคลังสินค้า 
     Open Browser      ${URL}      ${BROWSER}
     Set Window Size    1920   1080            
     Input Text To Element When Ready          ${XPATH_FIELD_USERNAME}       ${EMAIL}           
     Input Text To Element When Ready          ${XPATH_FIELD_PASSWORD}       ${PASSWORD}     
     Click Element When Ready                  ${XPATH_BUTTON_LOGIN}
     Sleep                                     10s
     ${popup1}  Run Keyword And Return Status    Element Should Be Visible         ${XPATH_CLOSE_POPUP_EXPIRE}
       Log  ${popup1}
       Run Keyword If    '${popup1}'=='True'       Click Element                     ${XPATH_CLOSE_POPUP_EXPIRE}
       Run Keyword If    '${popup1}'=='False'      Pop Up Not Avalible
     ${popup2}  Run Keyword And Return Status    Element Should Be Visible         ${XPATH_CLOSE_POPUP}
       Log  ${popup2}
       Run Keyword If    '${popup2}'=='True'       Click Element                     ${XPATH_CLOSE_POPUP}
       Run Keyword If    '${popup2}'=='False'      Pop Up Not Avalible
     # Wait Until Element Is Visible             ${XPATH_CLOSE_POPUP_EXPIRE}          20s
     # Click Element When Ready                  ${XPATH_CLOSE_POPUP_EXPIRE}
     # Wait Until Element Is Visible             ${XPATH_CLOSE_POPUP}                 20s
     # Click Element When Ready                  ${XPATH_CLOSE_POPUP}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU_ADD_ORDER}
     Click Element When Ready                  ${XPATH_RADIO_BUTTON_SHIPPING_NOW}
     Wait Until Element Is Visible             ${XPATH_DROPDOWN_SHIPPING_NOW}
     Capture Page Screenshot                                  
   
TC_140_Test Choose Data In Dropdown Shipping Now
     [Documentation]   ทดสอบเลือกข้อมูลคลังสินค้า
     Click Element When Ready                  ${XPATH_DROPDOWN_SHIPPING_NOW}
     Click Element When Ready                  ${XPATH_SAMPLE_WAREHOUSE_SHIPPING_NOW} 
     ${WAREHOUSE}=                      Get Text From Element When Ready         ${XPATH_DROPDOWN_SHIPPING_NOW_GET_VALUE}
     Should Be Equal                           ${WAREHOUSE}                    ข้อมูลตัวอย่าง 1  
     Capture Page Screenshot       

TC_141_Test Click Radio Button Wait Shipping
     [Documentation]   ทดสอบกด radio button รอโอนสินค้า
     Click Element When Ready                  ${XPATH_RADIO_BUTTON_WAIT_SHIPPING}
     Wait Until Element Is Visible             ${XPATH_DROPDOWN_WAIT_SHIPPING}
     Capture Page Screenshot 

TC_142_Test Choose Data In Dropdown Wait Shipping
     [Documentation]   ทดสอบเลือกข้อมูลคลังสินค้า
     Click Element When Ready                  ${XPATH_DROPDOWN_WAIT_SHIPPING}
     Click Element When Ready                  ${XPATH_SAMPLE_WAREHOUSE_WAIT_SHIPPING} 
     ${WAREHOUSE}=                      Get Text From Element When Ready         ${XPATH_DROPDOWN_WAIT_SHIPPING_GET_VALUE}
     Should Be Equal                           ${WAREHOUSE}                    ข้อมูลตัวอย่าง 1  
     Capture Page Screenshot 