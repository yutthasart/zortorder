*** Settings ***
Resource          ${CURDIR}/../resources/testdata/testdata.robot
Resource          ${CURDIR}/../resources/variables/variable_config.robot
Resource          ${CURDIR}/../resources/variables/variable_login.robot
Resource          ${CURDIR}/../resources/variables/variable_home.robot
Resource          ${CURDIR}/../resources/variables/variable_addsellorder.robot
Resource          ${CURDIR}/../keywords/common/common_keywords.robot
Suite Teardown    Close All Browsers


*** Test Cases ***
TC_009_Test Input Data In Textbox Sell Order
     [Documentation]   ทดสอบกรอกข้อมูลใน textbox รายการ
     Open Browser      ${URL}      ${BROWSER}
     Set Window Size    1920   1080            
     Input Text To Element When Ready          ${XPATH_FIELD_USERNAME}       ${EMAIL}           
     Input Text To Element When Ready          ${XPATH_FIELD_PASSWORD}       ${PASSWORD}     
     Click Element When Ready                  ${XPATH_BUTTON_LOGIN}
     Sleep                                     10s
     ${popup1}  Run Keyword And Return Status    Element Should Be Visible           ${XPATH_CLOSE_POPUP_EXPIRE}
       Log  ${popup1}
       Run Keyword If    '${popup1}'=='True'       Click Element                     ${XPATH_CLOSE_POPUP_EXPIRE}
       Run Keyword If    '${popup1}'=='False'      Pop Up Not Avalible
     ${popup2}  Run Keyword And Return Status    Element Should Be Visible           ${XPATH_CLOSE_POPUP}
       Log  ${popup2}
       Run Keyword If    '${popup2}'=='True'       Click Element                     ${XPATH_CLOSE_POPUP}
       Run Keyword If    '${popup2}'=='False'      Pop Up Not Avalible
     # Wait Until Element Is Visible             ${XPATH_CLOSE_POPUP_EXPIRE}          20s
     # Click Element When Ready                  ${XPATH_CLOSE_POPUP_EXPIRE}
     # Wait Until Element Is Visible             ${XPATH_CLOSE_POPUP}                 20s
     # Click Element When Ready                  ${XPATH_CLOSE_POPUP}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU_ADD_ORDER}
     Clear Text When Ready                     ${XPATH_FIELD_SO_NUMBER}
     Input Text To Element When Ready          ${XPATH_FIELD_SO_NUMBER}      SO-0123456789
     Capture Page Screenshot
   
TC_010_Test Choose Date
     [Documentation]   ทดสอบเลือกข้อมูลวันที่
     Click Element When Ready                  ${XPATH_FIELD_DATE}
     Click Element When Ready                  ${XPATH_SAMPLE_DATE}
     Capture Page Screenshot

TC_011_Test Remove Date
     [Documentation]   ทดสอบกด x เพื่อลบข้อมูลวันที่
     Click Element When Ready                  ${XPATH_REMOVE_DATE}
     Capture Page Screenshot

TC_012_Test Choose Expire Date
     [Documentation]   ทดสอบเลือกข้อมูลวันที่
     Click Element When Ready                  ${XPATH_FIELD_EXPIRE_DATE}
     Click Element When Ready                  ${XPATH_SAMPLE_DATE}
     Capture Page Screenshot

TC_013_Test Remove Expire Date
     [Documentation]   ทดสอบกด x เพื่อลบข้อมูลวันที่
     Click Element When Ready                  ${XPATH_REMOVE_EXPIRE_DATE}
     Capture Page Screenshot

TC_014_Test Input Data In Reference
     [Documentation]   ทดสอบกรอกข้อมูลใน textbox อ้างอิง
     Click Element When Ready                  ${XPATH_FIELD_REF}
     Input Text To Element When Ready          ${XPATH_FIELD_REF}     Test Reference
     Capture Page Screenshot

TC_015_Test Remove Reference
     [Documentation]   ทดสอบกด x เพื่อลบข้อมูลอ้างอิง
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_REMOVE_REF}
     Capture Page Screenshot

TC_016_Test Input Data In Sale Channel
     [Documentation]   ทดสอบกรอกข้อมูลใน textbox ช่องทางการขาย
     Click Element When Ready                  ${XPATH_FIELD_SALE_CHANNEL}
     Input Text To Element When Ready          ${XPATH_FIELD_SALE_CHANNEL}     Test Sale Channel
     Capture Page Screenshot

TC_017_Test Choose Data In Dropdown Sale Channel
     [Documentation]   ทดสอบเลือกข้อมูลใน dropdown ช่องทางการขาย
     Click Element When Ready                  ${XPATH_FIELD_SALE_CHANNEL}
     Clear Text When Ready                     ${XPATH_FIELD_SALE_CHANNEL}
     Click Element When Ready                  ${XPATH_SAMPLE_SALE_CHANNEL}
     Capture Page Screenshot

TC_018_Test Remove Sale Channel
     [Documentation]   ทดสอบกด x เพื่อลบข้อมูลช่องทางการขาย
     Sleep                                     3s
     Press Keys                                ${XPATH_FIELD_SALE_CHANNEL}      SPACE
     Click Element When Ready                  ${XPATH_REMOVE_SALE_CHANNEL}
     Capture Page Screenshot
     
TC_019_Test Choose Data In Dropdown VatType
     [Documentation]   ทดสอบเลือกข้อมูลใน dropdown ประเภทภาษี
     Click Element When Ready                  ${XPATH_FIELD_REF}
     Click Element When Ready                  ${XPATH_DROPDOWN_VATTYPE}
     Click Element When Ready                  ${XPATH_SAMPLE_VATTYPE_2}
     Capture Page Screenshot
     Click Element When Ready                  ${XPATH_SAMPLE_VATTYPE_3}
     Click Element When Ready                  ${XPATH_DROPDOWN_VATTYPE}
     Capture Page Screenshot
     Click Element When Ready                  ${XPATH_SAMPLE_VATTYPE_1}
     Click Element When Ready                  ${XPATH_DROPDOWN_VATTYPE}
     Capture Page Screenshot

TC_020_Test Click Text Link Agent
     [Documentation]   ทดสอบกด text link เลือกตัวแทน
     Click Element When Ready                  ${XPATH_TEXTLINK_AGENT}
     Wait Until Page Contains                  เลือกตัวแทนจำหน่าย
     Capture Page Screenshot

TC_021_Validate Modal Choose Agent
     [Documentation]   ตรวจสอบ modal เลือกตัวแทนจำหน่าย
     Wait Until Element Is Visible             ${XPATH_FIELD_SEARCH_AGENT}
     Wait Until Element Is Visible             ${XPATH_BUTTON_ADD_AGENT}
     Wait Until Page Contains                  ชื่อ
     Wait Until Page Contains                  เบอร์โทรศัพท์
     Wait Until Page Contains                  อีเมล
     Wait Until Element Is Visible             //tbody/tr[1]/td[4]//a[@class='button button-default button-sm'][text()='เลือก']
     Capture Page Screenshot

TC_022_Test Search Agent
     [Documentation]   ทดสอบค้นหาข้อมูลตัวแทนจำหน่ายใน Modal เลือกตัวแทนจำหน่าย
     Input Text To Element When Ready          //input[@id="quicksearchagenttext"]      ข้อมูลตัวอย่าง 1
     Press Keys                                //input[@id="quicksearchagenttext"]      ENTER
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 1
     Capture Page Screenshot

TC_023_Test Click Add Agent Button and Validate Add Agent Modal
     [Documentation]   ทดสอบกดปุ่มเพิ่มตัวแทนจำหน่ายใน Modal เลือกตัวแทนจำหน่าย และ ตรวจสอบ modal
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_ADD_AGENT}
     Wait Until Page Contains                  ชื่อตัวแทน
     Wait Until Element Is Visible             ${XPATH_FIELD_NAME_AGENT}
     Wait Until Page Contains                  เบอร์โทรศัพท์
     Wait Until Element Is Visible             ${XPATH_FIELD_TEL_AGENT}
     Wait Until Page Contains                  อีเมล
     Wait Until Element Is Visible             ${XPATH_FIELD_EMAIL_AGENT}
     Wait Until Page Contains                  ที่อยู่
     Wait Until Element Is Visible             ${XPATH_FIELD_ADDRESS_AGENT}
     Wait Until Element Is Visible             ${XPATH_BUTTON_OK_AGENT}
     Capture Page Screenshot

TC_024_Test Input Data In Add Agent Modal And Click Ok
     [Documentation]   ทดสอบกรอกรายละเอียดเพิ่มตัวแทนจำหน่ายแล้วกดปุ่มตกลง
     Input Text To Element When Ready          ${XPATH_FIELD_NAME_AGENT}          Agent Test
     Input Text To Element When Ready          ${XPATH_FIELD_TEL_AGENT}           0842289760
     Input Text To Element When Ready          ${XPATH_FIELD_EMAIL_AGENT}         yutthasart@zortout.com
     Input Text To Element When Ready          ${XPATH_FIELD_ADDRESS_AGENT}       11/1 หมู่ 3 ต.บางรักพัฒนา อ.บางบัวทอง จ.นนทบุรี 11110
     Click Element When Ready                  ${XPATH_BUTTON_OK_AGENT}
     Wait Until Page Contains                  Agent Test
     Capture Page Screenshot

TC_025_Test Remove Agent From Create Sell Order Page
     [Documentation]   ทดสอบลบตัวแทนจำหน่ายออกจากหน้าสร้างรายการขาย
     Click Element When Ready                  ${XPATH_TEXTLINK_DELETE_AGENT}
     Wait Until Page Does Not Contain          Agent Test
     Capture Page Screenshot

TC_026_Test Input Empty Data In Add Agent Modal And Click Ok
     [Documentation]   ทดสอบไม่กรอกข้อมูลเพิ่มตัวแทนจำหน่ายแล้วกดปุ่มตกลง
     Click Element When Ready                  ${XPATH_TEXTLINK_AGENT}
     Click Element When Ready                  ${XPATH_BUTTON_ADD_AGENT}
     Click Element When Ready                  ${XPATH_BUTTON_OK_AGENT}
     Wait Until Page Contains                  กรุณาใส่ข้อมูลให้ครบถ้วน
     Capture Page Screenshot

TC_027_Test Choose Agent In Chosse Agent Modal
     [Documentation]   ทดสอบกดปุ่มเลือกตัวแทนจำหน่ายที่มีอยู่แล้วใน Modal ตัวแทนจำหน่าย
     Click Element When Ready                  ${XPATH_BUTTON_OK_ERROR_DATA_AGENT}
     Click Element When Ready                  ${XPATH_BUTTON_CLOSE_MODAL_ADD_AGENT}
     Click Element When Ready                  ${XPATH_SAMPLE_CHOOSE_AGENT}
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 2
     Capture Page Screenshot