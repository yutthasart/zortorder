*** Settings ***
Resource          ${CURDIR}/../resources/testdata/testdata.robot
Resource          ${CURDIR}/../resources/variables/variable_config.robot
Resource          ${CURDIR}/../resources/variables/variable_login.robot
Resource          ${CURDIR}/../resources/variables/variable_home.robot
Resource          ${CURDIR}/../resources/variables/variable_addsellorder.robot
Resource          ${CURDIR}/../keywords/common/common_keywords.robot
Suite Teardown    Close All Browsers


*** Test Cases ***
TC_124_Test Click Button Add Payment
     [Documentation]   ทดสอบกดปุ่มเพิ่มการชำระเงิน 
     Open Browser      ${URL}      ${BROWSER}
     Set Window Size    1920   1080            
     Input Text To Element When Ready          ${XPATH_FIELD_USERNAME}       ${EMAIL}           
     Input Text To Element When Ready          ${XPATH_FIELD_PASSWORD}       ${PASSWORD}     
     Click Element When Ready                  ${XPATH_BUTTON_LOGIN}
     Sleep                                     10s
     ${popup1}  Run Keyword And Return Status    Element Should Be Visible         ${XPATH_CLOSE_POPUP_EXPIRE}
       Log  ${popup1}
       Run Keyword If    '${popup1}'=='True'       Click Element                     ${XPATH_CLOSE_POPUP_EXPIRE}
       Run Keyword If    '${popup1}'=='False'      Pop Up Not Avalible
     ${popup2}  Run Keyword And Return Status    Element Should Be Visible         ${XPATH_CLOSE_POPUP}
       Log  ${popup2}
       Run Keyword If    '${popup2}'=='True'       Click Element                     ${XPATH_CLOSE_POPUP}
       Run Keyword If    '${popup2}'=='False'      Pop Up Not Avalible
     # Wait Until Element Is Visible             ${XPATH_CLOSE_POPUP_EXPIRE}          20s
     # Click Element When Ready                  ${XPATH_CLOSE_POPUP_EXPIRE}
     # Wait Until Element Is Visible             ${XPATH_CLOSE_POPUP}                 20s
     # Click Element When Ready                  ${XPATH_CLOSE_POPUP}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU_ADD_ORDER}
     Click Element When Ready                  ${XPATH_BUTTON_ADD_PAYMENT}
     Wait Until Page Contains                  ชำระเงิน
     Capture Page Screenshot                                  
   
TC_125_Validate Add Payment Modal
     [Documentation]   ตรวจสอบ modal ชำระเงิน
     Wait Until Page Contains                  จำนวนเงิน
     Wait Until Element Is Visible             ${XPATH_FIELD_PAYMENTAMOUNT_IN_ADD_PAYMENT_MODAL}
     Wait Until Page Contains                  ช่องทางการชำระเงิน
     Wait Until Element Is Visible             ${XPATH_FIELD_PAYMENTNAME_IN_ADD_PAYMENT_MODAL}
     Wait Until Page Contains                  วันที่ชำระเงิน
     Wait Until Element Is Visible             ${XPATH_FIELD_PAYMENTDATETIME_IN_ADD_PAYMENT_MODAL}
     Wait Until Page Contains                  ตั้งค่า
     Wait Until Element Is Visible             ${XPATH_CHECKBOX_TAXSTATUS_IN_ADD_PAYMENT_MODAL}
     Wait Until Page Contains                  ภาษีหัก ณ ที่จ่าย
     Wait Until Element Is Visible             ${XPATH_BUTTON_OK_IN_ADD_PAYMENT_MODAL}
     Capture Page Screenshot       

TC_126_Test Input Integer In Payment Amount Text Box
     [Documentation]   ทดสอบกรอกจำนวนเต็มลงใน text box จำนวนเงิน
     Clear Text When Ready                     ${XPATH_FIELD_PAYMENTAMOUNT_IN_ADD_PAYMENT_MODAL}
     Input Text To Element When Ready          ${XPATH_FIELD_PAYMENTAMOUNT_IN_ADD_PAYMENT_MODAL}        500
     Click Element When Ready                  ${XPATH_FIELD_PAYMENTNAME_IN_ADD_PAYMENT_MODAL}                 
     ${Payment Amount}=                      Get Value From Element When Ready         ${XPATH_FIELD_PAYMENTAMOUNT_IN_ADD_PAYMENT_MODAL}
     Should Be Equal                           ${Payment Amount}                    500.00                      
     Capture Page Screenshot

TC_127_Test Decimal Text In Payment Amount Text Box
     [Documentation]   ทดสอบกรอกจำนวนที่มีจุดทศนิยมลงใน text box จำนวนเงิน
     Clear Text When Ready                     ${XPATH_FIELD_PAYMENTAMOUNT_IN_ADD_PAYMENT_MODAL}
     Input Text To Element When Ready          ${XPATH_FIELD_PAYMENTAMOUNT_IN_ADD_PAYMENT_MODAL}        500.50
     Click Element When Ready                  ${XPATH_FIELD_PAYMENTNAME_IN_ADD_PAYMENT_MODAL}
     ${Payment Amount}=                      Get Value From Element When Ready         ${XPATH_FIELD_PAYMENTAMOUNT_IN_ADD_PAYMENT_MODAL}
     Should Be Equal                           ${Payment Amount}                    500.50                      
     Capture Page Screenshot

TC_128_Test Input Negative Number In Payment Amount Text Box
     [Documentation]   ทดสอบกรอกจำนวนติดลบลงใน text box จำนวนเงิน
     Clear Text When Ready                     ${XPATH_FIELD_PAYMENTAMOUNT_IN_ADD_PAYMENT_MODAL}
     Input Text To Element When Ready          ${XPATH_FIELD_PAYMENTAMOUNT_IN_ADD_PAYMENT_MODAL}        -500
     Click Element When Ready                  ${XPATH_FIELD_PAYMENTNAME_IN_ADD_PAYMENT_MODAL}
     ${Payment Amount}=                      Get Value From Element When Ready         ${XPATH_FIELD_PAYMENTAMOUNT_IN_ADD_PAYMENT_MODAL}
     Should Be Equal                           ${Payment Amount}                    -500.00                     
     Capture Page Screenshot

TC_129_Test Input Text In Payment Amount Text Box
     [Documentation]   ทดสอบกรอกตัวอักษรลงในช่อง text box จำนวนเงิน
     Clear Text When Ready                     ${XPATH_FIELD_PAYMENTAMOUNT_IN_ADD_PAYMENT_MODAL}
     Input Text To Element When Ready          ${XPATH_FIELD_PAYMENTAMOUNT_IN_ADD_PAYMENT_MODAL}        test1234
     Click Element When Ready                  ${XPATH_FIELD_PAYMENTNAME_IN_ADD_PAYMENT_MODAL}
     ${Payment Amount}=                      Get Value From Element When Ready          ${XPATH_FIELD_PAYMENTAMOUNT_IN_ADD_PAYMENT_MODAL}
     Should Be Equal                           ${Payment Amount}                    ${EMPTY}                      
     Capture Page Screenshot      

TC_130_Test Input Data In Payment Name Text Box
     [Documentation]   ทดสอบกรอกข้อมูลช่องทางการชำระเงิน
     Clear Text When Ready                     ${XPATH_FIELD_PAYMENTNAME_IN_ADD_PAYMENT_MODAL}
     Input Text To Element When Ready          ${XPATH_FIELD_PAYMENTNAME_IN_ADD_PAYMENT_MODAL}        true money wallet                  
     Capture Page Screenshot  

TC_131_Test Choose Data In Payment Name Text Box
     [Documentation]   ทดสอบเลือกข้อมูลช่องทางการชำระเงิน
     Clear Text When Ready                     ${XPATH_FIELD_PAYMENTNAME_IN_ADD_PAYMENT_MODAL}
     Input Text To Element When Ready          ${XPATH_FIELD_PAYMENTNAME_IN_ADD_PAYMENT_MODAL}         บัตรเครดิต             
     Capture Page Screenshot                   

TC_132_Test Input Data In Payment Date Time Text Box
     [Documentation]   ทดสอบเลือกข้อมูลวันที่ชำระเงิน
     Click Element When Ready                  ${XPATH_FIELD_PAYMENTAMOUNT_IN_ADD_PAYMENT_MODAL}
     Click Element When Ready                  ${XPATH_FIELD_PAYMENTDATETIME_IN_ADD_PAYMENT_MODAL}
     ${Payment Date}=                      Get Value From Element When Ready         ${XPATH_FIELD_PAYMENTDATETIME_IN_ADD_PAYMENT_MODAL}
     Should Be Equal                           ${Payment Date}                       ${Payment Date}
     Capture Page Screenshot             

TC_133_Test Delete Data In Payment Date Time Text Box
     [Documentation]   ทดสอบกด x เพื่อลบข้อมูลวันที่
     Click Element When Ready                  ${XPATH_BUTTON_REMOVE_PAYMENT_DATE}
     ${Payment Date}=                      Get Value From Element When Ready         ${XPATH_FIELD_PAYMENTDATETIME_IN_ADD_PAYMENT_MODAL}
     Should Be Equal                           ${Payment Date}                       ${Empty}
     Capture Page Screenshot 

TC_134_Test Click Checkbox Tax Status
     [Documentation]   ทดสอบกดที่ check box ภาษีหัก ณ ที่จ่าย
     Click Element When Ready                  ${XPATH_CHECKBOX_TAXSTATUS_IN_ADD_PAYMENT_MODAL}
     Wait Until Page Contains                  ภาษีหัก ณ ที่จ่าย
     Wait Until Element Is Visible             ${XPATH_DROPDOWN_PERCENT_TAX_IN_ADD_PAYMENT_MODAL}
     Wait Until Page Contains                  ภาษีหัก ณ ที่จ่าย
     Wait Until Element Is Visible             ${XPATH_FIELD_AMOUNT_TAX_IN_ADD_PAYMENT_MODAL}
     Wait Until Page Contains                  ยอดเงินสุทธิ
     Wait Until Element Is Visible             ${XPATH_FIELD_AMOUNT_TOTAL_IN_ADD_PAYMENT_MODAL}     

TC_135_Validate Payment Tax Total
     [Documentation]   ทดสอบกรอกจำนวนเงินแล้วเลือก check box ภาษีหัก ณ ที่จ่าย
     Click Element When Ready                  ${XPATH_DROPDOWN_PERCENT_TAX_IN_ADD_PAYMENT_MODAL}
     Click Element When Ready                  ${XPATH_SAMPLE_CHOOSE_PERCENT_TAX_IN_ADD_PAYMENT_MODAL}           
     Clear Text When Ready                     ${XPATH_FIELD_PAYMENTAMOUNT_IN_ADD_PAYMENT_MODAL}
     Input Text To Element When Ready          ${XPATH_FIELD_PAYMENTAMOUNT_IN_ADD_PAYMENT_MODAL}        19000
     Click Element When Ready                  ${XPATH_FIELD_PAYMENTNAME_IN_ADD_PAYMENT_MODAL}                              
     ${Amount Tax}=                      Get Value From Element When Ready           ${XPATH_FIELD_AMOUNT_TAX_IN_ADD_PAYMENT_MODAL}
     Should Be Equal                           ${Amount Tax}                      570.00 
     ${Amount Total}=                      Get Value From Element When Ready         ${XPATH_FIELD_AMOUNT_TOTAL_IN_ADD_PAYMENT_MODAL}
     Should Be Equal                           ${Amount Total}                    18,430.00                      
     Capture Page Screenshot

TC_136_Test Input Data In Add Payment Modal And Click OK Button
     [Documentation]   ทดสอบกรอกข้อมูล modal ชำระเงิน แล้วกดปุ่มตกลง
     Input Text To Element When Ready          ${XPATH_FIELD_PAYMENTDATETIME_IN_ADD_PAYMENT_MODAL}             25/05/2564 17:50
     Click Element When Ready                  ${XPATH_BUTTON_OK_IN_ADD_PAYMENT_MODAL}
     Wait Until Page Contains                  18,430.00 - บัตรเครดิต
     Wait Until Page Contains                  570.00 - ภาษีหัก ณ ที่จ่าย

TC_137_Test Click Textlink Delete Payment
     [Documentation]   ทดสอบกด ลบ ที่ข้อมูลการชำระเงิน
     Click Element When Ready                  ${XPATH_TEXTLINK_DELETE_PAYMENT}
     Wait Until Page Contains                  ไม่มี เพิ่มการชำระเงิน

TC_138_Test No Input Data In Add Payment Modal And Click OK Button
     [Documentation]   ทดสอบเพิ่มการชำระเงินโดยไม่กรอกข้อมูลที่เป็น * จากนั้นกดปุ่มตกลง
     Click Element When Ready                  ${XPATH_TEXTLINK_ADD_PAYMENT}
     Clear Text When Ready                     ${XPATH_FIELD_PAYMENTAMOUNT_IN_ADD_PAYMENT_MODAL}
     Clear Text When Ready                     ${XPATH_FIELD_PAYMENTNAME_IN_ADD_PAYMENT_MODAL}
     Wait Until Page Contains                  กรุณาใส่ข้อมูลให้ครบถ้วน