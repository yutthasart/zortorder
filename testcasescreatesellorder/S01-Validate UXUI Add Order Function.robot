*** Settings ***
Resource          ${CURDIR}/../resources/testdata/testdata.robot
Resource          ${CURDIR}/../resources/variables/variable_config.robot
Resource          ${CURDIR}/../resources/variables/variable_login.robot
Resource          ${CURDIR}/../resources/variables/variable_home.robot
Resource          ${CURDIR}/../resources/variables/variable_addsellorder.robot
Resource          ${CURDIR}/../keywords/common/common_keywords.robot
Suite Teardown    Close All Browsers


*** Test Cases ***
TC_001_Validate Add Sell Order Data
     [Documentation]   ตรวจสอบ UX/UI ขั้นตอนการสร้างรายการขาย ในส่วนของ "ข้อมูล"
     Open Browser      ${URL}      ${BROWSER}
     Set Window Size    1920   1080            
     Input Text To Element When Ready          ${XPATH_FIELD_USERNAME}       ${EMAIL}           
     Input Text To Element When Ready          ${XPATH_FIELD_PASSWORD}       ${PASSWORD}     
     Click Element When Ready                  ${XPATH_BUTTON_LOGIN}
     Sleep                                     10s
     ${popup1}  Run Keyword And Return Status    Element Should Be Visible         ${XPATH_CLOSE_POPUP_EXPIRE}
       Log  ${popup1}
       Run Keyword If    '${popup1}'=='True'       Click Element                     ${XPATH_CLOSE_POPUP_EXPIRE}
       Run Keyword If    '${popup1}'=='False'      Pop Up Not Avalible
     ${popup2}  Run Keyword And Return Status    Element Should Be Visible         ${XPATH_CLOSE_POPUP}
       Log  ${popup2}
       Run Keyword If    '${popup2}'=='True'       Click Element                     ${XPATH_CLOSE_POPUP}
       Run Keyword If    '${popup2}'=='False'      Pop Up Not Avalible
     # Wait Until Element Is Visible             ${XPATH_CLOSE_POPUP_EXPIRE}          20s
     # Click Element When Ready                  ${XPATH_CLOSE_POPUP_EXPIRE}
     # Wait Until Element Is Visible             ${XPATH_CLOSE_POPUP}                 20s
     # Click Element When Ready                  ${XPATH_CLOSE_POPUP}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU_ADD_ORDER}
     Wait Until Page Contains                  ข้อมูล
     Wait Until Page Contains                  ประเภท
     Wait Until Page Contains                  ขายสินค้าออก
     Wait Until Page Contains                  รายการ
     Wait Until Element Is Visible             ${XPATH_FIELD_SO_NUMBER}
     Wait Until Page Contains                  วันที่
     Wait Until Element Is Visible             ${XPATH_FIELD_DATE}
     Wait Until Page Contains                  วันหมดอายุ
     Wait Until Element Is Visible             ${XPATH_FIELD_EXPIRE_DATE}
     Wait Until Page Contains                  อ้างอิง
     Wait Until Element Is Visible             ${XPATH_FIELD_REF}
     Wait Until Page Contains                  ช่องทางการขาย
     Wait Until Element Is Visible             ${XPATH_FIELD_SALE_CHANNEL}
     Wait Until Page Contains                  ประเภทภาษี
     Wait Until Element Is Visible             ${XPATH_DROPDOWN_VATTYPE}
     Wait Until Page Contains                  ตัวแทนจำหน่าย
     Wait Until Element Is Visible             ${XPATH_TEXTLINK_AGENT}
     Capture Page Screenshot
   
TC_002_Validate Add Sell Order Customer
     [Documentation]   ตรวจสอบ UX/UI ขั้นตอนการสร้างรายการขาย ในส่วนของ "ลูกค้า"
     Wait Until Page Contains                  ลูกค้า
     Wait Until Page Contains                  ชื่อลูกค้า
     Wait Until Element Is Visible             ${XPATH_FIELD_CUSTOMER_NAME}
     Click Element When Ready                  ${XPATH_BUTTON_CUSTOMER_CHOOSE_CUSTOMER}
     Click Element When Ready                  ${XPATH_BUTTON_CUSTOMER_CHOOSE_SAMPLE_CUSTOMER}
     Wait Until Page Contains                  รหัสลูกค้า
     Wait Until Element Is Visible             ${XPATH_FIELD_CUSTOMER_CODE}
     Wait Until Page Contains                  เบอร์โทรศัพท์ลูกค้า
     Wait Until Element Is Visible             ${XPATH_FIELD_CUSTOMER_PHONE}
     Wait Until Page Contains                  อีเมลลูกค้า
     Wait Until Element Is Visible             ${XPATH_FIELD_CUSTOMER_EMAIL}
     Wait Until Page Contains                  ที่อยู่ลูกค้า
     Wait Until Element Is Visible             ${XPATH_FIELD_CUSTOMER_ADDRESS}
     Wait Until Page Contains                  กำหนดเลขผู้เสียภาษี, ชื่อสาขา, เลขที่สาขา
     Wait Until Element Is Visible             ${XPATH_CHECKBOX_CUSTOMER_MERCHANT}
     Wait Until Page Contains                  เลขประจำตัวผู้เสียภาษี
     Wait Until Element Is Visible             ${XPATH_FIELD_CUSTOMER_ID_NUMBER}
     Wait Until Page Contains                  ชื่อสาขา
     Wait Until Element Is Visible             ${XPATH_FIELD_CUSTOMER_BRANCH_NAME}
     Wait Until Page Contains                  เลขที่สาขา
     Wait Until Element Is Visible             ${XPATH_FIELD_CUSTOMER_BRANCH_NO}
     Capture Page Screenshot

TC_003_Validate Add Sell Order Product
     [Documentation]   ตรวจสอบ UX/UI ขั้นตอนการสร้างรายการขาย ในส่วนของ "สินค้า"
     Wait Until Page Contains                  สินค้า
     Wait Until Page Contains                  เลือกสินค้า
     Wait Until Element Is Visible             ${XPATH_ฺBUTTON_MULTICHOOSE_PRODUCT}
     Wait Until Page Contains                  เลือก 
     Wait Until Element Is Visible             ${XPATH_BUTTON_CHOOSE_PRODUCT}
     Wait Until Page Contains                  รหัส
     Wait Until Element Is Visible             ${XPATH_FIELD_PRODUCT_CODE}
     Wait Until Page Contains                  ชื่อสินค้า
     Wait Until Element Is Visible             ${XPATH_FIELD_PRODUCT_NAME}
     Wait Until Page Contains                  จำนวน
     Wait Until Element Is Visible             ${XPATH_FIELD_PRODUCT_QUANTITY}
     Wait Until Page Contains                  มูลค่าต่อหน่วย
     Wait Until Element Is Visible             ${XPATH_FIELD_PRODUCT_PRICE}
     Wait Until Page Contains                  ส่วนลดต่อหน่วย
     Wait Until Element Is Visible             ${XPATH_FIELD_PRODUCT_DISCOUNT}
     Wait Until Page Contains                  รวม
     Wait Until Element Is Visible             ${XPATH_BUTTON_DELETE_PRODUCT}                 
     Wait Until Page Contains                  เพิ่มสินค้า
     Wait Until Element Is Visible             ${XPATH_BUTTON_ADD_PRODUCT}
     Wait Until Page Contains                  ช่องทางจัดส่ง
     Wait Until Element Is Visible             ${XPATH_FIELD_SHIPPING_CHANNEL}
     Wait Until Page Contains                  ส่วนลด
     Wait Until Element Is Visible             ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}
     Wait Until Page Contains                  หมายเหตุ
     Wait Until Element Is Visible             ${XPATH_FIELD_DESCRIPTION}                  
     Wait Until Page Contains                  ค่าส่ง
     Wait Until Page Contains                  (ที่เรียกเก็บจากลูกค้า)
     Wait Until Element Is Visible             ${XPATH_FIELD_SHIPPING_PRICE}
     Wait Until Page Contains                  ภาษีมูลค่าเพิ่ม
     Wait Until Element Is Visible             ${XPATH_CHECKBOX_VAT_SHIPPING}
     Wait Until Page Contains                  มูลค่ารวมก่อนภาษี
     Wait Until Page Contains                  ภาษีมูลค่าเพิ่ม (7%)
     Wait Until Page Contains                  มูลค่ารวม
     Wait Until Page Contains                  มูลค่ารวมสุทธิ
     Capture Page Screenshot
     
TC_004_Validate Add Sell Order Recipient Info
     [Documentation]   ตรวจสอบ UX/UI ขั้นตอนการสร้างรายการขาย ในส่วนของ "ข้อมูลที่อยู่ผู้รับ"
     Wait Until Page Contains                  ข้อมูลที่อยู่ผู้รับ
     Wait Until Page Contains                  ชื่อผู้รับ
     Wait Until Element Is Visible             ${XPATH_FIELD_SHIPPING_NAME}
     Wait Until Page Contains                  คัดลอกจากข้อมูลลูกค้า
     Click Element When Ready                  ${XPATH_BUTTON_COPY_SHIPPING_INFO_FROM_CUSTOMER}
     Wait Until Page Contains                  เบอร์โทรศัพท์ผู้รับ
     Wait Until Element Is Visible             ${XPATH_FIELD_SHIPPING_PHONE}
     Wait Until Page Contains                  อีเมลผู้รับ
     Wait Until Element Is Visible             ${XPATH_FIELD_SHIPPING_EMAIL}
     Wait Until Page Contains                  ที่อยู่/จัดส่ง
     Wait Until Element Is Visible             ${XPATH_FIELD_SHIPPING_ADDRESS}
     Wait Until Page Contains                  ตรวจสอบที่อยู่
     Click Element When Ready                  ${XPATH_BUTTON_VALIDATE_SHIPPING_ADDRESS}
     Wait Until Page Contains                  จังหวัด
     Wait Until Element Is Visible             ${XPATH_DROPDOWN_SHIPPING_PROVINCE}
     Wait Until Page Contains                  อำเภอ/เขต
     Wait Until Element Is Visible             ${XPATH_DROPDOWN_SHIPPING_DISTRICT}
     Wait Until Page Contains                  ตำบล/แขวง
     Wait Until Element Is Visible             ${XPATH_DROPDOWN_SHIPPING_SUBDISTRICT}
     Wait Until Page Contains                  รหัสไปรษณีย์
     Wait Until Element Is Visible             ${XPATH_FIELD_SHIPPING_POSTCODE}
     Capture Page Screenshot

TC_005_Validate Add Sell Order Shipping
     [Documentation]   ตรวจสอบ UX/UI ขั้นตอนการสร้างรายการขาย ในส่วนของ "ข้อมูลการจัดส่งสินค้า"
     Wait Until Page Contains                 ข้อมูลการจัดส่งสินค้า
     Wait Until Page Contains                 วันส่งสินค้า
     Wait Until Element Is Visible            ${XPATH_FIELD_SHIPPING_DATE}
     Wait Until Page Contains                 Tracking No.
     Wait Until Element Is Visible            ${XPATH_FIELD_SHIPPING_TRACKINGNO}
     Capture Page Screenshot

TC_006_Validate Add Sell Order Payment
     [Documentation]   ตรวจสอบ UX/UI ขั้นตอนการสร้างรายการขาย ในส่วนของ "การชำระเงิน"
     Wait Until Page Contains                 การชำระเงิน
     Wait Until Page Contains                 เพิ่มการชำระเงิน
     Wait Until Element Is Visible            ${XPATH_BUTTON_ADD_PAYMENT}
     Wait Until Page Contains                 ใช้สำหรับบันทึกการชำระเงิน เพื่อแสดงสถานะการชำระเงินของรายการขาย (ยอดขาย)
     Capture Page Screenshot

TC_007_Validate Add Sell Order Warehouse/Branch
     [Documentation]   ตรวจสอบ UX/UI ขั้นตอนการสร้างรายการขาย ในส่วนของ "คลังสินค้า/สาขา"
     Wait Until Page Contains                 คลังสินค้า/สาขา
     Wait Until Page Contains                 การโอนสินค้า 
     Wait Until Page Contains                 รอโอนสินค้า
     Click Element When Ready                 ${XPATH_RADIO_BUTTON_WAIT_SHIPPING} 
     Wait Until Element Is Visible            ${XPATH_DROPDOWN_WAIT_SHIPPING}            
     Wait Until Page Contains                 โอนทันทีออกจากคลังสินค้า
     Click Element When Ready                 ${XPATH_RADIO_BUTTON_SHIPPING_NOW}  
     Wait Until Element Is Visible            ${XPATH_DROPDOWN_SHIPPING_NOW}          
     Capture Page Screenshot

TC_008_Validate Add Sell Order Other
     [Documentation]   ตรวจสอบ UX/UI ขั้นตอนการสร้างรายการขาย ในส่วนของ "อื่นๆ"
     Wait Until Page Contains                 รายการขาย
     Wait Until Element Is Visible            ${XPATH_BUTTON_BACK_TO_ORDER_LIST_1}
     Wait Until Page Contains                 กลับ
     Wait Until Element Is Visible            ${XPATH_BUTTON_BACK_TO_ORDER_LIST_2}
     Wait Until Page Contains                 บันทึก + สร้างใหม่
     Wait Until Element Is Visible            ${XPATH_BUTTON_SAVE_AND_CREATE_ORDER}
     Wait Until Page Contains                 บันทึก
     Wait Until Element Is Visible            ${XPATH_BUTTON_SAVE_ORDER}
     Capture Page Screenshot