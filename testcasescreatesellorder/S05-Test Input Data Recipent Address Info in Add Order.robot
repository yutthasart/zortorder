*** Settings ***
Resource          ${CURDIR}/../resources/testdata/testdata.robot
Resource          ${CURDIR}/../resources/variables/variable_config.robot
Resource          ${CURDIR}/../resources/variables/variable_login.robot
Resource          ${CURDIR}/../resources/variables/variable_home.robot
Resource          ${CURDIR}/../resources/variables/variable_addsellorder.robot
Resource          ${CURDIR}/../keywords/common/common_keywords.robot
Suite Teardown    Close All Browsers


*** Test Cases ***
TC_108_Test Click Choose Product Button
     [Documentation]   ทดสอบกรอกข้อมูลลงใน text box ชื่อผู้รับ
     Open Browser      ${URL}      ${BROWSER}
     Set Window Size    1920   1080            
     Input Text To Element When Ready          ${XPATH_FIELD_USERNAME}       ${EMAIL}           
     Input Text To Element When Ready          ${XPATH_FIELD_PASSWORD}       ${PASSWORD}     
     Click Element When Ready                  ${XPATH_BUTTON_LOGIN}
     Sleep                                     10s
     ${popup1}  Run Keyword And Return Status    Element Should Be Visible         ${XPATH_CLOSE_POPUP_EXPIRE}
       Log  ${popup1}
       Run Keyword If    '${popup1}'=='True'       Click Element                     ${XPATH_CLOSE_POPUP_EXPIRE}
       Run Keyword If    '${popup1}'=='False'      Pop Up Not Avalible
     ${popup2}  Run Keyword And Return Status    Element Should Be Visible         ${XPATH_CLOSE_POPUP}
       Log  ${popup2}
       Run Keyword If    '${popup2}'=='True'       Click Element                     ${XPATH_CLOSE_POPUP}
       Run Keyword If    '${popup2}'=='False'      Pop Up Not Avalible
     # Wait Until Element Is Visible             ${XPATH_CLOSE_POPUP_EXPIRE}          20s
     # Click Element When Ready                  ${XPATH_CLOSE_POPUP_EXPIRE}
     # Wait Until Element Is Visible             ${XPATH_CLOSE_POPUP}                 20s
     # Click Element When Ready                  ${XPATH_CLOSE_POPUP}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU_ADD_ORDER}
     Input Text To Element When Ready          ${XPATH_FIELD_SHIPPING_NAME}           TestName TestSurname
     ${์Recipent Name}=                      Get Value From Element When Ready         ${XPATH_FIELD_SHIPPING_NAME}
     Should Be Equal                           ${์Recipent Name}                       TestName TestSurname
     Capture Page Screenshot                 
   
TC_109_Validate Choose Multi Product Modal
     [Documentation]   ทดสอบกรอกข้อมูลลงใน text box เบอร์โทรศัพท์ผู้รับ
     Input Text To Element When Ready          ${XPATH_FIELD_SHIPPING_PHONE}           084-228-9760
     ${์Recipent Phone}=                      Get Value From Element When Ready         ${XPATH_FIELD_SHIPPING_PHONE}
     Should Be Equal                           ${์Recipent Phone}                       084-228-9760
     Capture Page Screenshot                  

TC_110_Test Search Data In Multi Product Modal
     [Documentation]   ทดสอบกรอกข้อมูลลงใน text box อีเมลผู้รับ
     Input Text To Element When Ready          ${XPATH_FIELD_SHIPPING_EMAIL}           test@zortout.com
     ${์Recipent Email}=                      Get Value From Element When Ready         ${XPATH_FIELD_SHIPPING_EMAIL}
     Should Be Equal                           ${์Recipent Email}                       test@zortout.com
     Capture Page Screenshot 

TC_111_Test Swap Tab To Filter Data Product & Bundle and Validate Data In Choose Multi Product Modal
     [Documentation]   ทดสอบกรอกข้อมูลลงใน text box ที่อยู่/จัดส่ง
     Input Text To Element When Ready          ${XPATH_FIELD_SHIPPING_ADDRESS}           บริษัท ZORT เลขที่ 128/152 อาคารพญาไทพลาซ่า ชั้น 14 ถ.พญาไท แขวงทุ่งพญาไท เขตราชเทวี กรุงเทพ ฯ 10400
     ${์Recipent Address}=                      Get Value From Element When Ready         ${XPATH_FIELD_SHIPPING_ADDRESS}
     Should Be Equal                           ${์Recipent Address}                       บริษัท ZORT เลขที่ 128/152 อาคารพญาไทพลาซ่า ชั้น 14 ถ.พญาไท แขวงทุ่งพญาไท เขตราชเทวี กรุงเทพ ฯ 10400
     Capture Page Screenshot

TC_112_Test Click Button Copy Data From Customer
     [Documentation]   ทดสอบกดคัดลอกจากข้อมูลลูกค้า
     Input Text To Element When Ready          ${XPATH_FIELD_CUSTOMER_NAME}                 Testname1234 Testsurname1234
     Input Text To Element When Ready          ${XPATH_FIELD_CUSTOMER_CODE}                 C0002
     Input Text To Element When Ready          ${XPATH_FIELD_CUSTOMER_PHONE}                0842289760
     Input Text To Element When Ready          ${XPATH_FIELD_CUSTOMER_EMAIL}                test1234@gmail.com
     Input Text To Element When Ready          ${XPATH_FIELD_CUSTOMER_ADDRESS}              3/164 ซอย หมู่บ้านรัตนาธิเบศร์ 14/1 ตำบล บางรักพัฒนา อำเภอ บางบัวทอง จังหวัด นนทบุรี 11110
     Click Element When Ready                  ${XPATH_BUTTON_COPY_SHIPPING_INFO_FROM_CUSTOMER}
     ${์Recipent Name}=                         Get Value From Element When Ready         ${XPATH_FIELD_SHIPPING_NAME}
     Should Be Equal                           ${์Recipent Name}                          Testname1234 Testsurname1234
     ${์Recipent Phone}=                        Get Value From Element When Ready         ${XPATH_FIELD_SHIPPING_PHONE}
     Should Be Equal                           ${์Recipent Phone}                         0842289760
     ${์Recipent Email}=                        Get Value From Element When Ready         ${XPATH_FIELD_SHIPPING_EMAIL}
     Should Be Equal                           ${์Recipent Email}                         test1234@gmail.com
     ${์Recipent Address}=                      Get Value From Element When Ready         ${XPATH_FIELD_SHIPPING_ADDRESS}
     Should Be Equal                           ${์Recipent Address}                       3/164 ซอย หมู่บ้านรัตนาธิเบศร์ 14/1 ตำบล บางรักพัฒนา อำเภอ บางบัวทอง จังหวัด นนทบุรี 11110
     Capture Page Screenshot

TC_113_Test Click Button Validate Address
     [Documentation]   ทดสอบกดตรวจสอบที่อยู่
     Click Element When Ready                  ${XPATH_BUTTON_VALIDATE_SHIPPING_ADDRESS}
     ${Shipping Province}=                      Get Value From Element When Ready            ${XPATH_DROPDOWN_SHIPPING_PROVINCE}
     Should Be Equal                           ${Shipping Province}                          นนทบุรี
     ${Shipping District}=                      Get Value From Element When Ready            ${XPATH_DROPDOWN_SHIPPING_DISTRICT}
     Should Be Equal                           ${Shipping District}                          บางบัวทอง
     ${Shipping Sub District}=                  Get Value From Element When Ready            ${XPATH_DROPDOWN_SHIPPING_SUBDISTRICT}
     Should Be Equal                           ${Shipping Sub District}                      บางรักพัฒนา
     ${Shipping Postcode}=                      Get Value From Element When Ready            ${XPATH_FIELD_SHIPPING_POSTCODE}
     Should Be Equal                           ${Shipping Postcode}                          11110
     Capture Page Screenshot

TC_114_Test Choose Data In Dropdown Province
     [Documentation]   ทดสอบเลือกข้อมูลใน dropdown จังหวัด
     Click Element When Ready                  ${XPATH_DROPDOWN_SHIPPING_PROVINCE}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_PROVINCE}
     ${Shipping Province}=                      Get Value From Element When Ready            ${XPATH_DROPDOWN_SHIPPING_PROVINCE}
     Should Be Equal                           ${Shipping Province}                          เชียงราย
     Capture Page Screenshot

TC_115_Test Choose Data In Dropdown District
     [Documentation]   ทดสอบเลือกข้อมูลใน dropdown อำเภอ/เขต
     Click Element When Ready                  ${XPATH_DROPDOWN_SHIPPING_DISTRICT}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DISTRICT} 
     ${Shipping District}=                      Get Value From Element When Ready            ${XPATH_DROPDOWN_SHIPPING_DISTRICT}
     Should Be Equal                           ${Shipping District}                          บางใหญ่
     Capture Page Screenshot

TC_116_Test Choose Data In Dropdown Subdistrict
     [Documentation]   ทดสอบเลือกข้อมูลใน dropdown ตำบล/แขวง
     Click Element When Ready                  ${XPATH_DROPDOWN_SHIPPING_SUBDISTRICT}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_SUBDISTRICT}                   
     ${Shipping Sub District}=                 Get Value From Element When Ready            ${XPATH_DROPDOWN_SHIPPING_SUBDISTRICT}
     Should Be Equal                           ${Shipping Sub District}                      บางรักใหญ่
     Capture Page Screenshot

TC_117_Test Input Data In Textbox Postcode
     [Documentation]   ทดสอบกรอกข้อมูลลงใน textbox รหัสไปรษณีย์
     Clear Text When Ready                      ${XPATH_FIELD_SHIPPING_POSTCODE}
     Input Text To Element When Ready           ${XPATH_FIELD_SHIPPING_POSTCODE}             11130
     ${Shipping Postcode}=                      Get Value From Element When Ready            ${XPATH_FIELD_SHIPPING_POSTCODE}
     Should Be Equal                           ${Shipping Postcode}                          11130
     Capture Page Screenshot

TC_118_Test Choose Only Provice And Click Button Validate Address
     [Documentation]   ทดสอบกรอกเฉพาะจังหวัดแล้วกดปุ่มตรวจสอบที่อยู่
     Clear Text When Ready                     ${XPATH_FIELD_SHIPPING_ADDRESS}
     Input Text To Element When Ready          ${XPATH_FIELD_SHIPPING_ADDRESS}              3/164 นนทบุรี
     Click Element When Ready                  ${XPATH_BUTTON_VALIDATE_SHIPPING_ADDRESS}    
     ${Shipping Province}=                     Get Value From Element When Ready             ${XPATH_DROPDOWN_SHIPPING_PROVINCE}
     Should Be Equal                           ${Shipping Province}                          นนทบุรี
     ${Shipping District}=                     Get Value From Element When Ready             ${XPATH_DROPDOWN_SHIPPING_DISTRICT}
     Should Be Equal                           ${Shipping District}                          ${Empty}             
     ${Shipping Sub District}=                 Get Value From Element When Ready             ${XPATH_DROPDOWN_SHIPPING_SUBDISTRICT}
     Should Be Equal                           ${Shipping Sub District}                      ${Empty}
     Capture Page Screenshot

TC_119_Test Choose Only Provice & District And Click Button Validate Address 
     [Documentation]   ทดสอบกรอกเฉพาะ จังหวัด และ อำเภอ/เขต แล้วกดปุ่มตรวจสอบที่อยู่
     Clear Text When Ready                     ${XPATH_FIELD_SHIPPING_ADDRESS}
     Input Text To Element When Ready          ${XPATH_FIELD_SHIPPING_ADDRESS}              3/164 นนทบุรี บางบัวทอง
     Click Element When Ready                  ${XPATH_BUTTON_VALIDATE_SHIPPING_ADDRESS}    
     ${Shipping Province}=                     Get Value From Element When Ready             ${XPATH_DROPDOWN_SHIPPING_PROVINCE}
     Should Be Equal                           ${Shipping Province}                          นนทบุรี
     ${Shipping District}=                     Get Value From Element When Ready             ${XPATH_DROPDOWN_SHIPPING_DISTRICT}
     Should Be Equal                           ${Shipping District}                          บางบัวทอง            
     ${Shipping Sub District}=                 Get Value From Element When Ready             ${XPATH_DROPDOWN_SHIPPING_SUBDISTRICT}
     Should Be Equal                           ${Shipping Sub District}                      ${Empty}
     Capture Page Screenshot

TC_120_Test Choose Provice, District And Subdistrict And Click Button Validate Address
     [Documentation]   ทดสอบกรอก จังหวัด,อำเภอ/เขต และ ตำบลแขวง แล้วกดปุ่มตรวจสอบที่อยู่
     Clear Text When Ready                     ${XPATH_FIELD_SHIPPING_ADDRESS}
     Input Text To Element When Ready          ${XPATH_FIELD_SHIPPING_ADDRESS}              3/164 นนทบุรี บางบัวทอง บางรักพัฒนา
     Click Element When Ready                  ${XPATH_BUTTON_VALIDATE_SHIPPING_ADDRESS}    
     ${Shipping Province}=                     Get Value From Element When Ready            ${XPATH_DROPDOWN_SHIPPING_PROVINCE}
     Should Be Equal                           ${Shipping Province}                          นนทบุรี
     ${Shipping District}=                     Get Value From Element When Ready            ${XPATH_DROPDOWN_SHIPPING_DISTRICT}
     Should Be Equal                           ${Shipping District}                          บางบัวทอง            
     ${Shipping Sub District}=                 Get Value From Element When Ready            ${XPATH_DROPDOWN_SHIPPING_SUBDISTRICT}
     Should Be Equal                           ${Shipping Sub District}                      บางรักพัฒนา
     Capture Page Screenshot