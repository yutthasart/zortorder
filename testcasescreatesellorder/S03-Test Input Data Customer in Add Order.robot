*** Settings ***
Resource          ${CURDIR}/../resources/testdata/testdata.robot
Resource          ${CURDIR}/../resources/variables/variable_config.robot
Resource          ${CURDIR}/../resources/variables/variable_login.robot
Resource          ${CURDIR}/../resources/variables/variable_home.robot
Resource          ${CURDIR}/../resources/variables/variable_addsellorder.robot
Resource          ${CURDIR}/../keywords/common/common_keywords.robot
Suite Teardown    Close All Browsers


*** Test Cases ***
TC_028_Test Input Data In Textbox Customer Name
     [Documentation]   ทดสอบกรอกข้อมูลใน text box ชื่อลูกค้า
     Open Browser      ${URL}      ${BROWSER}
     Set Window Size    1920   1080            
     Input Text To Element When Ready          ${XPATH_FIELD_USERNAME}       ${EMAIL}           
     Input Text To Element When Ready          ${XPATH_FIELD_PASSWORD}       ${PASSWORD}     
     Click Element When Ready                  ${XPATH_BUTTON_LOGIN}
     Sleep                                     10s
     ${popup1}  Run Keyword And Return Status    Element Should Be Visible         ${XPATH_CLOSE_POPUP_EXPIRE}
       Log  ${popup1}
       Run Keyword If    '${popup1}'=='True'       Click Element                     ${XPATH_CLOSE_POPUP_EXPIRE}
       Run Keyword If    '${popup1}'=='False'      Pop Up Not Avalible
     ${popup2}  Run Keyword And Return Status    Element Should Be Visible         ${XPATH_CLOSE_POPUP}
       Log  ${popup2}
       Run Keyword If    '${popup2}'=='True'       Click Element                     ${XPATH_CLOSE_POPUP}
       Run Keyword If    '${popup2}'=='False'      Pop Up Not Avalible
     # Wait Until Element Is Visible             ${XPATH_CLOSE_POPUP_EXPIRE}          20s
     # Click Element When Ready                  ${XPATH_CLOSE_POPUP_EXPIRE}
     # Wait Until Element Is Visible             ${XPATH_CLOSE_POPUP}                 20s
     # Click Element When Ready                  ${XPATH_CLOSE_POPUP}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU_ADD_ORDER}
     Input Text To Element When Ready          ${XPATH_FIELD_CUSTOMER_NAME}      ยุทธศาสตร์ ถนอมมิตรวัฒนา
     Capture Page Screenshot
   
TC_029_Test Input Customer Name (Alreay In Shop) In Textbox Customer Name
     [Documentation]   ทดสอบกรอก รหัส หรือ ชื่อลูกค้าที่มีอยู่แล้วใน text box ชื่อลูกค้า
     Clear Text When Ready                     ${XPATH_FIELD_CUSTOMER_NAME}
     Input Text To Element When Ready          ${XPATH_FIELD_CUSTOMER_NAME}      ข้อมูล
     Wait Until Page Contains                  C0002
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 2
     Capture Page Screenshot

TC_030_Test Choose Customer Data From Auto Search In Textbox Customer Name
     [Documentation]   ทดสอบเลือกรหัส หรือ ชื่อลูกค้าที่มาจาก auto search
     Sleep                                     5s
     Click Element When Ready                  ${XPATH_TEXT_SAMPLE_CUSTOMER_NAME}
     Capture Page Screenshot

TC_031_Test Click Choose Customer Button
     [Documentation]   ทดสอบกดที่ icon ปุ่มเลือกผู้ติดต่อ
     Click Element When Ready                  ${XPATH_BUTTON_CUSTOMER_CHOOSE_CUSTOMER}
     Wait Until Page Contains                  เลือกผู้ติดต่อ
     Capture Page Screenshot

TC_032_Validate Choose Customer Modal
     [Documentation]   ทดสอบกดที่ icon ปุ่มเลือกผู้ติดต่อ
     Wait Until Element Is Visible             ${XPATH_FIELD_SEARCH_CUSTOMER_IN_CHOSSE_CUSTOMER_MODAL}
     Wait Until Element Is Visible             ${XPATH_TAB_ALL_IN_CHOOSE_CUSTOMER_MODAL}
     Wait Until Element Is Visible             ${XPATH_TAB_CUSTOMER_IN_CHOOSE_CUSTOMER_MODAL}
     Wait Until Page Contains                  ชื่อ
     Wait Until Page Contains                  เบอร์โทรศัพท์
     Wait Until Page Contains                  อีเมล
     Wait Until Page Contains                  เลขผู้เสียภาษี
     Wait Until Element Is Visible             ${XPATH_ICON_FACEBOOK_IN_CHOOSE_CUSTOMER_MODAL}
     Wait Until Element Is Visible             ${XPATH_ICON_LINE_IN_CHOOSE_CUSTOMER_MODAL}
     Wait Until Element Is Visible             ${XPATH_BUTTON_CUSTOMER_CHOOSE_SAMPLE_CUSTOMER}
     Capture Page Screenshot

TC_033_Test Search Data In Choose Customer Modal
     [Documentation]   ทดสอบค้นหาข้อมูลผู้ติดต่อใน modal เลือกผู้ติดต่อ
     Input Text To Element When Ready          ${XPATH_FIELD_SEARCH_CUSTOMER_IN_CHOSSE_CUSTOMER_MODAL}      C0001
     Press Keys                                ${XPATH_FIELD_SEARCH_CUSTOMER_IN_CHOSSE_CUSTOMER_MODAL}      ENTER
     Wait Until Page Contains                  C0001
     Capture Page Screenshot

TC_034_Test Swap Tab All and Customer
     [Documentation]   ทดสอบสลับ tab สำหรับ filter ข้อมูลทั้งหมด และ ลูกค้า แล้วตรวจสอบความถูกต้องของข้อมูล
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_TAB_CUSTOMER_IN_CHOOSE_CUSTOMER_MODAL}
     Wait Until Page Contains                  ไม่มีข้อมูล
     Capture Page Screenshot
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_TAB_ALL_IN_CHOOSE_CUSTOMER_MODAL}
     Wait Until Page Contains                  C0001
     Capture Page Screenshot

TC_035_Test Choose Customer In Choose Customer Modal
     [Documentation]   ทดสอบกดปุ่ม เลือก เพื่อเลือกผู้ติดต่อใน modal เลือกผู้ติดต่อ
     Click Element When Ready                  ${XPATH_BUTTON_CUSTOMER_CHOOSE_SAMPLE_CUSTOMER}
     Sleep                                     3s
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 1
     Wait Until Page Contains                  C0001
     Wait Until Page Contains                  0812345678
     Wait Until Page Contains                  you@zortshop.com
     ${ADDRESS_CUSTOMER}=                      Get Value From Element When Ready           ${XPATH_FIELD_CUSTOMER_ADDRESS}
     Should Be Equal                           ${ADDRESS_CUSTOMER}                         123 วัดชลอ บางกรวย นนทบุรี 11130
     Capture Page Screenshot

TC_036_Test Input Data In Textbox Customer Code
     [Documentation]   ทดสอบกรอกรหัสลูกค้า
     Clear Text When Ready                     ${XPATH_FIELD_CUSTOMER_CODE}
     Input Text To Element When Ready          ${XPATH_FIELD_CUSTOMER_CODE}                C0002
     Capture Page Screenshot

TC_037_Test Input Data In Textbox Customer Phone (10 Digit)
     [Documentation]   ทดสอบกรอกเบอร์โทรศัพท์โดยใช้ format โทรศัพท์ มือถือ (10 หลัก)
     Clear Text When Ready                     ${XPATH_FIELD_CUSTOMER_PHONE}
     Input Text To Element When Ready          ${XPATH_FIELD_CUSTOMER_PHONE}                0842289760
     Capture Page Screenshot
     
TC_038_Test Input Data In Textbox Customer Phone (9 Digit)
     [Documentation]   ทดสอบกรอกเบอร์โทรศัพท์โดยใช้ format โทรศัพท์ บ้าน (9 หลัก)
     Clear Text When Ready                     ${XPATH_FIELD_CUSTOMER_PHONE}
     Input Text To Element When Ready          ${XPATH_FIELD_CUSTOMER_PHONE}                023782183
     Capture Page Screenshot

TC_039_Test Input Data In Textbox Customer Phone (11 Digit)
      [Documentation]   ทดสอบกรอกเบอร์โทรศัพท์ เกิน 10 หลัก
     Clear Text When Ready                     ${XPATH_FIELD_CUSTOMER_PHONE}
     Input Text To Element When Ready          ${XPATH_FIELD_CUSTOMER_PHONE}                08422897600
     Capture Page Screenshot

TC_040_Test Input Data In Textbox Customer Phone (8 Digit)
     [Documentation]   ทดสอบกรอกเบอร์โทรศัพท์ น้อยกว่า 9 หลัก
     Clear Text When Ready                     ${XPATH_FIELD_CUSTOMER_PHONE}
     Input Text To Element When Ready          ${XPATH_FIELD_CUSTOMER_PHONE}                02378218
     Capture Page Screenshot

TC_041_Test Input Data In Textbox Customer Phone (Alphanumeric)
     [Documentation]   ทดสอบกรอกเบอร์โทรศัพท์ด้วยตัวเลขผสมตัวอักษร
     Clear Text When Ready                     ${XPATH_FIELD_CUSTOMER_PHONE}
     Input Text To Element When Ready          ${XPATH_FIELD_CUSTOMER_PHONE}                023782183 ต่อ 115
     Capture Page Screenshot

TC_042_Test Input Data In Textbox Customer Email (Normal xxx@xxx.x)
     [Documentation]   ทดสอบกรอกอีเมลลูกค้าโดยใช้ format email ปกติ
     Clear Text When Ready                     ${XPATH_FIELD_CUSTOMER_EMAIL}
     Input Text To Element When Ready          ${XPATH_FIELD_CUSTOMER_EMAIL}                test1234@gmail.com
     Capture Page Screenshot

TC_043_Test Input Data In Textbox Customer Email (Abnormal xxx.x)
     [Documentation]   ทดสอบกรอกอีเมลลูกค้าโดยไม่ใช้ format email 
     Clear Text When Ready                     ${XPATH_FIELD_CUSTOMER_EMAIL}
     Input Text To Element When Ready          ${XPATH_FIELD_CUSTOMER_EMAIL}                test1234.com
     Capture Page Screenshot

TC_044_Test Input Data In Textbox Customer Email (Other Language)
     [Documentation]   ทดสอบกรอกอีเมลลูกค้าโดยใช้ ภาษาไทย
     Clear Text When Ready                     ${XPATH_FIELD_CUSTOMER_EMAIL}
     Input Text To Element When Ready          ${XPATH_FIELD_CUSTOMER_EMAIL}                ยุทธศาสตร์@zortout.com
     Capture Page Screenshot


TC_045_Test Input Data In Textbox Customer Address
     [Documentation]   ทดสอบกรอกที่อยู่ลูกค้า
     Clear Text When Ready                     ${XPATH_FIELD_CUSTOMER_ADDRESS}
     Input Text To Element When Ready          ${XPATH_FIELD_CUSTOMER_ADDRESS}              3/164 ซอย หมู่บ้านรัตนาธิเบศร์ 14/1 ตำบล บางรักพัฒนา อำเภอ บางบัวทอง จังหวัด นนทบุรี 11110
     Capture Page Screenshot


TC_046_Test Choose Check Box For Validate Customer ID Number, Customer Branch Name, Customer Branch Number
     [Documentation]   ทดสอบกดที่ check box กำหนดเลขผู้เสียภาษี, ชื่อสาขา, เลขที่สาขา
     Click Element When Ready                  ${XPATH_CHECKBOX_CUSTOMER_MERCHANT}
     Wait Until Page Contains                  เลขประจำตัวผู้เสียภาษี
     Wait Until Element Is Visible             ${XPATH_FIELD_CUSTOMER_ID_NUMBER}
     Wait Until Page Contains                  ชื่อสาขา
     Wait Until Element Is Visible             ${XPATH_FIELD_CUSTOMER_BRANCH_NAME}
     Wait Until Page Contains                  เลขที่สาขา
     Wait Until Element Is Visible             ${XPATH_FIELD_CUSTOMER_BRANCH_NO}
     Capture Page Screenshot

TC_047_Test Input Data In Textbox Customer ID Number (10 and 13 digit)
     [Documentation]   ทดสอบกรอกเลขประจำตัวผู้เสียภาษี 10 และ 13 หลัก
     Clear Text When Ready                     ${XPATH_FIELD_CUSTOMER_ID_NUMBER}
     Input Text To Element When Ready          ${XPATH_FIELD_CUSTOMER_ID_NUMBER}              4929394959
     Capture Page Screenshot
     Clear Text When Ready                     ${XPATH_FIELD_CUSTOMER_ID_NUMBER}
     Input Text To Element When Ready          ${XPATH_FIELD_CUSTOMER_ID_NUMBER}              4959693959943
     Capture Page Screenshot

TC_048_Test Input Data In Textbox Customer ID Number (9 digit)
     [Documentation]   ทดสอบกรอกเลขประจำตัวผู้เสียภาษีน้อยกว่า 10 หลัก
     Clear Text When Ready                     ${XPATH_FIELD_CUSTOMER_ID_NUMBER}
     Input Text To Element When Ready          ${XPATH_FIELD_CUSTOMER_ID_NUMBER}                528385848
     Capture Page Screenshot

TC_049_Test Input Data In Textbox Customer ID Number (15 digit)
     [Documentation]   ทดสอบกรอกเลขประจำตัวผู้เสียภาษีเกิน 13 หลัก
     Clear Text When Ready                     ${XPATH_FIELD_CUSTOMER_ID_NUMBER}
     Input Text To Element When Ready          ${XPATH_FIELD_CUSTOMER_ID_NUMBER}                584929495969293
     Capture Page Screenshot

TC_050_Test Input Data In Textbox Customer Branch Name
     [Documentation]   ทดสอบกรอกชื่อสาขา
     Clear Text When Ready                     ${XPATH_FIELD_CUSTOMER_BRANCH_NAME}
     Input Text To Element When Ready          ${XPATH_FIELD_CUSTOMER_BRANCH_NAME}            zortout สาขา พญาไท
     Capture Page Screenshot

TC_051_Test Input Data In Textbox Customer Branch Number
     [Documentation]   ทดสอบกรอกเลขที่สาขา
     Clear Text When Ready                     ${XPATH_FIELD_CUSTOMER_BRANCH_NO}
     Input Text To Element When Ready          ${XPATH_FIELD_CUSTOMER_BRANCH_NO}              z0134
     Capture Page Screenshot