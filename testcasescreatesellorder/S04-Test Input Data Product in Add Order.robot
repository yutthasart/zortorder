*** Settings ***
Resource          ${CURDIR}/../resources/testdata/testdata.robot
Resource          ${CURDIR}/../resources/variables/variable_config.robot
Resource          ${CURDIR}/../resources/variables/variable_login.robot
Resource          ${CURDIR}/../resources/variables/variable_home.robot
Resource          ${CURDIR}/../resources/variables/variable_addsellorder.robot
Resource          ${CURDIR}/../keywords/common/common_keywords.robot
Suite Teardown    Close All Browsers


*** Test Cases ***
TC_052_Test Click Choose Product Button
     [Documentation]   ทดสอบกดปุ่มเลือกสินค้า
     Open Browser      ${URL}      ${BROWSER}
     Set Window Size    1920   1080            
     Input Text To Element When Ready          ${XPATH_FIELD_USERNAME}       ${EMAIL}           
     Input Text To Element When Ready          ${XPATH_FIELD_PASSWORD}       ${PASSWORD}     
     Click Element When Ready                  ${XPATH_BUTTON_LOGIN}
     Sleep                                     10s
     ${popup1}  Run Keyword And Return Status    Element Should Be Visible         ${XPATH_CLOSE_POPUP_EXPIRE}
       Log  ${popup1}
       Run Keyword If    '${popup1}'=='True'       Click Element                     ${XPATH_CLOSE_POPUP_EXPIRE}
       Run Keyword If    '${popup1}'=='False'      Pop Up Not Avalible
     ${popup2}  Run Keyword And Return Status    Element Should Be Visible         ${XPATH_CLOSE_POPUP}
       Log  ${popup2}
       Run Keyword If    '${popup2}'=='True'       Click Element                     ${XPATH_CLOSE_POPUP}
       Run Keyword If    '${popup2}'=='False'      Pop Up Not Avalible
     # Wait Until Element Is Visible             ${XPATH_CLOSE_POPUP_EXPIRE}          20s
     # Click Element When Ready                  ${XPATH_CLOSE_POPUP_EXPIRE}
     # Wait Until Element Is Visible             ${XPATH_CLOSE_POPUP}                 20s
     # Click Element When Ready                  ${XPATH_CLOSE_POPUP}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU_ADD_ORDER}
     Click Element When Ready                  ${XPATH_ฺBUTTON_MULTICHOOSE_PRODUCT}
     Wait Until Page Contains                  เลือกสินค้าหลายรายการ
     Capture Page Screenshot
   
TC_053_Validate Choose Multi Product Modal
     [Documentation]   ตรวจสอบ modal เลือกสินค้าหลายรายการ
     Wait Until Element Is Visible             ${XPATH_ฺFIELD_SEARCH_MULTIPRODUCT}
     Wait Until Element Is Visible             ${XPATH_BUTTON_CHOOSE_MULTIPRODUCT}
     Wait Until Element Is Visible             ${XPATH_TAB_PRODUCT_FOR_CHOOSE_MULTIPRODUCT}
     Wait Until Element Is Visible             ${XPATH_TAB_BUNDLE_FOR_CHOOSE_MULTIPRODUCT}
     Wait Until Element Is Visible             ${XPATH_CHECKBOX_FOR_CHOOSE_ALL_MULTIPRODUCT}
     Wait Until Element Is Visible             ${XPATH_CHECKBOX_FOR_CHOOSE_MULTIPRODUCT_ROW_1}
     Wait Until Element Is Visible             ${XPATH_PICTURE_MULTIPRODUCT_ROW_1}
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 7(M) 
     Wait Until Page Contains                  P0001-2
     Wait Until Page Contains                  คงเหลือ	
     Wait Until Page Contains                  พร้อมขาย
     Wait Until Page Contains                  ราคาขาย
     Capture Page Screenshot

TC_054_Test Search Data In Multi Product Modal
     [Documentation]   ทดสอบค้นหาข้อมูลสินค้า
     Clear Text When Ready                     ${XPATH_ฺFIELD_SEARCH_MULTIPRODUCT}
     Input Text To Element When Ready          ${XPATH_ฺFIELD_SEARCH_MULTIPRODUCT}             P0007
     Press Keys                                ${XPATH_ฺFIELD_SEARCH_MULTIPRODUCT}             ENTER
     Sleep                                     3s
     Wait Until Page Contains                  P0007
     Sleep                                     3s
     Wait Until Page Does Not Contain          P0006
     Capture Page Screenshot

TC_055_Test Swap Tab To Filter Data Product & Bundle and Validate Data In Choose Multi Product Modal
     [Documentation]   ทดสอบสลับ tab สำหรับ filter ข้อมูลสินค้า และ สินค้าเป็นชุด แล้วตรวจสอบความถูกต้องของข้อมูล
     Click Element When Ready                  ${XPATH_TAB_BUNDLE_FOR_CHOOSE_MULTIPRODUCT}
     Wait Until Page Contains                  ไม่มีข้อมูล
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_TAB_PRODUCT_FOR_CHOOSE_MULTIPRODUCT}
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 6
     Capture Page Screenshot

TC_056_Test Check Box All
     [Documentation]   ทดสอบเลือก check box สินค้าทั้งหมด
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_CHECKBOX_FOR_CHOOSE_ALL_MULTIPRODUCT}
     Capture Page Screenshot

TC_057_Test Uncheck Box All
     [Documentation]   ทดสอบเลือก check box สินค้าทั้งหมดออก
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_CHECKBOX_FOR_CHOOSE_ALL_MULTIPRODUCT}
     Capture Page Screenshot

TC_058_Test Check Box
     [Documentation]   ทดสอบเลือก check box สินค้าทีละชนิด
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_CHECKBOX_FOR_CHOOSE_MULTIPRODUCT_ROW_1}
     Capture Page Screenshot

TC_059_Test Uncheck Box
     [Documentation]   ทดสอบเลือก check box สินค้าทีละชนิดออก
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_CHECKBOX_FOR_CHOOSE_MULTIPRODUCT_ROW_1}
     Capture Page Screenshot

TC_060_Test Click Text Link Remain In Choose Multi Product Modal
     [Documentation]   ทดสอบกดที่จำนวนคงเหลือ
     Click Element When Ready                  ${XPATH_TEXTLINK_REMAIN_PRODUCT_IN_MULTI_CHOOSE_PRODUCT_MODAL}
     Wait Until Page Contains                  รหัส
     Wait Until Page Contains                  ชื่อคลัง/สาขา
     Wait Until Page Contains                  จำนวนคงเหลือ	
     Wait Until Page Contains                  จำนวนพร้อมขาย
     Capture Page Screenshot

TC_061_Test Click Text Link Ready For Sale In Choose Multi Product Modal
     [Documentation]   ทดสอบกดที่จำนวนพร้อมขาย
     Click Element When Ready                  ${XPATH_BUTTON_CLOSE_REMAIN_AND_READY_FOR_SALE_PRODUCT_IN_MULTI_CHOOSE_PRODUCT_MODAL}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_TEXTLINK_READY_FOR_SALE_PRODUCT_IN_MULTI_CHOOSE_PRODUCT_MODAL}
     Wait Until Page Contains                  รหัส
     Wait Until Page Contains                  ชื่อคลัง/สาขา
     Wait Until Page Contains                  จำนวนคงเหลือ	
     Wait Until Page Contains                  จำนวนพร้อมขาย
     Capture Page Screenshot
     Click Element When Ready                  ${XPATH_BUTTON_CLOSE_REMAIN_AND_READY_FOR_SALE_PRODUCT_IN_MULTI_CHOOSE_PRODUCT_MODAL}

TC_062_Test Check Box Product In Modal And Click Choose Button In Multi Choose Product Modal
     [Documentation]   ทดสอบเลือกสินค้าตาม check box ใน modal เลือกสินค้าหลายรายการ แล้วกดปุ่มเลือกสินค้า
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_CHECKBOX_FOR_CHOOSE_MULTIPRODUCT_ROW_1}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_CHOOSE_MULTIPRODUCT}
     ${Product Code}=                          Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_CODE2}
     Should Be Equal                           ${Product Code}                           P0001-2
     Capture Page Screenshot

TC_063_Test Check Box Product In Modal And Click Choose Button In Multi Choose Product Modal (Bundle)
     [Documentation]   ทดสอบเลือกสินค้าตาม check box ใน modal เลือกสินค้าหลายรายการ แล้วกดปุ่มเลือกสินค้า (สินค้าเป็นชุด)
     Click Element When Ready                  ${XPATH_ฺBUTTON_MULTICHOOSE_PRODUCT}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_TAB_BUNDLE_FOR_CHOOSE_MULTIPRODUCT}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_CHECKBOX_FOR_CHOOSE_MULTIPRODUCT_ROW_1}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_CHOOSE_MULTIPRODUCT}
     ${Product Code}=                          Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_CODE3}
     Should Be Equal                           ${Product Code}                           SET0002
     Capture Page Screenshot

TC_064_Test Click Choose Product Button
     [Documentation]   ทดสอบกดปุ่มเลือก
     Click Element When Ready                  ${XPATH_BUTTON_CHOOSE_PRODUCT}
     Wait Until Page Contains                  เลือกสินค้า
     Capture Page Screenshot

TC_065_Validate Choose Product Modal
     [Documentation]   ตรวจสอบ modal เลือกสินค้า
     Wait Until Element Is Visible             ${XPATH_ฺFIELD_SEARCH_PRODUCT}
     Wait Until Page Contains                  สินค้า
     Wait Until Page Contains                  สินค้าเป็นชุด
     Wait Until Element Is Visible             ${XPATH_PICTURE_PRODUCT_ROW_1}
     Wait Until Page Contains                  คงเหลือ
     Wait Until Page Contains                  พร้อมขาย
     Wait Until Page Contains                  ราคาขาย
     Wait Until Element Is Visible             ${XPATH_BUTTON_CHOOSE_PRODUCT_ROW_1}
     Capture Page Screenshot

TC_066_Test Search Product Data In Choose Product Modal
     [Documentation]   ทดสอบค้นหาข้อมูลสินค้า
     Clear Text When Ready                     ${XPATH_ฺFIELD_SEARCH_PRODUCT}
     Input Text To Element When Ready          ${XPATH_ฺFIELD_SEARCH_PRODUCT}        P0007
     Press Keys                                ${XPATH_ฺFIELD_SEARCH_PRODUCT}        ENTER  
     Sleep                                     3s 
     Wait Until Page Contains                  P0007
     Sleep                                     3s
     Wait Until Page Does Not Contain Element          ${XPATH_ELEMENT_SAMPLE_ROW_2_IN_CHOOSE_PRODUCT_MODAL}         
     Capture Page Screenshot

TC_067_Test Swap Tab To Filter Data Product & Bundle and Validate Data In Choose Product Modal
     [Documentation]   ทดสอบสลับ tab สำหรับ filter ข้อมูลสินค้า และ สินค้าเป็นชุด แล้วตรวจสอบความถูกต้องของข้อมูล
     Click Element When Ready                  ${XPATH_TAB_BUNDLE_FOR_CHOOSE_PRODUCT}
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 1
     Wait Until Page Contains                  SET0001
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 2
     Wait Until Page Contains                  SET0002
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_TAB_PRODUCT_FOR_CHOOSE_PRODUCT}
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 6
     Capture Page Screenshot

TC_068_Test Click Text Link Remain In Choose Product Modal
     [Documentation]   ทดสอบกดที่จำนวนคงเหลือ
     Click Element When Ready                  ${XPATH_TEXTLINK_REMAIN_PRODUCT_IN_CHOOSE_PRODUCT_MODAL}
     Wait Until Page Contains                  รหัส
     Wait Until Page Contains                  ชื่อคลัง/สาขา
     Wait Until Page Contains                  จำนวนคงเหลือ	
     Wait Until Page Contains                  จำนวนพร้อมขาย
     Capture Page Screenshot

TC_069_Test Click Text Link Ready For Sale In Choose Product Modal
     [Documentation]   ทดสอบกดที่จำนวนพร้อมขาย
     Click Element When Ready                  ${XPATH_BUTTON_CLOSE_REMAIN_AND_READY_FOR_SALE_PRODUCT_IN_CHOOSE_PRODUCT_MODAL}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_TEXTLINK_READY_FOR_SALE_PRODUCT_IN_CHOOSE_PRODUCT_MODAL}
     Wait Until Page Contains                  รหัส
     Wait Until Page Contains                  ชื่อคลัง/สาขา
     Wait Until Page Contains                  จำนวนคงเหลือ	
     Wait Until Page Contains                  จำนวนพร้อมขาย
     Capture Page Screenshot

TC_070_Test Choose Product In Choose Product Modal
     [Documentation]   ทดสอบกดปุ่มเลือกข้างหลังรายการสินค้าที่ต้องการ (สินค้าปกติ)
     Click Element When Ready                  ${XPATH_BUTTON_CLOSE_REMAIN_AND_READY_FOR_SALE_PRODUCT_IN_CHOOSE_PRODUCT_MODAL}
     Sleep                                     3s
     Clear Text When Ready                     ${XPATH_ฺFIELD_SEARCH_PRODUCT}
     Input Text To Element When Ready          ${XPATH_ฺFIELD_SEARCH_PRODUCT}        P0007
     Press Keys                                ${XPATH_ฺFIELD_SEARCH_PRODUCT}        ENTER 
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_CHOOSE_PRODUCT_ROW_1}
     ${Product Code}=                          Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_CODE}
     Should Be Equal                           ${Product Code}                           P0007
     Capture Page Screenshot

TC_071_Test Choose Bundle In Choose Product Modal
     [Documentation]   ทดสอบกดปุ่มเลือกข้างหลังรายการสินค้าที่ต้องการ (สินค้าเป็นชุด)
     Click Element When Ready                  ${XPATH_BUTTON_CHOOSE_PRODUCT3}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_TAB_BUNDLE_FOR_CHOOSE_PRODUCT}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_CHOOSE_PRODUCT_ROW_2}
     ${Product Code}=                          Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_CODE3}
     Should Be Equal                           ${Product Code}                           SET0001
     Capture Page Screenshot
     Sleep                                     3s
     Scroll Element Into View                  ${XPATH_BUTTON_DELETE_PRODUCT3}
     Click Element When Ready                  ${XPATH_BUTTON_DELETE_PRODUCT3}

TC_072_Test Input Data In Product Code Text Box
     [Documentation]   ทดสอบกรอกรหัสสินค้าลงใน text box รหัส
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_CODE}
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_CODE}        TESTPRODUCTCODE01
     Click Element When Ready                  ${XPATH_FIELD_PRODUCT_NAME}
     Capture Page Screenshot

TC_073_Test Input Data In Product Code Text Box 3 Digit For Validate Auto Search
     [Documentation]   ทดสอบกรอกรหัสขึ้นต้นของสินค้าที่มีอยู่แล้วในระบบ 3 ตัวอักษร ลงใน text box รหัส เพื่อทดสอบระบบ auto search
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_CODE}
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_CODE}        P00
     Sleep                                     5s
     Wait Until Page Contains                  P0001-2
     Wait Until Page Contains                  P0001-1
     Wait Until Page Contains                  P0007
     Wait Until Page Contains                  P0006
     Capture Page Screenshot

TC_074_Test Choose Data In Auto Search Dropdown (PRODUCT CODE TEXT BOX)
     [Documentation]   ทดสอบเลือกข้อมูลใน dropdown ของช่อง text box รหัส
     Click Element When Ready                  ${XPATH_SAMPLE_CODE_IN_DROPDOWN_AUTO_SEARCH}
     ${Product Code}=                          Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_CODE}
     Should Be Equal                           ${Product Code}                           P0003
     Capture Page Screenshot

TC_075_Test Input Data In Product Code Text Box
     [Documentation]   ทดสอบกรอกชื่อสินค้าลงใน text box ชื่อสินค้า
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_NAME}
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_NAME}        TESTPRODUCTNAME01
     Click Element When Ready                  ${XPATH_FIELD_PRODUCT_CODE}
     Capture Page Screenshot

TC_076_Test Input Already Data In Textbox Product Name
     [Documentation]   ทดสอบกรอกชื่อขึ้นต้นของสินค้าที่มีอยู่แล้วในระบบลงใน text box รหัส เพื่อทดสอบระบบ auto search
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_NAME}
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_NAME}        ข้อมูล
     Sleep                                     10s
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 6
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 5
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 4
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 3
     Capture Page Screenshot

TC_077_Choose Data In Auto Search Dropdown (PRODUCT NAME TEXT BOX)
     [Documentation]   ทดสอบเลือกข้อมูลใน dropdown ของช่อง text box ชื่อสินค้า
     Click Element When Ready                  ${XPATH_SAMPLE_PRODUCT_NAME_IN_DROPDOWN_AUTO_SEARCH}
     ${Product Name}=                          Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_NAME}
     Should Be Equal                           ${Product Name}                           ข้อมูลตัวอย่าง 2
     Capture Page Screenshot

TC_078_Test Input Integer In Quantity Text Box
     [Documentation]   ทดสอบกรอกจำนวนเต็มลงใน text box จำนวน
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_QUANTITY}
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_QUANTITY}        50
     Click Element When Ready                  ${XPATH_FIELD_PRODUCT_NAME}
     ${Product Quantity}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_QUANTITY}
     Should Be Equal                           ${Product Quantity}                           50.00
     Capture Page Screenshot

TC_079_Test Input Decimal In Quantity Text Box
     [Documentation]   ทดสอบกรอกจำนวนที่มีจุดทศนิยมลงใน text box จำนวน
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_QUANTITY}
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_QUANTITY}        50.50
     Click Element When Ready                  ${XPATH_FIELD_PRODUCT_NAME}
     ${Product Quantity}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_QUANTITY}
     Should Be Equal                           ${Product Quantity}                           50.50
     Capture Page Screenshot

TC_080_Test Input Negative Number In Quantity Text Box
     [Documentation]   ทดสอบกรอกจำนวนติดลบลงใน text box จำนวน
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_QUANTITY}
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_QUANTITY}        -6
     Click Element When Ready                  ${XPATH_FIELD_PRODUCT_NAME}
     ${Product Quantity}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_QUANTITY}
     Should Be Equal                           ${Product Quantity}                           -6.00
     Capture Page Screenshot

TC_081_Test Input Text In Quantity Text Box
     [Documentation]   ทดสอบกรอกตัวอักษรลงในช่อง text box จำนวน
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_QUANTITY}
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_QUANTITY}        test1234
     Click Element When Ready                  ${XPATH_FIELD_PRODUCT_NAME}
     ${Product Quantity}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_QUANTITY}
     Should Be Equal                           ${Product Quantity}                    ${EMPTY}                      
     Capture Page Screenshot

TC_082_Test Input Integer In Price Text Box
     [Documentation]   ทดสอบกรอกจำนวนเต็มลงใน text box มูลค่าต่อหน่วย
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_PRICE}
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_PRICE}        10000
     Click Element When Ready                  ${XPATH_FIELD_PRODUCT_DISCOUNT}
     ${Product Price}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_PRICE}
     Should Be Equal                           ${Product Price}                    10,000.00                      
     Capture Page Screenshot

TC_083_Test Decimal Text In Price Text Box
     [Documentation]   ทดสอบกรอกจำนวนที่มีจุดทศนิยมลงใน text box มูลค่าต่อหน่วย
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_PRICE}
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_PRICE}        10000.52
     Click Element When Ready                  ${XPATH_FIELD_PRODUCT_DISCOUNT}
     ${Product Price}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_PRICE}
     Should Be Equal                           ${Product Price}                    10,000.52                      
     Capture Page Screenshot

TC_084_Test Input Negative Number In Price Text Box
     [Documentation]   ทดสอบกรอกจำนวนติดลบลงใน text box มูลค่าต่อหน่วย
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_PRICE}
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_PRICE}        -10000
     Click Element When Ready                  ${XPATH_FIELD_PRODUCT_DISCOUNT}
     ${Product Price}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_PRICE}
     Should Be Equal                           ${Product Price}                    -10,000.00                      
     Capture Page Screenshot

TC_085_Test Input Text In Price Text Box
     [Documentation]   ทดสอบกรอกตัวอักษรลงในช่อง text box มูลค่าต่อหน่วย
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_PRICE}
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_PRICE}        test1234
     Click Element When Ready                  ${XPATH_FIELD_PRODUCT_DISCOUNT}
     ${Product Price}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_PRICE}
     Should Be Equal                           ${Product Price}                    ${EMPTY}                      
     Capture Page Screenshot

TC_086_Test Input Integer In Discount Text Box
     [Documentation]   ทดสอบกรอกจำนวนเต็มลงใน text box ส่วนลดต่อหน่วย
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_DISCOUNT}
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_DISCOUNT}        10000
     Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
     ${Product Discount}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_DISCOUNT}
     Should Be Equal                           ${Product Discount}                    10,000.00                      
     Capture Page Screenshot

TC_087_Test Decimal Text In Discount Text Box
     [Documentation]   ทดสอบกรอกจำนวนที่มีจุดทศนิยมลงใน text box ส่วนลดต่อหน่วย
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_DISCOUNT}
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_DISCOUNT}        10000.52
     Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
     ${Product Discount}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_DISCOUNT}
     Should Be Equal                           ${Product Discount}                    10,000.52                      
     Capture Page Screenshot

TC_088_Test Input Negative Number In Discount Text Box
     [Documentation]   ทดสอบกรอกจำนวนติดลบลงใน text box ส่วนลดต่อหน่วย
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_DISCOUNT}
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_DISCOUNT}        -10000
     Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
     ${Product Discount}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_DISCOUNT}
     Should Be Equal                           ${Product Discount}                    -10,000.00                      
     Capture Page Screenshot

TC_089_Test Input Text In Discount Text Box
     [Documentation]   ทดสอบกรอกตัวอักษรลงในช่อง text box ส่วนลดต่อหน่วย
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_DISCOUNT}
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_DISCOUNT}        test1234
     Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
     ${Product Discount}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_DISCOUNT}
     Should Be Equal                           ${Product Discount}                    ${EMPTY}                      
     Capture Page Screenshot

TC_090_Test Input Percent In Discount Text Box
     [Documentation]   ทดสอบกรอกจำนวนตามด้วยเปอร์เซ็นลงใน text box ส่วนลดต่อหน่วย
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_DISCOUNT}
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_DISCOUNT}        7%
     Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
     ${Product Discount}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_DISCOUNT}
     Should Be Equal                           ${Product Discount}                    7.00%                     
     Capture Page Screenshot

TC_091_Validate Summary Amount (Normal Discount)
     [Documentation]   ตรวจสอบผลคำนวนส่วนลดต่อหน่วยที่ รวม
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_CODE}
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_NAME}
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_QUANTITY}
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_PRICE}
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_DISCOUNT}
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_CODE}            TESTSUM
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_NAME}            TESTSUM
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_QUANTITY}        2    
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_PRICE}           100
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_DISCOUNT}        25
     Click Element When Ready                  ${XPATH_TEXT_SUMMARY_AMOUNT}
     Sleep                                     5s
     ${Summary Amount}=                        Get Text From Element When Ready             ${XPATH_TEXT_TOTAL_PRICE}
     Should Be Equal                           ${Summary Amount}                      150.00                    
     Capture Page Screenshot

TC_092_Test Click Add Product Button
     [Documentation]   ทดสอบกดปุ่ม + เพิ่่มสินค้า
     Click Element When Ready                  ${XPATH_BUTTON_ADD_PRODUCT}
     Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
     Wait Until Element Is Visible             ${XPATH_FIELD_PRODUCT_CODE4}                   
     Capture Page Screenshot

TC_093_Validate Summary Amount (Percent Discount)
     [Documentation]   ตรวจสอบผลคำนวนส่วนลดต่อหน่วยที่ รวม
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_CODE4}            TESTSUM2
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_NAME4}            TESTSUM2
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_QUANTITY4}        2    
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_PRICE4}           100
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_DISCOUNT4}        30%
     Click Element When Ready                  ${XPATH_TEXT_SUMMARY_AMOUNT}
     Sleep                                     5s
     ${Summary Amount}=                        Get Text From Element When Ready              ${XPATH_TEXT_TOTAL_PRICE4}
     Should Be Equal                           ${Summary Amount}                      140.00                    
     Capture Page Screenshot

TC_094_Test Input Data In Shipping Text Box
     [Documentation]   ทดสอบกรอกข้อมูลลงใน text box ช่องทางจัดส่ง
     Input Text To Element When Ready          ${XPATH_FIELD_SHIPPING_CHANNEL}        Ninja Van
     Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
     ${Shipping}=                              Get Value From Element When Ready              ${XPATH_FIELD_SHIPPING_CHANNEL}
     Should Be Equal                           ${Shipping}                            Ninja Van                    
     Capture Page Screenshot

TC_095_Test Choose Data In Dropdown Shipping Text Box
     [Documentation]   ทดสอบเลือกข้อมูลใน dropdown ช่องทางจัดส่ง
     Clear Text When Ready                     ${XPATH_FIELD_SHIPPING_CHANNEL}
     Sleep                                     5s
     Click Element When Ready                  ${XPATH_SAMPLE_CHOOSE_SHIPPING}
     ${Shipping}=                              Get Value From Element When Ready              ${XPATH_FIELD_SHIPPING_CHANNEL}
     Should Be Equal                           ${Shipping}                            Kerry                    
     Capture Page Screenshot

TC_096_Test Choose Data In Dropdown Shipping Text Box
     [Documentation]   ทดสอบกรอกข้อมูลลงใน text box หมายเหตุ
     Input Text To Element When Ready          ${XPATH_FIELD_DESCRIPTION}                     ทดสอบหมายเหตุ
     ${Shipping}=                              Get Value From Element When Ready              ${XPATH_FIELD_DESCRIPTION}
     Should Be Equal                           ${Shipping}                            ทดสอบหมายเหตุ                    
     Capture Page Screenshot

TC_097_Test Input Integer In Discount Total Text Box
     [Documentation]   ทดสอบกรอกจำนวนเต็มลงใน text box ส่วนลด
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}        10000
     Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
     ${Product Discount All}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}
     Should Be Equal                           ${Product Discount All}                    10,000.00                      
     Capture Page Screenshot

TC_098_Test Decimal Text In Discount Total Text Box
     [Documentation]   ทดสอบกรอกจำนวนที่มีจุดทศนิยมลงใน text box ส่วนลด
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}        10000.52
     Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
     ${Product Discount All}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}
     Should Be Equal                           ${Product Discount All}                    10,000.52                      
     Capture Page Screenshot

TC_099_Test Input Negative Number In Discount Total Text Box
     [Documentation]   ทดสอบกรอกจำนวนติดลบลงใน text box ส่วนลด
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}        -10000
     Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
     ${Product Discount All}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}
     Should Be Equal                           ${Product Discount All}                    -10,000.00                      
     Capture Page Screenshot

TC_100_Test Input Text In Discount Total Text Box
     [Documentation]   ทดสอบกรอกตัวอักษรลงในช่อง text box ส่วนลด
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}        test1234
     Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
     ${Product Discount All}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}
     Should Be Equal                           ${Product Discount All}                    ${EMPTY}                      
     Capture Page Screenshot

TC_101_Test Input Percent In Discount Total Text Box
     [Documentation]   ทดสอบกรอกจำนวนตามด้วยเปอร์เซ็นลงใน text box ส่วนลด
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}        7%
     Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
     ${Product Discount All}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}
     Should Be Equal                           ${Product Discount All}                    7.00%                     
     Capture Page Screenshot

TC_102_Validate Discount Total
     [Documentation]   ตรวจสอบผลคำนวนส่วนลด
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_CODE}
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_NAME}
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_QUANTITY}
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_PRICE}
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_DISCOUNT}
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_CODE2}
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_NAME2}
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_QUANTITY2}
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_PRICE2}
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_DISCOUNT2}
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_CODE4}
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_NAME4}
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_QUANTITY4}
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_PRICE4}
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_DISCOUNT4}
     Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_CODE}                      TestProductCode01
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_NAME}                      TestProductName01
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_QUANTITY}                  2
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_PRICE}                     5000
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_DISCOUNT}                  500
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_CODE2}                     TestProductCode02
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_Name2}                     TestProductName02
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_QUANTITY2}                 2
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_PRICE2}                    5000
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_DISCOUNT2}                 500
     Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}       500
     Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
     ${Total}=                      Get Text From Element When Ready         ${XPATH_TEXT_SUMMARY_AMOUNT}
     Should Be Equal                           ${Total}                    17,500.00                    
     Capture Page Screenshot

TC_103_Test Input Integer In Shipping Price Text Box
     [Documentation]   ทดสอบกรอกจำนวนเต็มลงใน text box ค่าส่ง
     Clear Text When Ready                     ${XPATH_FIELD_SHIPPING_PRICE}
     Input Text To Element When Ready          ${XPATH_FIELD_SHIPPING_PRICE}        500
     Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
     ${Shipping Price}=                      Get Value From Element When Ready         ${XPATH_FIELD_SHIPPING_PRICE}
     Should Be Equal                           ${Shipping Price}                    500.00                      
     Capture Page Screenshot

TC_104_Test Decimal Text In Shipping Price Text Box
     [Documentation]   ทดสอบกรอกจำนวนที่มีจุดทศนิยมลงใน text box ค่าส่ง
     Clear Text When Ready                     ${XPATH_FIELD_SHIPPING_PRICE}
     Input Text To Element When Ready          ${XPATH_FIELD_SHIPPING_PRICE}        500.50
     Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
     ${Shipping Price}=                      Get Value From Element When Ready         ${XPATH_FIELD_SHIPPING_PRICE}
     Should Be Equal                           ${Shipping Price}                    500.50                      
     Capture Page Screenshot

TC_105_Test Input Negative Number In Shipping Price Text Box
     [Documentation]   ทดสอบกรอกจำนวนติดลบลงใน text box ค่าส่ง
     Clear Text When Ready                     ${XPATH_FIELD_SHIPPING_PRICE}
     Input Text To Element When Ready          ${XPATH_FIELD_SHIPPING_PRICE}        -500
     Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
     ${Shipping Price}=                      Get Value From Element When Ready         ${XPATH_FIELD_SHIPPING_PRICE}
     Should Be Equal                           ${Shipping Price}                    -500.00                     
     Capture Page Screenshot

TC_106_Test Input Text In Shipping Price Text Box
     [Documentation]   ทดสอบกรอกตัวอักษรลงในช่อง text box ค่าส่ง
     Clear Text When Ready                     ${XPATH_FIELD_SHIPPING_PRICE}
     Input Text To Element When Ready          ${XPATH_FIELD_SHIPPING_PRICE}        test1234
     Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
     ${Shipping Price}=                      Get Value From Element When Ready         ${XPATH_FIELD_SHIPPING_PRICE}
     Should Be Equal                           ${Shipping Price}                    ${EMPTY}                      
     Capture Page Screenshot

TC_107_Validate Shipping Price Total
     [Documentation]   ตรวจสอบผลคำนวนค่าส่ง
     Clear Text When Ready                     ${XPATH_FIELD_SHIPPING_PRICE}
     Input Text To Element When Ready          ${XPATH_FIELD_SHIPPING_PRICE}        500
     Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
     ${Total}=                      Get Text From Element When Ready         ${XPATH_TEXT_SUMMARY_AMOUNT}
     Should Be Equal                           ${Total}                    18,000.00                    
     Capture Page Screenshot