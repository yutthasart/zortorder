*** Settings ***
Resource          ${CURDIR}/../resources/testdata/testdata.robot
Resource          ${CURDIR}/../resources/variables/variable_config.robot
Resource          ${CURDIR}/../resources/variables/variable_login.robot
Resource          ${CURDIR}/../resources/variables/variable_home.robot
Resource          ${CURDIR}/../resources/variables/variable_addsellorder.robot
Resource          ${CURDIR}/../resources/variables/variable_sellorder.robot
Resource          ${CURDIR}/../keywords/common/common_keywords.robot
Suite Teardown    Close All Browsers


*** Test Cases ***
TC_149_Test Click Text Link Order List
     [Documentation]   ทดสอบกด text link รายการขาย 
     Open Browser      ${URL}      ${BROWSER}
     Set Window Size    1920   1080            
     Input Text To Element When Ready          ${XPATH_FIELD_USERNAME}       ${EMAIL}           
     Input Text To Element When Ready          ${XPATH_FIELD_PASSWORD}       ${PASSWORD}     
     Click Element When Ready                  ${XPATH_BUTTON_LOGIN}
     Sleep                                     10s
     ${popup1}  Run Keyword And Return Status    Element Should Be Visible         ${XPATH_CLOSE_POPUP_EXPIRE}
       Log  ${popup1}
       Run Keyword If    '${popup1}'=='True'       Click Element                     ${XPATH_CLOSE_POPUP_EXPIRE}
       Run Keyword If    '${popup1}'=='False'      Pop Up Not Avalible
     ${popup2}  Run Keyword And Return Status    Element Should Be Visible         ${XPATH_CLOSE_POPUP}
       Log  ${popup2}
       Run Keyword If    '${popup2}'=='True'       Click Element                     ${XPATH_CLOSE_POPUP}
       Run Keyword If    '${popup2}'=='False'      Pop Up Not Avalible
     # Wait Until Element Is Visible             ${XPATH_CLOSE_POPUP_EXPIRE}          20s
     # Click Element When Ready                  ${XPATH_CLOSE_POPUP_EXPIRE}
     # Wait Until Element Is Visible             ${XPATH_CLOSE_POPUP}                 20s
     # Click Element When Ready                  ${XPATH_CLOSE_POPUP}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU_ADD_ORDER}
     Click Element When Ready                  ${XPATH_BUTTON_BACK_TO_ORDER_LIST_1}
     Wait Until Page Contains                  รายการขาย
     Wait Until Page Contains                  ทั้งหมด
     Wait Until Page Contains                  รอโอน
     Wait Until Page Contains                  รอชำระ
     Wait Until Page Contains                  สำเร็จ
     Capture Page Screenshot                                  
   
TC_150_Test Click Back Button
     [Documentation]   ทดสอบกดปุ่มกลับ
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU_ADD_ORDER}
     Click Element When Ready                  ${XPATH_BUTTON_BACK_TO_ORDER_LIST_2} 
     Wait Until Page Contains                  รายการขาย
     Wait Until Page Contains                  ทั้งหมด
     Wait Until Page Contains                  รอโอน
     Wait Until Page Contains                  รอชำระ
     Wait Until Page Contains                  สำเร็จ
     Capture Page Screenshot       
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_LENGTH}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_LENGTH_PAST_THREE_MONTH}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_SAMPLE_BUTTON_KEBAB_MENU}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_DELETE_IN_KEBAB_MENU}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_CONFIRM_DELETE_SO_IN_KEBAB_MENU}
     Sleep                                     3s  
     Click Element When Ready                  ${XPATH_SAMPLE_BUTTON_KEBAB_MENU}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_DELETE_IN_KEBAB_MENU}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_CONFIRM_DELETE_SO_IN_KEBAB_MENU}
     Sleep                                     3s  
     Click Element When Ready                  ${XPATH_SAMPLE_BUTTON_KEBAB_MENU}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_DELETE_IN_KEBAB_MENU}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_CONFIRM_DELETE_SO_IN_KEBAB_MENU}
     Sleep                                     3s  