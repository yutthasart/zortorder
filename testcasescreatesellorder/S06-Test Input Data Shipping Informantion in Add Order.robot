*** Settings ***
Resource          ${CURDIR}/../resources/testdata/testdata.robot
Resource          ${CURDIR}/../resources/variables/variable_config.robot
Resource          ${CURDIR}/../resources/variables/variable_login.robot
Resource          ${CURDIR}/../resources/variables/variable_home.robot
Resource          ${CURDIR}/../resources/variables/variable_addsellorder.robot
Resource          ${CURDIR}/../keywords/common/common_keywords.robot
Suite Teardown    Close All Browsers


*** Test Cases ***
TC_121_Test Choose Data In Textbox Shipping Date
     [Documentation]   ทดสอบการกรอกข้อมูล วันส่งสินค้า 
     Open Browser      ${URL}      ${BROWSER}
     Set Window Size    1920   1080            
     Input Text To Element When Ready          ${XPATH_FIELD_USERNAME}       ${EMAIL}           
     Input Text To Element When Ready          ${XPATH_FIELD_PASSWORD}       ${PASSWORD}     
     Click Element When Ready                  ${XPATH_BUTTON_LOGIN}
     Sleep                                     10s
     ${popup1}  Run Keyword And Return Status    Element Should Be Visible         ${XPATH_CLOSE_POPUP_EXPIRE}
       Log  ${popup1}
       Run Keyword If    '${popup1}'=='True'       Click Element                     ${XPATH_CLOSE_POPUP_EXPIRE}
       Run Keyword If    '${popup1}'=='False'      Pop Up Not Avalible
     ${popup2}  Run Keyword And Return Status    Element Should Be Visible         ${XPATH_CLOSE_POPUP}
       Log  ${popup2}
       Run Keyword If    '${popup2}'=='True'       Click Element                     ${XPATH_CLOSE_POPUP}
       Run Keyword If    '${popup2}'=='False'      Pop Up Not Avalible
     # Wait Until Element Is Visible             ${XPATH_CLOSE_POPUP_EXPIRE}          20s
     # Click Element When Ready                  ${XPATH_CLOSE_POPUP_EXPIRE}
     # Wait Until Element Is Visible             ${XPATH_CLOSE_POPUP}                 20s
     # Click Element When Ready                  ${XPATH_CLOSE_POPUP}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU_ADD_ORDER}
     Click Element When Ready                  ${XPATH_FIELD_SHIPPING_DATE}
     Click Element When Ready                  ${XPATH_CHOOSE_SHIPPING_MONTH}
     Click Element When Ready                  ${XPATH_CHOOSE_SHIPPING_YEAR}
     Click Element When Ready                  ${XPATH_SAMELE_SHIPPING_YEAR}
     Click Element When Ready                  ${XPATH_SAMELE_SHIPPING_MONTH}
     Click Element When Ready                  ${XPATH_SAMPLE_SHIPPING_DATE}
     Click Element When Ready                  ${XPATH_SAMPLE_SHIPPING_HOUR}
     Click Element When Ready                  ${XPATH_SAMPLE_SHIPING_MINUTE}
     ${์Shiping Date}=                      Get Value From Element When Ready         ${XPATH_FIELD_SHIPPING_DATE}
     Should Be Equal                           ${์Shiping Date}                       25/05/2564 17:50
     Capture Page Screenshot                                  
   
TC_122_Test Delete Data In Textbox Shipping Date
     [Documentation]   ทดสอบปุ่ม x ของวันที่ส่งสินค้า
     Click Element When Ready                  ${XPATH_BUTTON_REMOVE_SHIPPING_DATE}
     ${์Shiping Date}=                      Get Value From Element When Ready         ${XPATH_FIELD_SHIPPING_DATE}
     Should Be Equal                           ${์Shiping Date}                       ${Empty}
     Capture Page Screenshot       

TC_123_Test Input Data In Textbox Shipping Track
     [Documentation]   ทดสอบการกรอกข้อมูล Tracking no
     Click Element When Ready                  ${XPATH_FIELD_SHIPPING_TRACKINGNO}
     Input Text To Element When Ready          ${XPATH_FIELD_SHIPPING_TRACKINGNO}       TH00000001SP
     ${์Shiping Tracking}=                      Get Value From Element When Ready        ${XPATH_FIELD_SHIPPING_TRACKINGNO}
     Should Be Equal                           ${์Shiping Tracking}                      TH00000001SP
     Capture Page Screenshot                           