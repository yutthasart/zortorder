*** Settings ***
Resource          ${CURDIR}/../resources/testdata/testdata.robot
Resource          ${CURDIR}/../resources/variables/variable_config.robot
Resource          ${CURDIR}/../resources/variables/variable_login.robot
Resource          ${CURDIR}/../resources/variables/variable_home.robot
Resource          ${CURDIR}/../resources/variables/variable_addsellorder.robot
Resource          ${CURDIR}/../keywords/common/common_keywords.robot
Suite Teardown    Close All Browsers


*** Test Cases ***
TC_143_Test Add Order And Click Add And Create Order Button
     [Documentation]   ทดสอบกดปุ่มบันทึก + สร้างใหม่ โดยกรอกข้อมูลที่มี * 
     Open Browser      ${URL}      ${BROWSER}
     Set Window Size    1920   1080            
     Input Text To Element When Ready          ${XPATH_FIELD_USERNAME}       ${EMAIL}           
     Input Text To Element When Ready          ${XPATH_FIELD_PASSWORD}       ${PASSWORD}     
     Click Element When Ready                  ${XPATH_BUTTON_LOGIN}
     Sleep                                     10s
     ${popup1}  Run Keyword And Return Status    Element Should Be Visible         ${XPATH_CLOSE_POPUP_EXPIRE}
       Log  ${popup1}
       Run Keyword If    '${popup1}'=='True'       Click Element                     ${XPATH_CLOSE_POPUP_EXPIRE}
       Run Keyword If    '${popup1}'=='False'      Pop Up Not Avalible
     ${popup2}  Run Keyword And Return Status    Element Should Be Visible         ${XPATH_CLOSE_POPUP}
       Log  ${popup2}
       Run Keyword If    '${popup2}'=='True'       Click Element                     ${XPATH_CLOSE_POPUP}
       Run Keyword If    '${popup2}'=='False'      Pop Up Not Avalible
     # Wait Until Element Is Visible             ${XPATH_CLOSE_POPUP_EXPIRE}          20s
     # Click Element When Ready                  ${XPATH_CLOSE_POPUP_EXPIRE}
     # Wait Until Element Is Visible             ${XPATH_CLOSE_POPUP}                 20s
     # Click Element When Ready                  ${XPATH_CLOSE_POPUP}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU_ADD_ORDER}
     Click Element When Ready                  ${XPATH_BUTTON_SAVE_AND_CREATE_ORDER}
     Click Element When Ready                  ${XPATH_BUTTON_OK_SUCCESS_MODAL}
     Wait Until Page Contains                  สร้างรายการขาย
     Capture Page Screenshot                                  
   
TC_144_Test Add Order And Click Add Order Button
     [Documentation]   ทดสอบกดปุ่มบันทึก โดยกรอกข้อมูลที่มี *
     Click Element When Ready                  ${XPATH_BUTTON_SAVE_ORDER}
     Click Element When Ready                  ${XPATH_BUTTON_OK_SUCCESS_MODAL}
     Wait Until Page Contains                  รายละเอียดรายการขาย
     Capture Page Screenshot        

TC_145_Test Add Order And Input Duplicate SO Number And Click Add Order
     [Documentation]   ทดสอบกดปุ่มบันทึก หรือ ปุ่มบันทึก + สร้างใหม่ โดยกรอกข้อมูลรายการซ้ำกับที่มีอยู่ในระบบ
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU_ADD_ORDER}
     Clear Text When Ready                     ${XPATH_FIELD_SO_NUMBER}
     Input Text To Element When Ready          ${XPATH_FIELD_SO_NUMBER}             SO-202204001
     Click Element When Ready                  ${XPATH_BUTTON_SAVE_ORDER}
     Wait Until Page Contains                  หมายเลขรายการซ้ำ
     Capture Page Screenshot

TC_146_Test Click OK Button For Create New Order
     [Documentation]   ทดสอบกด ยืนยัน เพื่อยืนยันการบันทึก หมายเลขรายการใหม่ที่ระบบกำหนดให้
     ${New SO}=                                Get Text From Element When Ready            ${XPATH_TEXT_NEW_SO_IN_POPUP}
     Click Element When Ready                  ${XPATH_BUTTON_CONFIRM_NEW_SO}
     Click Element When Ready                  ${XPATH_BUTTON_OK_SUCCESS_MODAL} 
     ${New SO}                                 Remove String                               ${New SO}         หมายเลขรายการใหม่ของคุณ คือ\n     
     Wait Until Page Contains                  รายละเอียดรายการขาย
     Wait Until Page Contains                  ${New SO}
     Capture Page Screenshot 