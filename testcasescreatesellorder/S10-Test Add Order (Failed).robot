*** Settings ***
Resource          ${CURDIR}/../resources/testdata/testdata.robot
Resource          ${CURDIR}/../resources/variables/variable_config.robot
Resource          ${CURDIR}/../resources/variables/variable_login.robot
Resource          ${CURDIR}/../resources/variables/variable_home.robot
Resource          ${CURDIR}/../resources/variables/variable_addsellorder.robot
Resource          ${CURDIR}/../keywords/common/common_keywords.robot
Suite Teardown    Close All Browsers


*** Test Cases ***
TC_147_Test Add Order But Not Input * Data And Click Add And Create Order Button
     [Documentation]   ทดสอบกดปุ่มบันทึก + สร้างใหม่ โดยไม่กรอกข้อมูลที่มี * 
     Open Browser      ${URL}      ${BROWSER}
     Set Window Size    1920   1080            
     Input Text To Element When Ready          ${XPATH_FIELD_USERNAME}       ${EMAIL}           
     Input Text To Element When Ready          ${XPATH_FIELD_PASSWORD}       ${PASSWORD}     
     Click Element When Ready                  ${XPATH_BUTTON_LOGIN}
     Sleep                                     10s
     ${popup1}  Run Keyword And Return Status    Element Should Be Visible         ${XPATH_CLOSE_POPUP_EXPIRE}
       Log  ${popup1}
       Run Keyword If    '${popup1}'=='True'       Click Element                     ${XPATH_CLOSE_POPUP_EXPIRE}
       Run Keyword If    '${popup1}'=='False'      Pop Up Not Avalible
     ${popup2}  Run Keyword And Return Status    Element Should Be Visible         ${XPATH_CLOSE_POPUP}
       Log  ${popup2}
       Run Keyword If    '${popup2}'=='True'       Click Element                     ${XPATH_CLOSE_POPUP}
       Run Keyword If    '${popup2}'=='False'      Pop Up Not Avalible
     # Wait Until Element Is Visible             ${XPATH_CLOSE_POPUP_EXPIRE}          20s
     # Click Element When Ready                  ${XPATH_CLOSE_POPUP_EXPIRE}
     # Wait Until Element Is Visible             ${XPATH_CLOSE_POPUP}                 20s
     # Click Element When Ready                  ${XPATH_CLOSE_POPUP}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU_ADD_ORDER}
     Clear Text When Ready                     ${XPATH_FIELD_SO_NUMBER}
     Click Element When Ready                  ${XPATH_REMOVE_DATE}
     Click Element When Ready                  ${XPATH_BUTTON_SAVE_AND_CREATE_ORDER}
     Wait Until Page Contains                  กรุณาใส่ข้อมูลให้ครบถ้วน
     Capture Page Screenshot                                  
   
TC_148_Test Add Order But Not Input * Data And Click Add Order Button
     [Documentation]   ทดสอบกดปุ่มบันทึก โดยไม่กรอกข้อมูลที่มี *
     Click Element When Ready                  ${XPATH_BUTTON_OK_ERROR_POPUP} 
     Click Element When Ready                  ${XPATH_BUTTON_SAVE_ORDER}
     Wait Until Page Contains                  กรุณาใส่ข้อมูลให้ครบถ้วน
     Capture Page Screenshot       