*** Settings ***
Resource          ${CURDIR}/../resources/testdata/testdata.robot
Resource          ${CURDIR}/../resources/variables/variable_config.robot
Resource          ${CURDIR}/../resources/variables/variable_login.robot
Resource          ${CURDIR}/../resources/variables/variable_home.robot
Resource          ${CURDIR}/../resources/variables/variable_sellorder.robot
Resource          ${CURDIR}/../keywords/common/common_keywords.robot
Suite Teardown    Close All Browsers


*** Test Cases ***
TC_001_Test Search SO Number (Patial)
     [Documentation]   ทดสอบค้นหาเลขที่รายการแบบ patial
     Open Browser      ${URL}      ${BROWSER}
     Set Window Size    1920   1080            
     Input Text To Element When Ready          ${XPATH_FIELD_USERNAME}       ${EMAIL}           
     Input Text To Element When Ready          ${XPATH_FIELD_PASSWORD}       ${PASSWORD}     
     Click Element When Ready                  ${XPATH_BUTTON_LOGIN}
     Sleep                                     10s
     ${popup1}  Run Keyword And Return Status    Element Should Be Visible         ${XPATH_CLOSE_POPUP_EXPIRE}
       Log  ${popup1}
       Run Keyword If    '${popup1}'=='True'       Click Element When Ready                     ${XPATH_CLOSE_POPUP_EXPIRE}
       Run Keyword If    '${popup1}'=='False'      Pop Up Not Avalible
     ${popup2}  Run Keyword And Return Status    Element Should Be Visible         ${XPATH_CLOSE_POPUP}
       Log  ${popup2}
       Run Keyword If    '${popup2}'=='True'       Click Element When Ready                     ${XPATH_CLOSE_POPUP}
       Run Keyword If    '${popup2}'=='False'      Pop Up Not Avalible
     # Wait Until Element Is Visible             ${XPATH_CLOSE_POPUP_EXPIRE}          20s
     # Click Element When Ready                  ${XPATH_CLOSE_POPUP_EXPIRE}
     # Wait Until Element Is Visible             ${XPATH_CLOSE_POPUP}                 20s
     # Click Element When Ready                  ${XPATH_CLOSE_POPUP}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU_SELL_ORDER}
     Sleep                                     3s
     Input Text To Element When Ready          ${XPATH_FIELD_SEARCH_SELL_ORDER}             SO-202209
     Press Keys                                ${XPATH_FIELD_SEARCH_SELL_ORDER}             ENTER
     Wait Until Page Contains                  SO-202209003
     Wait Until Page Contains                  SO-202209002 
     Wait Until Page Contains                  SO-202209001 
     Capture Page Screenshot
   
TC_002_Test Search SO Number (Full)
     [Documentation]   ทดสอบค้นหาเลขที่่รายการแบบ full
     Sleep                                     3s
     Clear Text When Ready                     ${XPATH_FIELD_SEARCH_SELL_ORDER}
     Input Text To Element When Ready          ${XPATH_FIELD_SEARCH_SELL_ORDER}             SO-202209003
     Press Keys                                ${XPATH_FIELD_SEARCH_SELL_ORDER}             ENTER
     Wait Until Page Contains                  SO-202209003
     Wait Until Page Does Not Contain          SO-202209002 
     Wait Until Page Does Not Contain          SO-202209001 
     Capture Page Screenshot
   

TC_003_Test Search Customer Name (Patial)
     [Documentation]   ทดสอบค้นหาชื่อลูกค้าแบบ patial
     Sleep                                     3s
     Clear Text When Ready                     ${XPATH_FIELD_SEARCH_SELL_ORDER}
     Input Text To Element When Ready          ${XPATH_FIELD_SEARCH_SELL_ORDER}             ข้อมูลตัวอย่าง
     Press Keys                                ${XPATH_FIELD_SEARCH_SELL_ORDER}             ENTER
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 1
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 2
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 3
     Capture Page Screenshot
     
TC_004_Test Search Customer Name (Full)
     [Documentation]   ทดสอบค้นหาชื่่อลูกค้าแบบ full
     Sleep                                     3s
     Clear Text When Ready                     ${XPATH_FIELD_SEARCH_SELL_ORDER}
     Input Text To Element When Ready          ${XPATH_FIELD_SEARCH_SELL_ORDER}             ข้อมูลตัวอย่าง 1
     Press Keys                                ${XPATH_FIELD_SEARCH_SELL_ORDER}             ENTER
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 1
     Capture Page Screenshot

TC_005_Test Search Sales Channel (Patial) 
     [Documentation]   ทดสอบค้นหาช่องทางแบบ patial
     Sleep                                     3s
     Clear Text When Ready                     ${XPATH_FIELD_SEARCH_SELL_ORDER}
     Input Text To Element When Ready          ${XPATH_FIELD_SEARCH_SELL_ORDER}             ช่องทางตัวอย่าง
     Press Keys                                ${XPATH_FIELD_SEARCH_SELL_ORDER}             ENTER
     Wait Until Page Contains                  ช่องทางตัวอย่าง 1
     Wait Until Page Contains                  ช่องทางตัวอย่าง 2
     Capture Page Screenshot

TC_006_Test Search Sales Channel (Full)
     [Documentation]   ทดสอบค้นหาช่องทางแบบ full
     Sleep                                     3s
     Clear Text When Ready                     ${XPATH_FIELD_SEARCH_SELL_ORDER}
     Input Text To Element When Ready          ${XPATH_FIELD_SEARCH_SELL_ORDER}             ช่องทางตัวอย่าง 1
     Press Keys                                ${XPATH_FIELD_SEARCH_SELL_ORDER}             ENTER
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 1
     Capture Page Screenshot