*** Settings ***
Resource          ${CURDIR}/../resources/testdata/testdata.robot
Resource          ${CURDIR}/../resources/variables/variable_config.robot
Resource          ${CURDIR}/../resources/variables/variable_login.robot
Resource          ${CURDIR}/../resources/variables/variable_home.robot
Resource          ${CURDIR}/../resources/variables/variable_sellorder.robot
Resource          ${CURDIR}/../keywords/common/common_keywords.robot
Suite Teardown    Close All Browsers


*** Test Cases ***
TC_069_Test Click Tab Status All
     [Documentation]   ทดสอบกดแท็บสถานะ "ทั้งหมด"
     Open Browser      ${URL}      ${BROWSER}
     Set Window Size    1920   1080            
     Input Text To Element When Ready          ${XPATH_FIELD_USERNAME}       ${EMAIL}           
     Input Text To Element When Ready          ${XPATH_FIELD_PASSWORD}       ${PASSWORD}     
     Click Element When Ready                  ${XPATH_BUTTON_LOGIN}
     Sleep                                     10s
     ${popup1}  Run Keyword And Return Status    Element Should Be Visible         ${XPATH_CLOSE_POPUP_EXPIRE}
       Log  ${popup1}
       Run Keyword If    '${popup1}'=='True'       Click Element When Ready                     ${XPATH_CLOSE_POPUP_EXPIRE}
       Run Keyword If    '${popup1}'=='False'      Pop Up Not Avalible
     ${popup2}  Run Keyword And Return Status    Element Should Be Visible         ${XPATH_CLOSE_POPUP}
       Log  ${popup2}
       Run Keyword If    '${popup2}'=='True'       Click Element When Ready                     ${XPATH_CLOSE_POPUP}
       Run Keyword If    '${popup2}'=='False'      Pop Up Not Avalible
     # Wait Until Element Is Visible             ${XPATH_CLOSE_POPUP_EXPIRE}          20s
     # Click Element When Ready                  ${XPATH_CLOSE_POPUP_EXPIRE}
     # Wait Until Element Is Visible             ${XPATH_CLOSE_POPUP}                 20s
     # Click Element When Ready                  ${XPATH_CLOSE_POPUP}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU_SELL_ORDER}
     Click Element When Ready                  ${XPATH_TAB_STATUS_ALL}
     Wait Until Page Contains                  รอโอน
     Wait Until Page Contains                  รอชำระ
     Wait Until Page Contains                  ชำระบางส่วน
     Wait Until Page Contains                  สำเร็จ
     Wait Until Page Contains                  ชำระครบ
     Capture Page Screenshot
   
TC_070_Test Click Tab Status Pending
     [Documentation]   ทดสอบกดแท็บสถานะ "รอโอน"
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_TAB_STATUS_WAIT_TO_SHIPPING}
     Wait Until Page Contains                  รอโอน
     Wait Until Page Contains                  รอชำระ
     Wait Until Page Contains                  ชำระบางส่วน
     Capture Page Screenshot

TC_071_Test Click Tab Status Unpaid
     [Documentation]   ทดสอบกดแท็บสถานะ "รอชำระ"
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_TAB_STATUS_WAIT_TO_PAY}
     Wait Until Page Contains                  สำเร็จ
     Wait Until Page Contains                  รอชำระ
     Wait Until Page Contains                  ชำระบางส่วน
     Wait Until Page Contains                  รอโอน
     Wait Until Page Contains                  สำเร็จ
     Capture Page Screenshot

TC_072_Test Click Tab Status Completed
     [Documentation]   ทดสอบกดแท็บสถานะ "สำเร็จ"
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_TAB_STATUS_COMPLETE}
     Wait Until Page Contains                  สำเร็จ
     Wait Until Page Contains                  ชำระครบ
     Capture Page Screenshot

TC_073_Test Click Refresh Button
     [Documentation]   ทดสอบกดปุ่ม Refresh
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_REFRESH_ORDER_LIST}
     Wait Until Page Contains                  สำเร็จ
     Wait Until Page Contains                  ชำระครบ
     Capture Page Screenshot

TC_074_Test Click Check Box For Choose SO
     [Documentation]   ทดสอบกด Checkbox เพื่อเลือกรายการ
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_TAB_STATUS_ALL}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_SAMPLE_CHECK_BOX}
     Wait Until Element Is Visible             ${XPATH_BUTTON_COUNT_CHOOSE_SO}
     Wait Until Element Is Visible             ${XPATH_BUTTON_PRINT}
     Wait Until Element Is Visible             ${XPATH_BUTTON_CALL_SHIPPING}
     Wait Until Element Is Visible             ${XPATH_BUTTON_USE_COMMAND}
     Capture Page Screenshot

TC_075_Test Click Check Box For Cancel Choose SO
     [Documentation]   ทดสอบกด Checkbox เพื่อยกเลิกการเลือกรายการ
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_SAMPLE_CHECK_BOX}
     Wait Until Element Is Not Visible         ${XPATH_BUTTON_COUNT_CHOOSE_SO}
     Wait Until Element Is Not Visible         ${XPATH_BUTTON_PRINT}
     Wait Until Element Is Not Visible         ${XPATH_BUTTON_CALL_SHIPPING}
     Wait Until Element Is Not Visible         ${XPATH_BUTTON_USE_COMMAND}
     Capture Page Screenshot

TC_076_Test Click Record Choose SO Button
     [Documentation]   ทดสอบกดปุ่มบอกจำนวนรายการที่เลือก
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_SAMPLE_CHECK_BOX}
     Click Element When Ready                  ${XPATH_BUTTON_COUNT_CHOOSE_SO}
     Wait Until Element Is Visible             ${XPATH_TEXTLINK_DELETE_CHOOSE_SO}
     Wait Until Element Is Visible             ${XPATH_TEXT_CHOOSE_SO}
     Capture Page Screenshot

TC_077_Test Click Clear Record SO Text Link 
     [Documentation]   ทดสอบกดปุ่มล้างข้อมูล
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_TEXTLINK_DELETE_CHOOSE_SO}
     Wait Until Element Is Not Visible         ${XPATH_BUTTON_COUNT_CHOOSE_SO}
     Wait Until Element Is Not Visible         ${XPATH_BUTTON_PRINT}
     Wait Until Element Is Not Visible         ${XPATH_BUTTON_CALL_SHIPPING}
     Wait Until Element Is Not Visible         ${XPATH_BUTTON_USE_COMMAND}
     Capture Page Screenshot

TC_078_Test Click Print Button
     [Documentation]   ทดสอบกดปุ่มพิมพ์เอกสาร
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_SAMPLE_CHECK_BOX}
     Click Element When Ready                  ${XPATH_BUTTON_PRINT}
     Wait Until Element Is Visible             ${XPATH_BUTTON_PRINT_DOCUMENT}
     Wait Until Element Is Visible             ${XPATH_BUTTON_PRINT_LETTER_BOX_LABEL}
     Wait Until Element Is Visible             ${XPATH_BUTTON_PRINT_PREPARATION}
     Capture Page Screenshot

TC_079_Test Click Print In Print Button
     [Documentation]   ทดสอบกดพิมพ์เอกสาร
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_PRINT_DOCUMENT}
     Wait Until Element Is Visible             ${XPATH_RADIO_BUTTON_PRINT_DOCUMENT_SIZE_A4}
     Wait Until Element Is Visible             ${XPATH_RADIO_BUTTON_PRINT_DOCUMENT_SIZE_A5}
     Wait Until Element Is Visible             ${XPATH_RADIO_BUTTON_PRINT_DOCUMENT_SIZE_SLIP}
     Wait Until Element Is Visible             ${XPATH_DROPDOWN_CHOOSE_FORMAT_PAPER}
     Wait Until Element Is Visible             ${XPATH_CHECKBOX_PRINT_ORIGINAL_DOCUMENT}
     Wait Until Element Is Visible             ${XPATH_CHECKBOX_PRINT_COPY_DOCUMENT}
     Wait Until Element Is Visible             ${XPATH_CHECKBOX_CUSTOM_NUMBER_DOCUMENT}
     Wait Until Element Is Visible             ${XPATH_BUTTON_ACTION_PRINT_DOCUMENT}
     Capture Page Screenshot                  

TC_080_Test Click Print Shipping Label In Print Button
     [Documentation]   ทดสอบกดพิมพ์ใบแปะกล่อง
     Click Element When Ready                  ${XPATH_BUTTON_CLOSE_PRINT_DOCUMENT_MODAL} 
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_PRINT}
     Click Element When Ready                  ${XPATH_BUTTON_PRINT_LETTER_BOX_LABEL}
     Wait Until Element Is Visible             ${XPATH_RADIO_BUTTON_PRINT_LETTER_BOX_LABEL_SIZE_A5}
     Wait Until Element Is Visible             ${XPATH_RADIO_BUTTON_PRINT_LETTER_BOX_LABEL_SIZE_A4}
     Wait Until Element Is Visible             ${XPATH_RADIO_BUTTON_PRINT_LETTER_BOX_LABEL_SIZE_LETTER}
     Wait Until Element Is Visible             ${XPATH_RADIO_BUTTON_PRINT_LETTER_BOX_LABEL_SIZE_STICKER}
     Wait Until Element Is Visible             ${XPATH_RADIO_BUTTON_PRINT_LETTER_BOX_LABEL_SIZE_BOX_LABEL}
     Wait Until Element Is Visible             ${XPATH_BUTTON_ACTION_PRINT_LETTER_BOX_LABEL}
     Capture Page Screenshot

TC_081_Test Click Print Print Packing Document In Print Button
     [Documentation]   ทดสอบกดพิมพ์ใบจัดเตรียมสินค้า
     Click Element When Ready                  ${XPATH_BUTTON_CLOSE_PRINT_LETTER_BOX_LABEL_MODAL}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_PRINT}
     Click Element When Ready                  ${XPATH_BUTTON_PRINT_PREPARATION}
     Sleep                                     3s
     Switch Window                             title: ใบจัดเตรียมสินค้า
     Sleep                                     3s
     Wait Until Page Contains                  ใบจัดเตรียมสินค้า
     Wait Until Page Contains                  ใบรายการขาย
     Capture Page Screenshot
     Close Window

TC_082_Test Click Postal Service Button
     [Documentation]   ทดสอบกดปุ่มบริการส่งสินค้า
     Switch Window                             title: รายการขาย                           
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_CALL_SHIPPING}
     Wait Until Element Is Visible             ${XPATH_BUTTON_CALL_SHIPPING_GROUP}
     Wait Until Element Is Visible             ${XPATH_BUTTON_CALL_SHIPPING_SEPARATE}
     Wait Until Element Is Visible             ${XPATH_BUTTON_EXPORT_EXCEL_KERRY_EASY_SHIP}
     Wait Until Element Is Visible             ${XPATH_BUTTON_EXPORT_EXCEL_KERRY_EASY_SHIP_COD}
     Wait Until Element Is Visible             ${XPATH_BUTTON_EXPORT_EXCEL_KERRY_BUSINESS}
     Wait Until Element Is Visible             ${XPATH_BUTTON_EXPORT_EXCEL_KERRY_BUSINESS_COD}
     Capture Page Screenshot

TC_083_Test Click 1 Shipment In Porstal Service Button
     [Documentation]   ทดสอบกดปุ่มรวมรายการ
     Click Element When Ready                  ${XPATH_BUTTON_CALL_SHIPPING_GROUP}
     Wait Until Page Contains                  บริการขนส่ง
     Wait Until Page Contains                  ข้อมูลผู้ส่ง
     Wait Until Page Contains                  ข้อมูลผู้รับคนที่ 1
     Wait Until Page Contains                  เลือกช่องทางขนส่ง
     Capture Page Screenshot

TC_084_Test Click Each Shipment In Porstal Service Button
     [Documentation]   ทดสอบกดปุ่มแยกรายการ
     Click Element When Ready                  ${XPATH_BUTTON_CLOSE_SHIPPING_GROUP_MODAL}
     Click Element When Ready                  ${XPATH_BUTTON_CALL_SHIPPING}
     Click Element When Ready                  ${XPATH_BUTTON_CALL_SHIPPING_SEPARATE}
     Wait Until Page Contains                  ข้อมูลผู้ส่ง
     Wait Until Page Contains                  ข้อมูลผู้รับคนที่ 1
     Wait Until Page Contains                  เลือกช่องทางขนส่ง
     Wait Until Page Contains                  เลือกช่องทางขนส่ง
     Wait Until Page Contains                  รายการ
     Capture Page Screenshot

TC_085_Test Click Export to Kerry Easy Ship Excel file In Porstal Service Button
     [Documentation]   ทดสอบกดปุ่ม Export to Kerry Easy Ship Excel file
     Click Element When Ready                  ${XPATH_BUTTON_CLOSE_SHIPPING_SEPARATE_MODAL}
     Click Element When Ready                  ${XPATH_BUTTON_CALL_SHIPPING}
     Click Element When Ready                  ${XPATH_BUTTON_EXPORT_EXCEL_KERRY_EASY_SHIP}
     Wait Until Element Is Visible             ${XPATH_TEXT_EXPORT_EXCEL_KERRY_EASY_SHIP}
     Capture Page Screenshot

TC_086_Test Click Export to Kerry Easy Ship Excel file (COD) In Porstal Service Button
     [Documentation]   ทดสอบกดปุ่ม Export to Kerry Easy Ship Excel file (COD)
     Click Element When Ready                  ${XPATH_BUTTON_CALL_SHIPPING}
     Click Element When Ready                  ${XPATH_BUTTON_EXPORT_EXCEL_KERRY_EASY_SHIP_COD}
     Wait Until Element Is Visible             ${XPATH_TEXT_EXPORT_EXCEL_KERRY_EASY_SHIP_COD}
     Capture Page Screenshot

TC_087_Test Click Export to Kerry Business Excel file In Porstal Service Button
     [Documentation]   ทดสอบกดปุ่ม Export to Kerry Business Excel file
     Click Element When Ready                  ${XPATH_BUTTON_CALL_SHIPPING}
     Click Element When Ready                  ${XPATH_BUTTON_EXPORT_EXCEL_KERRY_BUSINESS}
     Wait Until Element Is Visible             ${XPATH_TEXT_EXPORT_EXCEL_KERRY_BUSINESS}
     Capture Page Screenshot

TC_088_Test Click Export to Kerry Business Excel file (COD) In Porstal Service Button
     [Documentation]   ทดสอบกดปุ่ม Export to Kerry Business Excel file (COD)
     Click Element When Ready                  ${XPATH_BUTTON_CALL_SHIPPING}
     Click Element When Ready                  ${XPATH_BUTTON_EXPORT_EXCEL_KERRY_BUSINESS_COD}
     Wait Until Element Is Visible             ${XPATH_TEXT_EXPORT_EXCEL_KERRY_BUSINESS_COD}
     Capture Page Screenshot

TC_089_Test Click More Button
     [Documentation]   ทดสอบกดปุ่มคำสั่ง
     Click Element When Ready                  ${XPATH_BUTTON_USE_COMMAND}
     Wait Until Element Is Visible             ${XPATH_BUTTON_COMMAND_PIN_FAV_SO}
     Wait Until Element Is Visible             ${XPATH_BUTTON_COMMAND_EDIT_SHIPPING}
     Wait Until Element Is Visible             ${XPATH_BUTTON_COMMAND_COMPLETE_SHIPPING}
     Wait Until Element Is Visible             ${XPATH_BUTTON_COMMAND_COMPLETE_PAYMENT}
     Wait Until Element Is Visible             ${XPATH_BUTTON_COMMAND_MERGE_SO}
     Wait Until Element Is Visible             ${XPATH_BUTTON_COMMAND_HIDE_SO}
     Wait Until Element Is Visible             ${XPATH_BUTTON_COMMAND_DELETE_SO}
     Wait Until Element Is Visible             ${XPATH_BUTTON_COMMAND_CANCEL_SO}
     Capture Page Screenshot

TC_090_Test Click Pin In More Button
     [Documentation]   ทดสอบกดปุ่ม ปักหมุดไว้บนสุด
     Click Element When Ready                  ${XPATH_BUTTON_COMMAND_PIN_FAV_SO}
     Input Text To Element When Ready          ${XPATH_TEXTBOX_CONFIRM_PIN_FAV_SO}             confirm
     Click Element When Ready                  ${XPATH_BUTTON_CONFIRM_PIN_FAV_SO}
     Wait Until Element Is Visible             ${XPATH_SAMPLE_IMG_FAV_SO}
     Capture Page Screenshot

TC_091_Test Click Edit Shipment In More Button
     [Documentation]   ทดสอบกดปุ่ม แก้ไขข้อมูลขนส่ง
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_SAMPLE_CHECK_BOX}
     Click Element When Ready                  ${XPATH_BUTTON_USE_COMMAND}
     Click Element When Ready                  ${XPATH_BUTTON_COMMAND_EDIT_SHIPPING}
     Wait Until Page Contains                  แก้ไขข้อมูลขนส่ง
     Wait Until Page Contains                  วันส่งสินค้า
     Wait Until Page Contains                  ช่องทางจัดส่ง
     Capture Page Screenshot

TC_092_Test Click Complete Orders In More Button
     [Documentation]   ทดสอบกดปุ่ม โอนสินค้าทั้งหมด
     Click Element When Ready                  ${XPATH_BUTTON_CLOSE_COMMAND_EDIT_SHIPPING}
     Click Element When Ready                  ${XPATH_BUTTON_USE_COMMAND}
     Click Element When Ready                  ${XPATH_BUTTON_COMMAND_COMPLETE_SHIPPING}
     Wait Until Page Contains                  ยืนยันการโอนสินค้าทั้งหมด
     Wait Until Page Contains                  คลังสินค้า
     Wait Until Page Contains                  วันที่โอน
     Capture Page Screenshot

TC_093_Test Click Add Payment In More Button
     [Documentation]   ทดสอบกดปุ่ม ชำระเต็มจำนวน
     Click Element When Ready                  ${XPATH_BUTTON_CLOSE_COMMAND_COMPLETE_SHIPPING}
     Click Element When Ready                  ${XPATH_BUTTON_USE_COMMAND}
     Click Element When Ready                  ${XPATH_BUTTON_COMMAND_COMPLETE_PAYMENT}
     Wait Until Page Contains                  ชำระเงิน
     Wait Until Page Contains                  ช่องทางการชำระเงิน
     Wait Until Page Contains                  วันที่ชำระเงิน
     Wait Until Page Contains                  ตั้งค่า
     Wait Until Page Contains                  ภาษีหัก ณ ที่จ่าย
     Capture Page Screenshot

TC_094_Test Click Merge In More Button
     [Documentation]   ทดสอบกดปุ่ม รวมรายการ
     Click Element When Ready                  ${XPATH_BUTTON_CLOSE_COMMAND_COMPLETE_PAYMENT}
     Click Element When Ready                  ${XPATH_BUTTON_USE_COMMAND}
     Click Element When Ready                  ${XPATH_BUTTON_COMMAND_MERGE_SO}
     Wait Until Page Contains                  ยืนยันการรวมรายการ
     Capture Page Screenshot
     Click Element When Ready                  ${XPATH_BUTTON_CLOSE_COMMAND_MERGE_SO}
     Click Element When Ready                  ${XPATH_SAMPLE_CHECK_BOX}

TC_095_Test Click Merge In More Button (Disparate Customer Name Case)
     [Documentation]   ทดสอบยืนยันการรวมรายการ โดยเลือก 2 รายการที่มีชื่อลูกค้าต่างกัน
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_SAMPLE_CHECK_BOX}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_SAMPLE_CHECK_BOX2}
     Click Element When Ready                  ${XPATH_BUTTON_USE_COMMAND}
     Click Element When Ready                  ${XPATH_BUTTON_COMMAND_MERGE_SO}
     Input Text To Element When Ready          ${XPATH_TEXTBOX_CONFIRM_MERGE_SO}            confirm
     Click Element When Ready                  ${XPATH_BUTTON_CONFIRM_MERGE_SO}
     Wait Until Page Contains                  กรุณาเลือกรายการที่มีลูกค้าตรงกัน
     Capture Page Screenshot
     Click Element When Ready                  ${XPATH_BUTTON_CLOSE_ERROR_MERGE_SO}

TC_096_Test Click Merge In More Button (Same Customer Name But Disparate Sale Channel Case)
     [Documentation]   ทดสอบกดปุ่ม รวมรายการ โดยเลือก 2 รายการที่มีลูกค้าชื่อเดียวกัน แต่ช่องทางการขายต่างกัน
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_SAMPLE_CHECK_BOX}
     Click Element When Ready                  ${XPATH_SAMPLE_CHECK_BOX2}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_SAMPLE_CHECK_BOX8}
     Click Element When Ready                  ${XPATH_SAMPLE_CHECK_BOX12}
     Sleep                                     3s
     Scroll Element Into View                  ${XPATH_BUTTON_USE_COMMAND}
     Click Element When Ready                  ${XPATH_BUTTON_USE_COMMAND}
     Click Element When Ready                  ${XPATH_BUTTON_COMMAND_MERGE_SO}
     Input Text To Element When Ready          ${XPATH_TEXTBOX_CONFIRM_MERGE_SO}            confirm
     Click Element When Ready                  ${XPATH_BUTTON_CONFIRM_MERGE_SO}
     Wait Until Page Contains                  กรุณาเลือกรายการที่มีช่องทางการขายตรงกัน
     Capture Page Screenshot
     Click Element When Ready                  ${XPATH_BUTTON_CLOSE_ERROR_MERGE_SO}

TC_097_Test Click Merge In More Button (Same Customer Name, Same Sale Channel But Disparate Wharehouse Case)
     [Documentation]   ทดสอบกดปุ่ม รวมรายการ โดยเลือก 2 รายการที่มีลูกค้าชื่อเดียวกัน และช่องทางการขายเดียวกัน แต่คลังสินค้าต่างกัน
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_SAMPLE_CHECK_BOX8}
     Click Element When Ready                  ${XPATH_SAMPLE_CHECK_BOX12}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_SAMPLE_CHECK_BOX6}
     Click Element When Ready                  ${XPATH_SAMPLE_CHECK_BOX9}
     Sleep                                     3s
     Scroll Element Into View                  ${XPATH_BUTTON_USE_COMMAND}
     Click Element When Ready                  ${XPATH_BUTTON_USE_COMMAND}
     Click Element When Ready                  ${XPATH_BUTTON_COMMAND_MERGE_SO}
     Input Text To Element When Ready          ${XPATH_TEXTBOX_CONFIRM_MERGE_SO}            confirm
     Click Element When Ready                  ${XPATH_BUTTON_CONFIRM_MERGE_SO}
     Wait Until Page Contains                  กรุณาเลือกรายการที่มีคลัง/สาขาตรงกัน
     Capture Page Screenshot
     Click Element When Ready                  ${XPATH_BUTTON_CLOSE_ERROR_MERGE_SO}

TC_098_Test Click Hide In More Button
     [Documentation]   ทดสอบกดปุ่ม ซ่อน
     Click Element When Ready                  ${XPATH_SAMPLE_CHECK_BOX6}
     Click Element When Ready                  ${XPATH_SAMPLE_CHECK_BOX9}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_SAMPLE_CHECK_BOX14}
     Sleep                                     3s
     Scroll Element Into View                  ${XPATH_BUTTON_USE_COMMAND}
     Click Element When Ready                  ${XPATH_BUTTON_USE_COMMAND}
     Click Element When Ready                  ${XPATH_BUTTON_COMMAND_HIDE_SO}
     Input Text To Element When Ready          ${XPATH_TEXTBOX_CONFIRM_HIDE_SO}            confirm
     Click Element When Ready                  ${XPATH_BUTTON_CONFIRM_HIDE_SO}
     Wait Until Page Contains                  บันทึกสำเร็จ
     Capture Page Screenshot
     Click Element When Ready                  ${XPATH_BUTTON_CLOSE_SUCCESS_HIDE_SO}

TC_99_Test Click Delete In More Button
     [Documentation]   ทดสอบกดปุ่ม ลบรายการ
     Click Element When Ready                  ${XPATH_SAMPLE_CHECK_BOX13}
     Sleep                                     3s
     Scroll Element Into View                  ${XPATH_BUTTON_USE_COMMAND}
     Click Element When Ready                  ${XPATH_BUTTON_USE_COMMAND}
     Click Element When Ready                  ${XPATH_BUTTON_COMMAND_DELETE_SO}
     Input Text To Element When Ready          ${XPATH_TEXTBOX_CONFIRM_DELETE_SO}            confirm
     Click Element When Ready                  ${XPATH_BUTTON_CONFIRM_DELETE_SO}
     Wait Until Page Contains                  บันทึกสำเร็จ
     Capture Page Screenshot
     Click Element When Ready                  ${XPATH_BUTTON_CLOSE_SUCCESS_HIDE_SO}

TC_100_Test Click Void In More Button
     [Documentation]   ทดสอบกดปุ่ม ยกเลิกรายการ
     Click Element When Ready                  ${XPATH_SAMPLE_CHECK_BOX13}
     Sleep                                     3s
     Scroll Element Into View                  ${XPATH_BUTTON_USE_COMMAND}
     Click Element When Ready                  ${XPATH_BUTTON_USE_COMMAND}
     Click Element When Ready                  ${XPATH_BUTTON_COMMAND_CANCEL_SO}
     Input Text To Element When Ready          ${XPATH_TEXTBOX_CONFIRM_CANCEL_SO}            confirm
     Click Element When Ready                  ${XPATH_BUTTON_CONFIRM_CANCEL_SO}
     Wait Until Page Contains                  บันทึกสำเร็จ
     Capture Page Screenshot
     Click Element When Ready                  ${XPATH_BUTTON_CLOSE_SUCCESS_HIDE_SO}


TC_101_Test Click SO Number In Order List
     [Documentation]   ทดสอบกด หมายเลขรายการ
     Click Element When Ready                  ${XPATH_SAMPLE_TEXT_LINK_SO_NUMBER}
     Sleep                                     3s
     Wait Until Page Contains                  รายละเอียดรายการขาย
     Wait Until Page Contains                  ข้อมูล
     Wait Until Page Contains                  ลูกค้า
     Wait Until Page Contains                  สินค้า
     Wait Until Page Contains                  ข้อมูลที่อยู่ผู้รับ
     Wait Until Page Contains                  การชำระเงิน
     Wait Until Page Contains                  ค่าใช้จ่ายอื่น
     Wait Until Page Contains                  การโอนสินค้า
     Capture Page Screenshot
     Go Back

TC_102_Test Click Icon Share Link In Order List
     [Documentation]   ทดสอบกด icon รูปโซ่ (Share link)
     Wait Until Element Is Visible             ${XPATH_SAMPLE_ICON_SHARE_LINK_SO_NUMBER}
     Click Element When Ready                  ${XPATH_SAMPLE_ICON_SHARE_LINK_SO_NUMBER}
     Wait Until Page Contains                  คัดลอกเรียบร้อยแล้ว
     Capture Page Screenshot

TC_103_Test Click Customer Name In Order List
     [Documentation]   ทดสอบกดชื่อลูกค้า
     Click Element When Ready                  ${XPATH_SAMPLE_TEXT_LINK_CUSTOMER_NAME}
     Sleep                                     3s
     Wait Until Page Contains                  รายละเอียดผู้ติดต่อ
     Wait Until Page Contains                  ยอดขายเดือนนี้ (บาท)
     Wait Until Page Contains                  ยอดขายปีนี้ (บาท)
     Wait Until Page Contains                  ยอดค้างชำระ (บาท)
     Wait Until Page Contains                  ยอดขาย รายสินค้า
     Wait Until Page Contains                  รายการ
     Capture Page Screenshot
     Go Back

TC_104_Test Click Edit Shipping Date In Order List
     [Documentation]   ทดสอบกดแก้ไขวันที่ส่งสินค้า
     Click Element When Ready                  ${XPATH_SAMPLE_BUTTON_EDIT_SHIPPING_DATE}
     Sleep                                     3s
     Wait Until Page Contains                  แก้ไขข้อมูลขนส่ง
     Wait Until Page Contains                  วันส่งสินค้า
     Wait Until Page Contains                  ค่าส่ง (ที่เรียกเก็บจากลูกค้า)
     Wait Until Page Contains                  ชื่อผู้รับ
     Wait Until Page Contains                  Tracking No.
     Capture Page Screenshot
     Click Element When Ready                  ${XPATH_BUTTON_CLOSE_EDIT_SHIPPING_DATE_MODAL}

TC_105_Test Click Tranfer Status In Order List
     [Documentation]   ทดสอบกดสถานะ รอโอน, โอนบางส่วน, รอส่ง
     Click Element When Ready                  ${XPATH_SAMPLE_TEXT_LINK_EDIT_TRANFER}
     Sleep                                     3s
     Wait Until Page Contains                  โอนสินค้าออกจากคลัง
     Wait Until Page Contains                  คลังสินค้า
     Wait Until Page Contains                  วันที่โอน
     Capture Page Screenshot
     Click Element When Ready                  ${XPATH_BUTTON_CLOSE_EDIT_TRANFER_MODAL}

TC_106_Test Click Payment Status In Order List
     [Documentation]   ทดสอบกดสถานะชำระเงิน รอชำระ, ชำระบางส่วน
     Click Element When Ready                  ${XPATH_SAMPLE_TEXT_LINK_EDIT_PAYMENT}
     Sleep                                     3s
     Wait Until Page Contains                  ชำระเงิน
     Wait Until Page Contains                  จำนวนเงิน
     Wait Until Page Contains                  ช่องทางการชำระเงิน
     Wait Until Page Contains                  วันที่ชำระเงิน
     Wait Until Page Contains                  ตั้งค่า
     Capture Page Screenshot
     Click Element When Ready                  ${XPATH_BUTTON_CLOSE_EDIT_PAYMENT_MODAL}
     Sleep                                     3s

TC_107_Test Click Kebab Menu In Order List
     [Documentation]   กด 3 จุดด้านหลังรายการขาย
     Click Element When Ready                  ${XPATH_SAMPLE_BUTTON_KEBAB_MENU}
     Sleep                                     3s
     Wait Until Element Is Visible             ${XPATH_BUTTON_UNPIN_FAV_SO_IN_KEBAB_MENU}
     Wait Until Element Is Visible             ${XPATH_BUTTON_COPY_NEW_SO_IN_KEBAB_MENU}
     Wait Until Element Is Visible             ${XPATH_BUTTON_EDIT_SO_IN_KEBAB_MENU}
     Wait Until Element Is Visible             ${XPATH_BUTTON_EDIT_SHIPMENT_IN_KEBAB_MENU}
     Wait Until Element Is Visible             ${XPATH_BUTTON_PRINT_IN_KEBAB_MENU}
     Wait Until Element Is Visible             ${XPATH_BUTTON_PRINT_SHIPPING_LABEL_IN_KEBAB_MENU}
     Wait Until Element Is Visible             ${XPATH_BUTTON_PACK_IN_KEBAB_MENU}
     Wait Until Element Is Visible             ${XPATH_BUTTON_SEND_SMS_ORDER_CONFIRM_IN_KEBAB_MENU}
     Wait Until Element Is Visible             ${XPATH_BUTTON_SEND_SMS_SHIPPING_CONFIRM_IN_KEBAB_MENU}
     Wait Until Element Is Visible             ${XPATH_BUTTON_HIDE_SO_IN_KEBAB_MENU}
     Wait Until Element Is Visible             ${XPATH_BUTTON_VOID_IN_KEBAB_MENU}
     Wait Until Element Is Visible             ${XPATH_BUTTON_DELETE_IN_KEBAB_MENU}
     Capture Page Screenshot

TC_108_Test Click Kebab Menu In Order List And Then Click Unpin SO
     [Documentation]   กด 3 จุดด้านหลังรายการขาย >> เลือกคำสั่งถอนหมุดจากบนสุด
     Click Element When Ready                  ${XPATH_BUTTON_UNPIN_FAV_SO_IN_KEBAB_MENU}
     Sleep                                     3s
     Wait Until Element Is Not Visible         ${XPATH_SAMPLE_IMG_FAV_SO}
     Capture Page Screenshot

TC_109_Test Click Kebab Menu In Order List And Then Click Pin SO
     [Documentation]   กด 3 จุดด้านหลังรายการขาย >> เลือกคำสั่งปักหมุดไว้บนสุด
     Click Element When Ready                  ${XPATH_SAMPLE_BUTTON_KEBAB_MENU}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_PIN_FAV_SO_IN_KEBAB_MENU}
     Sleep                                     3s
     Wait Until Element Is Visible             ${XPATH_SAMPLE_IMG_FAV_SO}
     Capture Page Screenshot

TC_110_Test Click Kebab Menu In Order List And Then Click Copy
     [Documentation]   กด 3 จุดด้านหลังรายการขาย >> เลือกคำสั่งคัดลอก
     Click Element When Ready                  ${XPATH_SAMPLE_BUTTON_KEBAB_MENU}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_COPY_NEW_SO_IN_KEBAB_MENU}
     Sleep                                     3s
     Wait Until Page Contains                  สร้างรายการขาย
     Wait Until Page Contains                  ข้อมูล
     Wait Until Page Contains                  ลูกค้า
     Wait Until Page Contains                  สินค้า
     Wait Until Page Contains                  ข้อมูลที่อยู่ผู้รับ
     Wait Until Page Contains                  ข้อมูลการจัดส่งสินค้า
     Wait Until Page Contains                  การชำระเงิน
     Wait Until Page Contains                  คลังสินค้า/สาขา
     Capture Page Screenshot
     Go Back

TC_111_Test Click Kebab Menu In Order List And Then Click Edit
     [Documentation]   กด 3 จุดด้านหลังรายการขาย >> เลือกคำสั่งคัดลอก
     Click Element When Ready                  ${XPATH_SAMPLE_BUTTON_KEBAB_MENU}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_EDIT_SO_IN_KEBAB_MENU}
     Sleep                                     3s
     Wait Until Page Contains                  แก้ไขรายการขาย
     Wait Until Page Contains                  ข้อมูล
     Wait Until Page Contains                  ลูกค้า
     Wait Until Page Contains                  สินค้า
     Wait Until Page Contains                  ข้อมูลที่อยู่ผู้รับ
     Wait Until Page Contains                  ข้อมูลการจัดส่งสินค้า
     Capture Page Screenshot
     Go Back

TC_112_Test Click Kebab Menu In Order List And Then Click Edit Shipment
     [Documentation]   กด 3 จุดด้านหลังรายการขาย >> เลือกคำสั่งแก้ไขข้อมูลขนส่ง
     Click Element When Ready                  ${XPATH_SAMPLE_BUTTON_KEBAB_MENU}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_EDIT_SHIPMENT_IN_KEBAB_MENU}
     Sleep                                     3s
     Wait Until Page Contains                  แก้ไขข้อมูลขนส่ง
     Wait Until Page Contains                  วันส่งสินค้า
     Wait Until Page Contains                  ช่องทางจัดส่ง
     Wait Until Page Contains                  ค่าส่ง (ที่เรียกเก็บจากลูกค้า)
     Wait Until Page Contains                  ชื่อผู้รับ
     Wait Until Page Contains                  Tracking No.
     Click Element When Ready                  ${XPATH_BUTTON_CLOSE_EDIT_SHIPMENT_IN_KEBAB_MENU}
     Capture Page Screenshot

TC_113_Test Click Kebab Menu In Order List And Then Click Print
     [Documentation]   กด 3 จุดด้านหลังรายการขาย >> เลือกคำสั่งพิมพ์เอกสาร
     Click Element When Ready                  ${XPATH_SAMPLE_BUTTON_KEBAB_MENU}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_PRINT_IN_KEBAB_MENU}
     Sleep                                     3s
     Wait Until Page Contains                  พิมพ์เอกสาร
     Wait Until Page Contains                  ขนาด
     Wait Until Page Contains                  รูปแบบ
     Wait Until Page Contains                  หัวเรื่อง
     Wait Until Page Contains                  พิมพ์เอกสาร
     Wait Until Page Contains                  ตั้งค่า
     Click Element When Ready                  ${XPATH_BUTTON_CLOSE_PRINT_IN_KEBAB_MENU}
     Capture Page Screenshot

TC_114_Test Click Kebab Menu In Order List And Then Click Print Shiping Label
     [Documentation]   กด 3 จุดด้านหลังรายการขาย >> เลือกคำสั่งพิมพ์เอกสาร
     Click Element When Ready                  ${XPATH_SAMPLE_BUTTON_KEBAB_MENU}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_PRINT_SHIPPING_LABEL_IN_KEBAB_MENU}
     Sleep                                     3s
     Wait Until Page Contains                  พิมพ์ใบแปะจดหมาย/กล่อง
     Wait Until Page Contains                  ขนาด
     Click Element When Ready                  ${XPATH_BUTTON_CLOSE_PRINT_SHIPPING_LABEL_IN_KEBAB_MENU}
     Capture Page Screenshot

TC_115_Test Click Kebab Menu In Order List And Then Click Pack
     [Documentation]   กด 3 จุดด้านหลังรายการขาย >> เลือกคำสั่งแพ็คสินค้า
     ${SO_NUMBER}=                          Get Text From Element When Ready             ${XPATH_SAMPLE_TEXT_LINK_SO_NUMBER}
     Click Element When Ready                  ${XPATH_SAMPLE_BUTTON_KEBAB_MENU}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_PACK_IN_KEBAB_MENU}
     Sleep                                     3s
     Switch Window                             ${SO_NUMBER} : แพ็คสินค้า
     Wait Until Page Contains                  รายละเอียดรายการขาย
     Wait Until Page Contains                  แพ็คสินค้า
     Wait Until Page Contains                  สินค้าที่ยังไม่ได้แพ็ค
     Wait Until Page Contains                  สินค้าที่แพ็คแล้ว
     Capture Page Screenshot
     Close Window
     Switch Window                             title: รายการขาย

TC_116_Test Click Kebab Menu In Order List And Then Click Send SMS Order Confirm
     [Documentation]   กด 3 จุดด้านหลังรายการขาย >> เลือกคำสั่งส่ง SMS ยืนยันคำสั่งซื้อ
     Click Element When Ready                  ${XPATH_SAMPLE_BUTTON_KEBAB_MENU}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_SEND_SMS_ORDER_CONFIRM_IN_KEBAB_MENU}
     Sleep                                     3s
     Wait Until Page Contains                  SMS Credit ไม่เพียงพอ กรุณาเติม SMS Credit
     Click Element When Ready                  ${XPATH_BUTTON_CLOSE_SEND_SMS_ORDER_CONFIRM_IN_KEBAB_MENU}
     Capture Page Screenshot

TC_117_Test Click Kebab Menu In Order List And Then Click Send SMS Order Confirm
     [Documentation]   กด 3 จุดด้านหลังรายการขาย >> เลือกคำสั่งส่ง SMS ยืนยันการส่งสินค้า
     Click Element When Ready                  ${XPATH_SAMPLE_BUTTON_KEBAB_MENU}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_SEND_SMS_SHIPPING_CONFIRM_IN_KEBAB_MENU}
     Sleep                                     3s
     Wait Until Page Contains                  SMS Credit ไม่เพียงพอ กรุณาเติม SMS Credit
     Click Element When Ready                  ${XPATH_BUTTON_CLOSE_SEND_SMS_SHIPPING_CONFIRM_IN_KEBAB_MENU}
     Capture Page Screenshot

TC_118_Test Click Kebab Menu In Order List And Then Click Hide
     [Documentation]   กด 3 จุดด้านหลังรายการขาย >> เลือกคำสั่งซ่อน
     ${SO_NUMBER}=                             Get Text From Element When Ready             ${XPATH_SAMPLE_TEXT_LINK_SO_NUMBER}
     Click Element When Ready                  ${XPATH_SAMPLE_BUTTON_KEBAB_MENU}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_HIDE_SO_IN_KEBAB_MENU}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_CONFIRM_HIDE_SO_IN_KEBAB_MENU}
     Sleep                                     3s
     Wait Until Page Does Not Contain          ${SO_NUMBER}
     Capture Page Screenshot

TC_119_Test Click Kebab Menu In Order List And Then Click Unhide
     [Documentation]   กด 3 จุดด้านหลังรายการขาย >> เลือกคำสั่งยกเลิกซ่อน
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_LENGTH}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_LENGTH_PAST_THREE_MONTH}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_OPEN_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_OPEN_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_CHECKBOX_SHOW_HIDDEN_SO_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_SEARCH_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     3s
     ${SO_NUMBER}=                             Get Text From Element When Ready             ${XPATH_SAMPLE_TEXT_LINK_SO_NUMBER}
     ${SO_NUMBER14}=                           Get Text From Element When Ready             ${XPATH_SAMPLE_TEXT_LINK_SO_NUMBER14}
     Click Element When Ready                  ${XPATH_SAMPLE_BUTTON_KEBAB_MENU}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_UNHIDE_SO_IN_KEBAB_MENU}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_CONFIRM_UNHIDE_SO_IN_KEBAB_MENU}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_SAMPLE_BUTTON_KEBAB_MENU14}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_UNHIDE_SO_IN_KEBAB_MENU14}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_CONFIRM_UNHIDE_SO_IN_KEBAB_MENU}
     Wait Until Page Contains                  ${SO_NUMBER}
     Wait Until Page Contains                  ${SO_NUMBER14}
     Capture Page Screenshot

TC_120_Test Click Kebab Menu In Order List And Then Click Void
     [Documentation]   กด 3 จุดด้านหลังรายการขาย >> เลือกคำสั่งยกเลิกรายการ
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_SAMPLE_BUTTON_KEBAB_MENU12}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_VOID_IN_KEBAB_MENU12}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_CONFIRM_VOID_SO_IN_KEBAB_MENU}
     Sleep                                     3s
     ${Sort Result}=                           Get Text From Element When Ready              ${XPATH_SAMPLE_STATUS_SHIPPING_VOID}
     Should Be Equal                           ${Sort Result}                                ยกเลิก   
     ${Sort Result2}=                          Get Text From Element When Ready              ${XPATH_SAMPLE_STATUS_PAYMENT_VOID}
     Should Be Equal                           ${Sort Result2}                               ยกเลิก           
     Capture Page Screenshot

TC_121_Test Click Kebab Menu In Order List And Then Click Delete
     [Documentation]   กด 3 จุดด้านหลังรายการขาย >> เลือกคำสั่งลบรายการ
     ${SO_NUMBER2}=                            Get Text From Element When Ready             ${XPATH_SAMPLE_TEXT_LINK_SO_NUMBER2}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_SAMPLE_BUTTON_KEBAB_MENU2}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_DELETE_IN_KEBAB_MENU2}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_CONFIRM_DELETE_SO_IN_KEBAB_MENU}
     Sleep                                     3s  
     Wait Until Page Does Not Contain          ${SO_NUMBER2}       
     Capture Page Screenshot

# TC_060_Test Click Text Link Remain In Choose Multi Product Modal
#      [Documentation]   ทดสอบกดที่จำนวนคงเหลือ
#      Click Element When Ready                  ${XPATH_TEXTLINK_REMAIN_PRODUCT_IN_MULTI_CHOOSE_PRODUCT_MODAL}
#      Wait Until Page Contains                  รหัส
#      Wait Until Page Contains                  ชื่อคลัง/สาขา
#      Wait Until Page Contains                  จำนวนคงเหลือ	
#      Wait Until Page Contains                  จำนวนพร้อมขาย
#      Capture Page Screenshot

# TC_061_Test Click Text Link Ready For Sale In Choose Multi Product Modal
#      [Documentation]   ทดสอบกดที่จำนวนพร้อมขาย
#      Click Element When Ready                  ${XPATH_BUTTON_CLOSE_REMAIN_AND_READY_FOR_SALE_PRODUCT_IN_MULTI_CHOOSE_PRODUCT_MODAL}
#      Sleep                                     3s
#      Click Element When Ready                  ${XPATH_TEXTLINK_READY_FOR_SALE_PRODUCT_IN_MULTI_CHOOSE_PRODUCT_MODAL}
#      Wait Until Page Contains                  รหัส
#      Wait Until Page Contains                  ชื่อคลัง/สาขา
#      Wait Until Page Contains                  จำนวนคงเหลือ	
#      Wait Until Page Contains                  จำนวนพร้อมขาย
#      Capture Page Screenshot

# TC_062_Test Check Box Product In Modal And Click Choose Button In Multi Choose Product Modal
#      [Documentation]   ทดสอบเลือกสินค้าตาม check box ใน modal เลือกสินค้าหลายรายการ แล้วกดปุ่มเลือกสินค้า
#      Click Element When Ready                  ${XPATH_BUTTON_CLOSE_REMAIN_AND_READY_FOR_SALE_PRODUCT_IN_MULTI_CHOOSE_PRODUCT_MODAL}
#      Sleep                                     3s
#      Click Element When Ready                  ${XPATH_CHECKBOX_FOR_CHOOSE_MULTIPRODUCT_ROW_1}
#      Sleep                                     3s
#      Click Element When Ready                  ${XPATH_BUTTON_CHOOSE_MULTIPRODUCT}
#      ${Product Code}=                          Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_CODE2}
#      Should Be Equal                           ${Product Code}                           P0001-2
#      Capture Page Screenshot

# # TC_063_Test Check Box Product In Modal And Click Choose Button In Multi Choose Product Modal
# #      [Documentation]   ทดสอบเลือกสินค้าตาม check box ใน modal เลือกสินค้าหลายรายการ แล้วกดปุ่มเลือกสินค้า
# #      Click Element When Ready                  ${XPATH_BUTTON_CLOSE_REMAIN_AND_READY_FOR_SALE_PRODUCT_IN_MULTI_CHOOSE_PRODUCT_MODAL}
# #      Sleep                                     3s
# #      Click Element When Ready                  ${XPATH_CHECKBOX_FOR_CHOOSE_MULTIPRODUCT_ROW_1}
# #      Sleep                                     3s
# #      Click Element When Ready                  ${XPATH_BUTTON_CHOOSE_MULTIPRODUCT}
# #      ${Product Code}=                          Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_CODE2}
# #      Should Be Equal                           ${Product Code}                           P0001-2
# #      Capture Page Screenshot

# TC_064_Test Click Choose Product Button
#      [Documentation]   ทดสอบกดปุ่มเลือก
#      Click Element When Ready                  ${XPATH_BUTTON_CHOOSE_PRODUCT}
#      Wait Until Page Contains                  เลือกสินค้า
#      Capture Page Screenshot

# TC_065_Validate Choose Product Modal
#      [Documentation]   ตรวจสอบ modal เลือกสินค้า
#      Wait Until Element Is Visible             ${XPATH_ฺFIELD_SEARCH_PRODUCT}
#      Wait Until Page Contains                  สินค้า
#      Wait Until Page Contains                  สินค้าเป็นชุด
#      Wait Until Element Is Visible             ${XPATH_PICTURE_PRODUCT_ROW_1}
#      Wait Until Page Contains                  คงเหลือ
#      Wait Until Page Contains                  พร้อมขาย
#      Wait Until Page Contains                  ราคาขาย
#      Wait Until Element Is Visible             ${XPATH_BUTTON_CHOOSE_PRODUCT_ROW_1}
#      Capture Page Screenshot

# TC_066_Test Search Product Data In Choose Product Modal
#      [Documentation]   ทดสอบค้นหาข้อมูลสินค้า
#      Clear Text When Ready                     ${XPATH_ฺFIELD_SEARCH_PRODUCT}
#      Input Text To Element When Ready          ${XPATH_ฺFIELD_SEARCH_PRODUCT}        P0007
#      Press Keys                                ${XPATH_ฺFIELD_SEARCH_PRODUCT}        ENTER  
#      Sleep                                     3s 
#      Wait Until Page Contains                  P0007
#      Sleep                                     3s
#      Wait Until Page Does Not Contain Element          ${XPATH_ELEMENT_SAMPLE_ROW_2_IN_CHOOSE_PRODUCT_MODAL}         
#      Capture Page Screenshot

# TC_067_Test Swap Tab To Filter Data Product & Bundle and Validate Data In Choose Product Modal
#      [Documentation]   ทดสอบสลับ tab สำหรับ filter ข้อมูลสินค้า และ สินค้าเป็นชุด แล้วตรวจสอบความถูกต้องของข้อมูล
#      Click Element When Ready                  ${XPATH_TAB_BUNDLE_FOR_CHOOSE_PRODUCT}
#      Wait Until Page Contains                  ไม่มีข้อมูล
#      Sleep                                     3s
#      Click Element When Ready                  ${XPATH_TAB_PRODUCT_FOR_CHOOSE_PRODUCT}
#      Wait Until Page Contains                  ข้อมูลตัวอย่าง 6
#      Capture Page Screenshot

# TC_068_Test Click Text Link Remain In Choose Product Modal
#      [Documentation]   ทดสอบกดที่จำนวนคงเหลือ
#      Click Element When Ready                  ${XPATH_TEXTLINK_REMAIN_PRODUCT_IN_CHOOSE_PRODUCT_MODAL}
#      Wait Until Page Contains                  รหัส
#      Wait Until Page Contains                  ชื่อคลัง/สาขา
#      Wait Until Page Contains                  จำนวนคงเหลือ	
#      Wait Until Page Contains                  จำนวนพร้อมขาย
#      Capture Page Screenshot

# TC_069_Test Click Text Link Ready For Sale In Choose Product Modal
#      [Documentation]   ทดสอบกดที่จำนวนพร้อมขาย
#      Click Element When Ready                  ${XPATH_BUTTON_CLOSE_REMAIN_AND_READY_FOR_SALE_PRODUCT_IN_CHOOSE_PRODUCT_MODAL}
#      Sleep                                     3s
#      Click Element When Ready                  ${XPATH_TEXTLINK_READY_FOR_SALE_PRODUCT_IN_CHOOSE_PRODUCT_MODAL}
#      Wait Until Page Contains                  รหัส
#      Wait Until Page Contains                  ชื่อคลัง/สาขา
#      Wait Until Page Contains                  จำนวนคงเหลือ	
#      Wait Until Page Contains                  จำนวนพร้อมขาย
#      Capture Page Screenshot

# TC_070_Test Choose Product In Choose Product Modal
#      [Documentation]   ทดสอบกดปุ่มเลือกข้างหลังรายการสินค้าที่ต้องการ (สินค้าปกติ)
#      Click Element When Ready                  ${XPATH_BUTTON_CLOSE_REMAIN_AND_READY_FOR_SALE_PRODUCT_IN_CHOOSE_PRODUCT_MODAL}
#      Sleep                                     3s
#      Clear Text When Ready                     ${XPATH_ฺFIELD_SEARCH_PRODUCT}
#      Input Text To Element When Ready          ${XPATH_ฺFIELD_SEARCH_PRODUCT}        P0007
#      Press Keys                                ${XPATH_ฺFIELD_SEARCH_PRODUCT}        ENTER 
#      Sleep                                     3s
#      Click Element When Ready                  ${XPATH_BUTTON_CHOOSE_PRODUCT_ROW_1}
#      ${Product Code}=                          Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_CODE}
#      Should Be Equal                           ${Product Code}                           P0007
#      Capture Page Screenshot

# # TC_071_Test Choose Bundle In Choose Product Modal
# #      [Documentation]   ทดสอบกดปุ่มเลือกข้างหลังรายการสินค้าที่ต้องการ (สินค้าเป็นชุด)
# #      Clear Text When Ready                     ${XPATH_ฺFIELD_SEARCH_PRODUCT}
# #      Input Text To Element When Ready          ${XPATH_ฺFIELD_SEARCH_PRODUCT}        P0007
# #      Press Keys                                ${XPATH_ฺFIELD_SEARCH_PRODUCT}        ENTER 
# #      Sleep                                     3s
# #      Click Element When Ready                  ${XPATH_BUTTON_CHOOSE_PRODUCT_ROW_1}
# #      ${Product Code}=                          Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_CODE}
# #      Should Be Equal                           ${Product Code}                           P0007
# #      Capture Page Screenshot

# TC_072_Test Input Data In Product Code Text Box
#      [Documentation]   ทดสอบกรอกรหัสสินค้าลงใน text box รหัส
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_CODE}
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_CODE}        TESTPRODUCTCODE01
#      Click Element When Ready                  ${XPATH_FIELD_PRODUCT_NAME}
#      Capture Page Screenshot

# TC_073_Test Input Data In Product Code Text Box 3 Digit For Validate Auto Search
#      [Documentation]   ทดสอบกรอกรหัสขึ้นต้นของสินค้าที่มีอยู่แล้วในระบบ 3 ตัวอักษร ลงใน text box รหัส เพื่อทดสอบระบบ auto search
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_CODE}
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_CODE}        P00
#      Sleep                                     5s
#      Wait Until Page Contains                  P0001-2
#      Wait Until Page Contains                  P0001-1
#      Wait Until Page Contains                  P0007
#      Wait Until Page Contains                  P0006
#      Capture Page Screenshot

# TC_074_Test Choose Data In Auto Search Dropdown (PRODUCT CODE TEXT BOX)
#      [Documentation]   ทดสอบเลือกข้อมูลใน dropdown ของช่อง text box รหัส
#      Click Element When Ready                  ${XPATH_SAMPLE_CODE_IN_DROPDOWN_AUTO_SEARCH}
#      ${Product Code}=                          Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_CODE}
#      Should Be Equal                           ${Product Code}                           P0003
#      Capture Page Screenshot

# TC_075_Test Input Data In Product Code Text Box
#      [Documentation]   ทดสอบกรอกชื่อสินค้าลงใน text box ชื่อสินค้า
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_NAME}
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_NAME}        TESTPRODUCTNAME01
#      Click Element When Ready                  ${XPATH_FIELD_PRODUCT_CODE}
#      Capture Page Screenshot

# TC_076_Test Input Already Data In Textbox Product Name
#      [Documentation]   ทดสอบกรอกชื่อขึ้นต้นของสินค้าที่มีอยู่แล้วในระบบลงใน text box รหัส เพื่อทดสอบระบบ auto search
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_NAME}
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_NAME}        ข้อมูล
#      Sleep                                     10s
#      Wait Until Page Contains                  ข้อมูลตัวอย่าง 6
#      Wait Until Page Contains                  ข้อมูลตัวอย่าง 5
#      Wait Until Page Contains                  ข้อมูลตัวอย่าง 4
#      Wait Until Page Contains                  ข้อมูลตัวอย่าง 3
#      Capture Page Screenshot

# TC_077_Choose Data In Auto Search Dropdown (PRODUCT NAME TEXT BOX)
#      [Documentation]   ทดสอบเลือกข้อมูลใน dropdown ของช่อง text box ชื่อสินค้า
#      Click Element When Ready                  ${XPATH_SAMPLE_PRODUCT_NAME_IN_DROPDOWN_AUTO_SEARCH}
#      ${Product Name}=                          Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_NAME}
#      Should Be Equal                           ${Product Name}                           ข้อมูลตัวอย่าง 2
#      Capture Page Screenshot

# TC_078_Test Input Integer In Quantity Text Box
#      [Documentation]   ทดสอบกรอกจำนวนเต็มลงใน text box จำนวน
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_QUANTITY}
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_QUANTITY}        50
#      Click Element When Ready                  ${XPATH_FIELD_PRODUCT_NAME}
#      ${Product Quantity}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_QUANTITY}
#      Should Be Equal                           ${Product Quantity}                           50.00
#      Capture Page Screenshot

# TC_079_Test Input Decimal In Quantity Text Box
#      [Documentation]   ทดสอบกรอกจำนวนที่มีจุดทศนิยมลงใน text box จำนวน
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_QUANTITY}
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_QUANTITY}        50.50
#      Click Element When Ready                  ${XPATH_FIELD_PRODUCT_NAME}
#      ${Product Quantity}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_QUANTITY}
#      Should Be Equal                           ${Product Quantity}                           50.50
#      Capture Page Screenshot

# TC_080_Test Input Negative Number In Quantity Text Box
#      [Documentation]   ทดสอบกรอกจำนวนติดลบลงใน text box จำนวน
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_QUANTITY}
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_QUANTITY}        -6
#      Click Element When Ready                  ${XPATH_FIELD_PRODUCT_NAME}
#      ${Product Quantity}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_QUANTITY}
#      Should Be Equal                           ${Product Quantity}                           -6.00
#      Capture Page Screenshot

# TC_081_Test Input Text In Quantity Text Box
#      [Documentation]   ทดสอบกรอกตัวอักษรลงในช่อง text box จำนวน
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_QUANTITY}
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_QUANTITY}        test1234
#      Click Element When Ready                  ${XPATH_FIELD_PRODUCT_NAME}
#      ${Product Quantity}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_QUANTITY}
#      Should Be Equal                           ${Product Quantity}                    ${EMPTY}                      
#      Capture Page Screenshot

# TC_082_Test Input Integer In Price Text Box
#      [Documentation]   ทดสอบกรอกจำนวนเต็มลงใน text box มูลค่าต่อหน่วย
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_PRICE}
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_PRICE}        10000
#      Click Element When Ready                  ${XPATH_FIELD_PRODUCT_DISCOUNT}
#      ${Product Price}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_PRICE}
#      Should Be Equal                           ${Product Price}                    10,000.00                      
#      Capture Page Screenshot

# TC_083_Test Decimal Text In Price Text Box
#      [Documentation]   ทดสอบกรอกจำนวนที่มีจุดทศนิยมลงใน text box มูลค่าต่อหน่วย
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_PRICE}
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_PRICE}        10000.52
#      Click Element When Ready                  ${XPATH_FIELD_PRODUCT_DISCOUNT}
#      ${Product Price}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_PRICE}
#      Should Be Equal                           ${Product Price}                    10,000.52                      
#      Capture Page Screenshot

# TC_084_Test Input Negative Number In Price Text Box
#      [Documentation]   ทดสอบกรอกจำนวนติดลบลงใน text box มูลค่าต่อหน่วย
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_PRICE}
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_PRICE}        -10000
#      Click Element When Ready                  ${XPATH_FIELD_PRODUCT_DISCOUNT}
#      ${Product Price}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_PRICE}
#      Should Be Equal                           ${Product Price}                    -10,000.00                      
#      Capture Page Screenshot

# TC_085_Test Input Text In Price Text Box
#      [Documentation]   ทดสอบกรอกตัวอักษรลงในช่อง text box มูลค่าต่อหน่วย
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_PRICE}
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_PRICE}        test1234
#      Click Element When Ready                  ${XPATH_FIELD_PRODUCT_DISCOUNT}
#      ${Product Price}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_PRICE}
#      Should Be Equal                           ${Product Price}                    ${EMPTY}                      
#      Capture Page Screenshot

# TC_086_Test Input Integer In Discount Text Box
#      [Documentation]   ทดสอบกรอกจำนวนเต็มลงใน text box ส่วนลดต่อหน่วย
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_DISCOUNT}
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_DISCOUNT}        10000
#      Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
#      ${Product Discount}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_DISCOUNT}
#      Should Be Equal                           ${Product Discount}                    10,000.00                      
#      Capture Page Screenshot

# TC_087_Test Decimal Text In Discount Text Box
#      [Documentation]   ทดสอบกรอกจำนวนที่มีจุดทศนิยมลงใน text box ส่วนลดต่อหน่วย
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_DISCOUNT}
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_DISCOUNT}        10000.52
#      Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
#      ${Product Discount}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_DISCOUNT}
#      Should Be Equal                           ${Product Discount}                    10,000.52                      
#      Capture Page Screenshot

# TC_088_Test Input Negative Number In Discount Text Box
#      [Documentation]   ทดสอบกรอกจำนวนติดลบลงใน text box ส่วนลดต่อหน่วย
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_DISCOUNT}
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_DISCOUNT}        -10000
#      Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
#      ${Product Discount}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_DISCOUNT}
#      Should Be Equal                           ${Product Discount}                    -10,000.00                      
#      Capture Page Screenshot

# TC_089_Test Input Text In Discount Text Box
#      [Documentation]   ทดสอบกรอกตัวอักษรลงในช่อง text box ส่วนลดต่อหน่วย
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_DISCOUNT}
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_DISCOUNT}        test1234
#      Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
#      ${Product Discount}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_DISCOUNT}
#      Should Be Equal                           ${Product Discount}                    ${EMPTY}                      
#      Capture Page Screenshot

# TC_090_Test Input Percent In Discount Text Box
#      [Documentation]   ทดสอบกรอกจำนวนตามด้วยเปอร์เซ็นลงใน text box ส่วนลดต่อหน่วย
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_DISCOUNT}
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_DISCOUNT}        7%
#      Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
#      ${Product Discount}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_DISCOUNT}
#      Should Be Equal                           ${Product Discount}                    7.00%                     
#      Capture Page Screenshot

# TC_091_Validate Summary Amount (Normal Discount)
#      [Documentation]   ตรวจสอบผลคำนวนส่วนลดต่อหน่วยที่ รวม
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_CODE}
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_NAME}
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_QUANTITY}
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_PRICE}
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_DISCOUNT}
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_CODE}            TESTSUM
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_NAME}            TESTSUM
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_QUANTITY}        2    
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_PRICE}           100
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_DISCOUNT}        25
#      Click Element When Ready                  ${XPATH_TEXT_SUMMARY_AMOUNT}
#      Sleep                                     5s
#      ${Summary Amount}=                        Get Text From Element When Ready             ${XPATH_TEXT_TOTAL_PRICE}
#      Should Be Equal                           ${Summary Amount}                      150.00                    
#      Capture Page Screenshot

# TC_092_Test Click Add Product Button
#      [Documentation]   ทดสอบกดปุ่ม + เพิ่่มสินค้า
#      Click Element When Ready                  ${XPATH_BUTTON_ADD_PRODUCT}
#      Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
#      Wait Until Element Is Visible             ${XPATH_FIELD_PRODUCT_CODE3}                   
#      Capture Page Screenshot

# TC_093_Validate Summary Amount (Percent Discount)
#      [Documentation]   ตรวจสอบผลคำนวนส่วนลดต่อหน่วยที่ รวม
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_CODE3}            TESTSUM2
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_NAME3}            TESTSUM2
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_QUANTITY3}        2    
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_PRICE3}           100
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_DISCOUNT3}        30%
#      Click Element When Ready                  ${XPATH_TEXT_SUMMARY_AMOUNT}
#      Sleep                                     5s
#      ${Summary Amount}=                        Get Text From Element When Ready              ${XPATH_TEXT_TOTAL_PRICE3}
#      Should Be Equal                           ${Summary Amount}                      140.00                    
#      Capture Page Screenshot

# TC_094_Test Input Data In Shipping Text Box
#      [Documentation]   ทดสอบกรอกข้อมูลลงใน text box ช่องทางจัดส่ง
#      Input Text To Element When Ready          ${XPATH_FIELD_SHIPPING_CHANNEL}        Ninja Van
#      Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
#      ${Shipping}=                              Get Value From Element When Ready              ${XPATH_FIELD_SHIPPING_CHANNEL}
#      Should Be Equal                           ${Shipping}                            Ninja Van                    
#      Capture Page Screenshot

# TC_095_Test Choose Data In Dropdown Shipping Text Box
#      [Documentation]   ทดสอบเลือกข้อมูลใน dropdown ช่องทางจัดส่ง
#      Clear Text When Ready                     ${XPATH_FIELD_SHIPPING_CHANNEL}
#      Sleep                                     5s
#      Click Element When Ready                  ${XPATH_SAMPLE_CHOOSE_SHIPPING}
#      ${Shipping}=                              Get Value From Element When Ready              ${XPATH_FIELD_SHIPPING_CHANNEL}
#      Should Be Equal                           ${Shipping}                            Kerry                    
#      Capture Page Screenshot

# TC_096_Test Choose Data In Dropdown Shipping Text Box
#      [Documentation]   ทดสอบกรอกข้อมูลลงใน text box หมายเหตุ
#      Input Text To Element When Ready          ${XPATH_FIELD_DESCRIPTION}                     ทดสอบหมายเหตุ
#      ${Shipping}=                              Get Value From Element When Ready              ${XPATH_FIELD_DESCRIPTION}
#      Should Be Equal                           ${Shipping}                            ทดสอบหมายเหตุ                    
#      Capture Page Screenshot

# TC_097_Test Input Integer In Discount Total Text Box
#      [Documentation]   ทดสอบกรอกจำนวนเต็มลงใน text box ส่วนลด
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}        10000
#      Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
#      ${Product Discount All}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}
#      Should Be Equal                           ${Product Discount All}                    10,000.00                      
#      Capture Page Screenshot

# TC_098_Test Decimal Text In Discount Total Text Box
#      [Documentation]   ทดสอบกรอกจำนวนที่มีจุดทศนิยมลงใน text box ส่วนลด
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}        10000.52
#      Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
#      ${Product Discount All}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}
#      Should Be Equal                           ${Product Discount All}                    10,000.52                      
#      Capture Page Screenshot

# TC_099_Test Input Negative Number In Discount Total Text Box
#      [Documentation]   ทดสอบกรอกจำนวนติดลบลงใน text box ส่วนลด
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}        -10000
#      Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
#      ${Product Discount All}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}
#      Should Be Equal                           ${Product Discount All}                    -10,000.00                      
#      Capture Page Screenshot

# TC_100_Test Input Text In Discount Total Text Box
#      [Documentation]   ทดสอบกรอกตัวอักษรลงในช่อง text box ส่วนลด
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}        test1234
#      Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
#      ${Product Discount All}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}
#      Should Be Equal                           ${Product Discount All}                    ${EMPTY}                      
#      Capture Page Screenshot

# TC_101_Test Input Percent In Discount Total Text Box
#      [Documentation]   ทดสอบกรอกจำนวนตามด้วยเปอร์เซ็นลงใน text box ส่วนลด
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}        7%
#      Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
#      ${Product Discount All}=                      Get Value From Element When Ready         ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}
#      Should Be Equal                           ${Product Discount All}                    7.00%                     
#      Capture Page Screenshot

# TC_102_Validate Discount Total
#      [Documentation]   ตรวจสอบผลคำนวนส่วนลด
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_CODE}
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_NAME}
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_QUANTITY}
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_PRICE}
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_DISCOUNT}
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_CODE2}
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_NAME2}
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_QUANTITY2}
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_PRICE2}
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_DISCOUNT2}
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_CODE3}
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_NAME3}
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_QUANTITY3}
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_PRICE3}
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_DISCOUNT3}
#      Clear Text When Ready                     ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_CODE}                      TestProductCode01
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_NAME}                      TestProductName01
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_QUANTITY}                  2
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_PRICE}                     5000
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_DISCOUNT}                  500
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_CODE2}                     TestProductCode02
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_Name2}                     TestProductName02
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_QUANTITY2}                 2
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_PRICE2}                    5000
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_DISCOUNT2}                 500
#      Input Text To Element When Ready          ${XPATH_FIELD_PRODUCT_DISCOUT_TOTAL_ORDER}       500
#      Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
#      ${Total}=                      Get Text From Element When Ready         ${XPATH_TEXT_SUMMARY_AMOUNT}
#      Should Be Equal                           ${Total}                    17,500.00                    
#      Capture Page Screenshot

# TC_103_Test Input Integer In Shipping Price Text Box
#      [Documentation]   ทดสอบกรอกจำนวนเต็มลงใน text box ค่าส่ง
#      Clear Text When Ready                     ${XPATH_FIELD_SHIPPING_PRICE}
#      Input Text To Element When Ready          ${XPATH_FIELD_SHIPPING_PRICE}        500
#      Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
#      ${Shipping Price}=                      Get Value From Element When Ready         ${XPATH_FIELD_SHIPPING_PRICE}
#      Should Be Equal                           ${Shipping Price}                    500.00                      
#      Capture Page Screenshot

# TC_104_Test Decimal Text In Shipping Price Text Box
#      [Documentation]   ทดสอบกรอกจำนวนที่มีจุดทศนิยมลงใน text box ค่าส่ง
#      Clear Text When Ready                     ${XPATH_FIELD_SHIPPING_PRICE}
#      Input Text To Element When Ready          ${XPATH_FIELD_SHIPPING_PRICE}        500.50
#      Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
#      ${Shipping Price}=                      Get Value From Element When Ready         ${XPATH_FIELD_SHIPPING_PRICE}
#      Should Be Equal                           ${Shipping Price}                    500.50                      
#      Capture Page Screenshot

# TC_105_Test Input Negative Number In Shipping Price Text Box
#      [Documentation]   ทดสอบกรอกจำนวนติดลบลงใน text box ค่าส่ง
#      Clear Text When Ready                     ${XPATH_FIELD_SHIPPING_PRICE}
#      Input Text To Element When Ready          ${XPATH_FIELD_SHIPPING_PRICE}        -500
#      Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
#      ${Shipping Price}=                      Get Value From Element When Ready         ${XPATH_FIELD_SHIPPING_PRICE}
#      Should Be Equal                           ${Shipping Price}                    -500.00                     
#      Capture Page Screenshot

# TC_106_Test Input Text In Shipping Price Text Box
#      [Documentation]   ทดสอบกรอกตัวอักษรลงในช่อง text box ค่าส่ง
#      Clear Text When Ready                     ${XPATH_FIELD_SHIPPING_PRICE}
#      Input Text To Element When Ready          ${XPATH_FIELD_SHIPPING_PRICE}        test1234
#      Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
#      ${Shipping Price}=                      Get Value From Element When Ready         ${XPATH_FIELD_SHIPPING_PRICE}
#      Should Be Equal                           ${Shipping Price}                    ${EMPTY}                      
#      Capture Page Screenshot

# TC_107_Validate Shipping Price Total
#      [Documentation]   ตรวจสอบผลคำนวนค่าส่ง
#      Clear Text When Ready                     ${XPATH_FIELD_SHIPPING_PRICE}
#      Input Text To Element When Ready          ${XPATH_FIELD_SHIPPING_PRICE}        500
#      Click Element When Ready                  ${XPATH_FIELD_PRODUCT_PRICE}
#      ${Total}=                      Get Text From Element When Ready         ${XPATH_TEXT_SUMMARY_AMOUNT}
#      Should Be Equal                           ${Total}                    18,000.00                    
#      Capture Page Screenshot