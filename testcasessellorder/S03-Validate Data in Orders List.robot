*** Settings ***
Resource          ${CURDIR}/../resources/testdata/testdata.robot
Resource          ${CURDIR}/../resources/variables/variable_config.robot
Resource          ${CURDIR}/../resources/variables/variable_login.robot
Resource          ${CURDIR}/../resources/variables/variable_home.robot
Resource          ${CURDIR}/../resources/variables/variable_sellorder.robot
Resource          ${CURDIR}/../keywords/common/common_keywords.robot
Suite Teardown    Close All Browsers


*** Test Cases ***
TC_048_Validate SO Data, Quantity And Total Amount
     [Documentation]   ตรวจสอบข้อมูลรายการขาย จำนวนรายการ, มูลค่าทั้งหมด
     Open Browser      ${URL}      ${BROWSER}
     Set Window Size    1920   1080            
     Input Text To Element When Ready          ${XPATH_FIELD_USERNAME}       ${EMAIL}           
     Input Text To Element When Ready          ${XPATH_FIELD_PASSWORD}       ${PASSWORD}     
     Click Element When Ready                  ${XPATH_BUTTON_LOGIN}
     Sleep                                     10s
     ${popup1}  Run Keyword And Return Status    Element Should Be Visible         ${XPATH_CLOSE_POPUP_EXPIRE}
       Log  ${popup1}
       Run Keyword If    '${popup1}'=='True'       Click Element When Ready                     ${XPATH_CLOSE_POPUP_EXPIRE}
       Run Keyword If    '${popup1}'=='False'      Pop Up Not Avalible
     ${popup2}  Run Keyword And Return Status    Element Should Be Visible         ${XPATH_CLOSE_POPUP}
       Log  ${popup2}
       Run Keyword If    '${popup2}'=='True'       Click Element When Ready                     ${XPATH_CLOSE_POPUP}
       Run Keyword If    '${popup2}'=='False'      Pop Up Not Avalible
     # Wait Until Element Is Visible             ${XPATH_CLOSE_POPUP_EXPIRE}          20s
     # Click Element When Ready                  ${XPATH_CLOSE_POPUP_EXPIRE}
     # Wait Until Element Is Visible             ${XPATH_CLOSE_POPUP}                 20s
     # Click Element When Ready                  ${XPATH_CLOSE_POPUP}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU_SELL_ORDER}
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 4              
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 1
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 7
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 6
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 3
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 8
     Wait Until Page Contains                  142,500
     Wait Until Page Contains                  91,200
     Wait Until Page Contains                  84,600	
     Wait Until Page Contains                  60,000
     Wait Until Page Contains                  57,000
     Wait Until Page Contains                  43,700
     Capture Page Screenshot
   
TC_049_Validate Tab Status
     [Documentation]   ตรวจสอบข้อมูล "แท็บสถานะ"
     Wait Until Element Is Visible                ${XPATH_TAB_STATUS_ALL}                                             
     Wait Until Element Is Visible                ${XPATH_TAB_STATUS_WAIT_TO_SHIPPING}                                 
     Wait Until Element Is Visible                ${XPATH_TAB_STATUS_WAIT_TO_PAY}                               
     Wait Until Element Is Visible                ${XPATH_TAB_STATUS_COMPLETE}                                          
     Capture Page Screenshot

TC_050_Validate Refresh Button
     [Documentation]   ตรวจสอบปุ่ม refresh
     Wait Until Element Is Visible                 ${XPATH_BUTTON_REFRESH_ORDER_LIST}
     Capture Page Screenshot

TC_051_Validate Sell Order Column
     [Documentation]   ตรวจสอบหัวคอลัมน์รายการขาย 
     Wait Until Element Is Visible                 ${XPATH_SORT_DATE_ASC}
     Wait Until Element Is Visible                 ${XPATH_SORT_ORDER_NO_ASC}
     Wait Until Element Is Visible                 ${XPATH_SORT_CUSTOMER_ASC}
     Wait Until Element Is Visible                 ${XPATH_SORT_SALE_CHANNEL_ASC}
     Wait Until Element Is Visible                 ${XPATH_SORT_SHIPPING_DATE_ASC}
     Wait Until Element Is Visible                 ${XPATH_SORT_AMOUNT_ASC}
     Wait Until Element Is Visible                 ${XPATH_SORT_STATUS_SHIPPING_ASC}
     Wait Until Element Is Visible                 ${XPATH_SORT_STATUS_PAYMENT_ASC}
     Capture Page Screenshot

TC_052_Test Sort By Date (ASC)
     [Documentation]   ทดสอบกด Sort เพื่อเรียงข้อมูลวันที่ โดยกดคำว่าวันที่
     Click Element When Ready                      ${XPATH_SORT_DATE_ASC}
     ${Sort Result}=                               Get Text From Element When Ready              ${XPATH_SAMPLE_SORT_DATE}
     Should Be Equal                               ${Sort Result}                                ${Sort Result}                
     Capture Page Screenshot                  

TC_053_Test Sort By Date (DESC)
     [Documentation]   ทดสอบกด Sort เพื่อเรียงข้อมูลวันที่ โดยกดคำว่าวันที่อีกครั้ง
     Click Element When Ready                      ${XPATH_SORT_DATE_DESC}
     ${Sort Result}=                               Get Text From Element When Ready              ${XPATH_SAMPLE_SORT_DATE}
     Should Be Equal                               ${Sort Result}                                วันนี้                 
     Capture Page Screenshot

TC_054_Test Sort By SO Number (ASC)
     [Documentation]   ทดสอบกด Sort เพื่อเรียงข้อมูลหมายเลขรายการ โดยกดคำว่ารายการ
     Click Element When Ready                      ${XPATH_SORT_ORDER_NO_ASC} 
     ${Sort Result}=                               Get Text From Element When Ready              ${XPATH_SAMPLE_SORT_ORDER_NO}
     Should Be Equal                               ${Sort Result}                                ${Sort Result}                
     Capture Page Screenshot

TC_055_Test Sort By SO Number (DESC)
     [Documentation]   ทดสอบกด Sort เพื่อเรียงข้อมูลหมายเลขรายการ โดยกดคำว่ารายการอีกครั้ง
     Click Element When Ready                      ${XPATH_SORT_ORDER_NO_DESC} 
     ${Sort Result}=                               Get Text From Element When Ready              ${XPATH_SAMPLE_SORT_ORDER_NO}
     Should Be Equal                               ${Sort Result}                                ${Sort Result}               
     Capture Page Screenshot

TC_056_Test Sort By Customer Name (ASC)
     [Documentation]   ทดสอบกด Sort เพื่อเรียงรายชื่อลูกค้า โดยกดคำว่าลูกค้า
     Click Element When Ready                      ${XPATH_SORT_CUSTOMER_ASC} 
     ${Sort Result}=                               Get Text From Element When Ready              ${XPATH_SAMPLE_SORT_CUSTOMER} 
     Should Be Equal                               ${Sort Result}                                -                
     Capture Page Screenshot

TC_057_Test Sort By Customer Name (DESC)
     [Documentation]   ทดสอบกด Sort เพื่อเรียงข้อมูลลูกค้า โดยกดคำว่าลูกค้าอีกครั้ง
     Click Element When Ready                      ${XPATH_SORT_CUSTOMER_DESC} 
     ${Sort Result}=                               Get Text From Element When Ready              ${XPATH_SAMPLE_SORT_CUSTOMER} 
     Should Be Equal                               ${Sort Result}                                ข้อมูลตัวอย่าง 8               
     Capture Page Screenshot
     
TC_058_Test Sort By Sale Channel (ASC)
     [Documentation]   ทดสอบกด Sort เพื่อเรียงข้อมูลช่องทางการขาย โดยกดคำว่าช่องทาง
     Click Element When Ready                      ${XPATH_SORT_SALE_CHANNEL_ASC} 
     ${Sort Result}=                               Get Text From Element When Ready              ${XPATH_SAMPLE_SORT_SALE_CHANNEL} 
     Should Be Equal                               ${Sort Result}                                -                
     Capture Page Screenshot

TC_059_Test Sort By Sale Channel (DESC)
     [Documentation]   ทดสอบกด Sort เพื่อเรียงข้อมูลช่องทางการขาย โดยกดคำว่าช่องทางอีกครั้ง
     Click Element When Ready                      ${XPATH_SORT_SALE_CHANNEL_DESC} 
     ${Sort Result}=                               Get Text From Element When Ready              ${XPATH_SAMPLE_SORT_SALE_CHANNEL} 
     Should Be Equal                               ${Sort Result}                                ตัวแทนจำหน่าย                
     Capture Page Screenshot

TC_060_Test Sort By Shipping Date (ASC)
     [Documentation]   ทดสอบกด Sort เพื่อเรียงข้อมูลวันที่จัดส่งสินค้า โดยกดคำว่าวันส่งสินค้า
     Click Element When Ready                      ${XPATH_SORT_SHIPPING_DATE_ASC} 
     ${Sort Result}=                               Get Text From Element When Ready              ${XPATH_SAMPLE_SORT_SHIPPING_DATE} 
     Should Be Equal                               ${Sort Result}                                แก้ไข                
     Capture Page Screenshot

TC_061_Test Sort By Shipping Date (DESC)
     [Documentation]   ทดสอบกด Sort เพื่อเรียงข้อมูลวันที่จัดส่งสินค้า โดยกดคำว่า วันส่งสินค้าอีกครั้ง
     Click Element When Ready                      ${XPATH_SORT_SHIPPING_DATE_DESC} 
     ${Sort Result}=                               Get Text From Element When Ready              ${XPATH_SAMPLE_SORT_SHIPPING_DATE} 
     Should Be Equal                               ${Sort Result}                                วันนี้                
     Capture Page Screenshot

TC_062_Test Sort By Amount (ASC)
     [Documentation]   ทดสอบกด Sort เพื่อเรียงข้อมูลมูลค่าของรายการขาย โดยกดคำว่ามูลค่า
     Click Element When Ready                      ${XPATH_SORT_AMOUNT_ASC} 
     ${Sort Result}=                               Get Text From Element When Ready              ${XPATH_SAMPLE_SORT_AMOUNT}
     Should Be Equal                               ${Sort Result}                                6,000                
     Capture Page Screenshot

TC_063_Test Sort By Amount (DESC)
     [Documentation]   ทดสอบกด Sort เพื่อเรียงข้อมูลมูลค่าของรายการขาย โดยกดคำว่ามูลค่าอีกครั้ง
     Click Element When Ready                      ${XPATH_SORT_AMOUNT_DESC} 
     ${Sort Result}=                               Get Text From Element When Ready              ${XPATH_SAMPLE_SORT_AMOUNT} 
     Should Be Equal                               ${Sort Result}                                142,500                
     Capture Page Screenshot

TC_064_Test Sort By Shiping Status (ASC)
     [Documentation]   ทดสอบกด Sort เพื่อเรียงข้อมูลสถานะของรายการขาย โดยกดคำว่าสถานะ
     Click Element When Ready                      ${XPATH_SORT_STATUS_SHIPPING_ASC} 
     ${Sort Result}=                               Get Text From Element When Ready              ${XPATH_SAMPLE_STATUS_SHIPPING_WAITING}
     Should Be Equal                               ${Sort Result}                                รอโอน               
     Capture Page Screenshot


TC_065_Test Sort By Shiping Status (DESC)
     [Documentation]   ทดสอบกด Sort เพื่อเรียงข้อมูลสถานะของรายการขาย โดยกดคำว่าสถานะอีกครั้ง
     Click Element When Ready                      ${XPATH_SORT_STATUS_SHIPPING_DESC} 
     ${Sort Result}=                               Get Text From Element When Ready              ${XPATH_SAMPLE_STATUS_SHIPPING_SUCCESS}
     Should Be Equal                               ${Sort Result}                                สำเร็จ              
     Capture Page Screenshot

TC_066_Test Sort By Payment Status (ASC)
     [Documentation]   ทดสอบกด Sort เพื่อเรียงข้อมูลสถานะการชำระเงินของรายการขาย โดยกดคำว่าชำระเงิน
     Click Element When Ready                      ${XPATH_SORT_STATUS_PAYMENT_ASC} 
     ${Sort Result}=                               Get Text From Element When Ready              ${XPATH_SAMPLE_STATUS_PAYMENT}
     Should Be Equal                               ${Sort Result}                                รอชำระ              
     Capture Page Screenshot

TC_067_Test Sort By Payment Status (DESC)
     [Documentation]   ทดสอบกด Sort เพื่อเรียงข้อมูลสถานะการชำระเงินของรายการขาย โดยกดคำว่าชำระเงินอีกครั้ง
     Click Element When Ready                      ${XPATH_SORT_STATUS_PAYMENT_DESC} 
     ${Sort Result}=                               Get Text From Element When Ready              ${XPATH_SAMPLE_STATUS_PAYMENT}
     Should Be Equal                               ${Sort Result}                                ชำระครบ              
     Capture Page Screenshot

TC_068_Validate Data In Order List
     [Documentation]   ตรวจสอบข้อมููลในตารางรายการขาย
     Wait Until Page Contains                      ข้อมูลตัวอย่าง 1
     Wait Until Page Contains                      LINE
     Wait Until Page Contains                      24,000
     Wait Until Page Contains                      สำเร็จ
     Wait Until Page Contains                      ชำระบางส่วน
     Capture Page Screenshot