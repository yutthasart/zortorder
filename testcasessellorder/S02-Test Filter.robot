*** Settings ***
Resource          ${CURDIR}/../resources/testdata/testdata.robot
Resource          ${CURDIR}/../resources/variables/variable_config.robot
Resource          ${CURDIR}/../resources/variables/variable_login.robot
Resource          ${CURDIR}/../resources/variables/variable_home.robot
Resource          ${CURDIR}/../resources/variables/variable_sellorder.robot
Resource          ${CURDIR}/../keywords/common/common_keywords.robot
Suite Teardown    Close All Browsers

*** Test Cases ***
TC_007_Test Filter Data By Date & Range Date To Day
     [Documentation]   ทดสอบกรองข้อมูลจาก วันที่ และเลือกระยะเวลาวันนี้
     Open Browser      ${URL}      ${BROWSER}
     Set Window Size    1920   1080            
     Input Text To Element When Ready          ${XPATH_FIELD_USERNAME}       ${EMAIL}           
     Input Text To Element When Ready          ${XPATH_FIELD_PASSWORD}       ${PASSWORD}     
     Click Element When Ready                  ${XPATH_BUTTON_LOGIN}
     Sleep                                     10s
     ${popup1}  Run Keyword And Return Status    Element Should Be Visible           ${XPATH_CLOSE_POPUP_EXPIRE}
       Log  ${popup1}
       Run Keyword If    '${popup1}'=='True'       Click Element When Ready                     ${XPATH_CLOSE_POPUP_EXPIRE}
       Run Keyword If    '${popup1}'=='False'      Pop Up Not Avalible
     ${popup2}  Run Keyword And Return Status    Element Should Be Visible           ${XPATH_CLOSE_POPUP}
       Log  ${popup2}
       Run Keyword If    '${popup2}'=='True'       Click Element When Ready                     ${XPATH_CLOSE_POPUP}
       Run Keyword If    '${popup2}'=='False'      Pop Up Not Avalible
     # Wait Until Element Is Visible             ${XPATH_CLOSE_POPUP_EXPIRE}          20s
     # Click Element When Ready                  ${XPATH_CLOSE_POPUP_EXPIRE}
     # Wait Until Element Is Visible             ${XPATH_CLOSE_POPUP}                 20s
     # Click Element When Ready                  ${XPATH_CLOSE_POPUP}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU_SELL_ORDER}
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_LENGTH}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_LENGTH_TODAY}
     Wait Until Page Contains                  วันนี้
     Capture Page Screenshot
   
TC_008_Test Filter Data By Payment Date & Range Date To Day
     [Documentation]   ทดสอบกรองข้อมูลจาก วันที่ชำระเงิน และเลือกระยะเวลาวันนี้
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_SELL_ORDER}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_SELL_ORDER_PAYMENT_DATE}
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_LENGTH}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_LENGTH_TODAY}
     Wait Until Page Contains                  วันนี้
     Wait Until Page Contains                  ชำระครบ
     Wait Until Page Contains                  ชำระบางส่วน
     Capture Page Screenshot

TC_009_Test Filter Data By Sent Date & Range Date To Day
     [Documentation]   ทดสอบกรองข้อมูลจาก วันที่ส่งสินค้า และเลือกระยะเวลาวันนี้
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_SELL_ORDER}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_SELL_ORDER_SENT_DATE}
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_LENGTH}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_LENGTH_TODAY}
     Wait Until Page Contains                  วันนี้
     Capture Page Screenshot

TC_010_Test Filter Data By Shipping Date & Range Date To Day
     [Documentation]   ทดสอบกรองข้อมูลจาก วันที่โอนสินค้า และเลือกระยะเวลาวันนี้
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_SELL_ORDER}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_SELL_ORDER_SHIPPING_DATE}
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_LENGTH}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_LENGTH_TODAY}
     Wait Until Page Contains                  สำเร็จ
     Capture Page Screenshot

TC_011_Test Filter Data By Date & Range Date This Week
     [Documentation]   ทดสอบกรองข้อมูลจาก วันที่ และเลือกระยะเวลาสัปดาห์นี้
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_SELL_ORDER}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_SELL_ORDER_DATE}
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_LENGTH}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_LENGTH_THIS_WEEK}
     Wait Until Page Contains                  วันนี้
     Capture Page Screenshot

TC_012_Test Filter Data By Payment Date & Range Date This Week
     [Documentation]   ทดสอบกรองข้อมูลจาก วันที่ชำระเงิน และเลือกระยะเวลาสัปดาห์นี้
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_SELL_ORDER}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_SELL_ORDER_PAYMENT_DATE}
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_LENGTH}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_LENGTH_THIS_WEEK}
     Wait Until Page Contains                  วันนี้
     Wait Until Page Contains                  ชำระครบ
     Wait Until Page Contains                  ชำระบางส่วน
     Capture Page Screenshot

TC_013_Test Filter Data By Sent Date & Range Date This Week
     [Documentation]   ทดสอบกรองข้อมูลจาก วันที่ส่งสินค้า และเลือกระยะเวลาสัปดาห์นี้
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_SELL_ORDER}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_SELL_ORDER_SENT_DATE}
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_LENGTH}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_LENGTH_THIS_WEEK}
     Wait Until Page Contains                  วันนี้
     Capture Page Screenshot

TC_014_Test Filter Data By Shipping Date & Range Date This Week
     [Documentation]   ทดสอบกรองข้อมูลจาก วันที่โอนสินค้า และเลือกระยะเวลาสัปดาห์นี้
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_SELL_ORDER}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_SELL_ORDER_SHIPPING_DATE}
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_LENGTH}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_LENGTH_THIS_WEEK}
     Wait Until Page Contains                  วันนี้
     Wait Until Page Contains                  สำเร็จ
     Capture Page Screenshot

TC_015_Test Filter Data By Date & Range Date This Month
     [Documentation]   ทดสอบกรองข้อมูลจาก วันที่ และเลือกระยะเวลาเดือนนี้
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_SELL_ORDER}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_SELL_ORDER_DATE}
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_LENGTH}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_LENGTH_THIS_MONTH}
     Wait Until Page Contains                  วันนี้
     Capture Page Screenshot

TC_016_Test Filter Data By Payment Date & Range Date This Month
     [Documentation]   ทดสอบกรองข้อมูลจาก วันที่ชำระเงิน และเลือกระยะเวลาเดือนนี้
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_SELL_ORDER}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_SELL_ORDER_PAYMENT_DATE}
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_LENGTH}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_LENGTH_THIS_MONTH}
     Wait Until Page Contains                  วันนี้
     Wait Until Page Contains                  ชำระครบ
     Wait Until Page Contains                  ชำระบางส่วน
     Capture Page Screenshot

TC_017_Test Filter Data By Sent Date & Range Date This Month
     [Documentation]   ทดสอบกรองข้อมูลจาก วันที่ส่งสินค้า และเลือกระยะเวลาเดือนนี้
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_SELL_ORDER}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_SELL_ORDER_SENT_DATE}
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_LENGTH}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_LENGTH_THIS_MONTH}
     Wait Until Page Contains                  วันนี้
     Capture Page Screenshot

TC_018_Test Filter Data By Shipping Date & Range Date This Month
     [Documentation]   ทดสอบกรองข้อมูลจาก วันที่โอนสินค้า และเลือกระยะเวลาเดือนนี้
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_SELL_ORDER}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_SELL_ORDER_SHIPPING_DATE}
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_LENGTH}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_LENGTH_THIS_MONTH}
     Wait Until Page Contains                  วันนี้
     Wait Until Page Contains                  สำเร็จ
     Capture Page Screenshot
     
TC_019_Test Filter Data By Date & Range Date Past 1 Month
     [Documentation]   ทดสอบกรองข้อมูลจาก วันที่ และเลือกระยะเวลาย้อนหลัง 1 เดือน
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_SELL_ORDER}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_SELL_ORDER_DATE}
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_LENGTH}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_LENGTH_PAST_ONE_MONTH}
     Wait Until Page Contains                  วันนี้
     Capture Page Screenshot

TC_020_Test Filter Data By Payment Date & Range Date Past 1 Month
     [Documentation]   ทดสอบกรองข้อมูลจาก วันที่ชำระเงิน และเลือกระยะเวลาย้อนหลัง 1 เดือน
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_SELL_ORDER}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_SELL_ORDER_PAYMENT_DATE}
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_LENGTH}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_LENGTH_PAST_ONE_MONTH}
     Wait Until Page Contains                  วันนี้
     Wait Until Page Contains                  ชำระครบ
     Wait Until Page Contains                  ชำระบางส่วน
     Capture Page Screenshot

TC_021_Test Filter Data By Sent Date & Range Date Past 1 Month
     [Documentation]   ทดสอบกรองข้อมูลจาก วันที่ส่งสินค้า และเลือกระยะเวลาย้อนหลัง 1 เดือน
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_SELL_ORDER}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_SELL_ORDER_SENT_DATE}
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_LENGTH}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_LENGTH_PAST_ONE_MONTH}
     Wait Until Page Contains                  วันนี้
     Wait Until Page Contains                  สำเร็จ
     Capture Page Screenshot

TC_022_Test Filter Data By Shipping Date & Range Date Past 1 Month
     [Documentation]   ทดสอบกรองข้อมูลจาก วันที่โอนสินค้า และเลือกระยะเวลาย้อนหลัง 1 เดือน
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_SELL_ORDER}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_SELL_ORDER_SHIPPING_DATE}
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_LENGTH}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_LENGTH_PAST_ONE_MONTH}
     Wait Until Page Contains                  วันนี้
     Wait Until Page Contains                  สำเร็จ
     Capture Page Screenshot

TC_023_Test Filter Data By Date & Range Date Past 3 Month
     [Documentation]   ทดสอบกรองข้อมูลจาก วันที่ และเลือกระยะเวลาย้อนหลัง 3 เดือน
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_SELL_ORDER}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_SELL_ORDER_DATE}
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_LENGTH}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_LENGTH_PAST_THREE_MONTH}
     Wait Until Page Contains                  วันนี้
     Capture Page Screenshot

TC_024_Test Filter Data By Payment Date & Range Date Past 3 Month
     [Documentation]   ทดสอบกรองข้อมูลจาก วันที่ชำระเงิน และเลือกระยะเวลาย้อนหลัง 3 เดือน
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_SELL_ORDER}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_SELL_ORDER_PAYMENT_DATE}
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_LENGTH}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_LENGTH_PAST_THREE_MONTH}
     Wait Until Page Contains                  ชำระครบ
     Wait Until Page Contains                  ชำระบางส่วน
     Capture Page Screenshot

TC_025_Test Filter Data By Sent Date & Range Date Past 3 Month
     [Documentation]   ทดสอบกรองข้อมูลจาก วันที่ส่งสินค้า และเลือกระยะเวลาย้อนหลัง 3 เดือน
     Sleep                                     10s
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_SELL_ORDER}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_SELL_ORDER_SENT_DATE}
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_LENGTH}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_LENGTH_PAST_THREE_MONTH}
     Wait Until Page Contains                  วันนี้
     Wait Until Page Contains                  สำเร็จ
     Capture Page Screenshot

TC_026_Test Filter Data By Shipping Date & Range Date Past 3 Month
     [Documentation]   ทดสอบกรองข้อมูลจาก วันที่โอนสินค้า และเลือกระยะเวลาย้อนหลัง 3 เดือน
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_SELL_ORDER}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_SELL_ORDER_SHIPPING_DATE}
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_LENGTH}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_LENGTH_PAST_THREE_MONTH}
     Wait Until Page Contains                  วันนี้
     Wait Until Page Contains                  สำเร็จ
     Capture Page Screenshot

TC_027_Test Filter Data By Date & Range Date Custom
     [Documentation]   ทดสอบกรองข้อมูลจาก วันที่ และเลือกระยะเวลาแบบกำหนดเอง
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_SELL_ORDER}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_SELL_ORDER_DATE}
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_LENGTH}
     Click Element When Ready                  ${XPATH_FIELD_DATE_LENGTH_CUSTOM_START_DATE}
     Click Element When Ready                  ${XPATH_SAMPLE_CHOOSE_DATE_LENGTH_CUSTOM_START_DATE}
     Click Element When Ready                  ${XPATH_FIELD_DATE_LENGTH_CUSTOM_END_DATE}
     Click Element When Ready                  ${XPATH_SAMPLE_CHOOSE_DATE_LENGTH_CUSTOM_END_DATE}                 
     Click Element When Ready                  ${XPATH_BUTTON_OK_DATE_LENGTH_CUSTOM}
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 6
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 8
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 3
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 4
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 1
     Capture Page Screenshot

TC_028_Test Filter Data By Payment Date & Range Date Custom
     [Documentation]   ทดสอบกรองข้อมูลจาก วันที่ชำระเงิน และเลือกระยะเวลาแบบกำหนดเอง
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_SELL_ORDER}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_SELL_ORDER_PAYMENT_DATE}
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_LENGTH}
     Click Element When Ready                  ${XPATH_FIELD_DATE_LENGTH_CUSTOM_START_DATE}
     Click Element When Ready                  ${XPATH_SAMPLE_CHOOSE_DATE_LENGTH_CUSTOM_START_DATE}
     Click Element When Ready                  ${XPATH_FIELD_DATE_LENGTH_CUSTOM_END_DATE}
     Click Element When Ready                  ${XPATH_SAMPLE_CHOOSE_DATE_LENGTH_CUSTOM_END_DATE}                 
     Click Element When Ready                  ${XPATH_BUTTON_OK_DATE_LENGTH_CUSTOM}
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 8
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 3
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 4
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 1
     Capture Page Screenshot

TC_029_Test Filter Data By Sent Date & Range Date Custom
     [Documentation]   ทดสอบกรองข้อมูลจาก วันที่ส่งสินค้า และเลือกระยะเวลาแบบกำหนดเอง
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_SELL_ORDER}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_SELL_ORDER_SENT_DATE}
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_LENGTH}
     Click Element When Ready                  ${XPATH_FIELD_DATE_LENGTH_CUSTOM_START_DATE}
     Click Element When Ready                  ${XPATH_SAMPLE_CHOOSE_DATE_LENGTH_CUSTOM_START_DATE}
     Click Element When Ready                  ${XPATH_FIELD_DATE_LENGTH_CUSTOM_END_DATE}
     Click Element When Ready                  ${XPATH_SAMPLE_CHOOSE_DATE_LENGTH_CUSTOM_END_DATE}                 
     Click Element When Ready                  ${XPATH_BUTTON_OK_DATE_LENGTH_CUSTOM}
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 1
     Capture Page Screenshot

TC_030_Test Filter Data By Shipping Date & Range Date Custom
     [Documentation]   ทดสอบกรองข้อมูลจาก วันที่โอนสินค้า และเลือกระยะเวลาแบบกำหนดเอง
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_SELL_ORDER}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_SELL_ORDER_SHIPPING_DATE}
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_LENGTH}
     Click Element When Ready                  ${XPATH_FIELD_DATE_LENGTH_CUSTOM_START_DATE}
     Click Element When Ready                  ${XPATH_SAMPLE_CHOOSE_DATE_LENGTH_CUSTOM_START_DATE}
     Click Element When Ready                  ${XPATH_FIELD_DATE_LENGTH_CUSTOM_END_DATE}
     Click Element When Ready                  ${XPATH_SAMPLE_CHOOSE_DATE_LENGTH_CUSTOM_END_DATE}                 
     Click Element When Ready                  ${XPATH_BUTTON_OK_DATE_LENGTH_CUSTOM}
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 8
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 3
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 4
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 1
     Capture Page Screenshot

TC_031_Test Click Filter Search Button
     [Documentation]   ทดสอบกดปุ่มตัวกรอง
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_SELL_ORDER}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_SELL_ORDER_DATE}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_LENGTH}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_LENGTH_PAST_THREE_MONTH}
     Click Element When Ready                  ${XPATH_BUTTON_OPEN_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_OPEN_MODAL_FILTER_SEARCH_SELL_ORDER}
     Wait Until Page Contains                  รอโอนสินค้า
     Wait Until Page Contains                  สำเร็จ
     Wait Until Page Contains                  โอนบางส่วน
     Wait Until Page Contains                  ยกเลิก
     Wait Until Page Contains                  กำลังส่งสินค้า
     Wait Until Page Contains                  ยกเลิกการส่งสินค้า
     Wait Until Page Contains                  รอการชำระเงิน
     Wait Until Page Contains                  ชำระบางส่วน
     Wait Until Page Contains                  แสดงเฉพาะรายการเก็บเงินปลายทาง
     Wait Until Page Contains                  แสดงรายการที่ถูกซ่อน
     Wait Until Element Is Visible             ${XPATH_BUTTON_OPEN_MODAL_FILTER_SEARCH_SELL_ORDER}
     Wait Until Element Is Visible             ${XPATH_CHECKBOX_WAIT_SHIPPING_FILTER_SEARCH_SELL_ORDER}
     Wait Until Element Is Visible             ${XPATH_CHECKBOX_COMPLETE_SHIPPING_FILTER_SEARCH_SELL_ORDER}
     Wait Until Element Is Visible             ${XPATH_DROPDOWN_WAREHOUSE_FILTER_SEARCH_SELL_ORDER}
     Wait Until Element Is Visible             ${XPATH_DROPDOWN_SEARCH_USER_FILTER_SEARCH_SELL_ORDER}
     Wait Until Element Is Visible             ${XPATH_FIELD_SEARCH_AGENT_NAME_FILTER_SEARCH_SELL_ORDER}
     Capture Page Screenshot

TC_032_Test Click Check Box Filter By Tranfer Status And Click Search Button
     [Documentation]   ทดสอบเลือก check box กรองข้อมูลจากสถานะการโอนสินค้า แล้วกดปุ่มค้นหา
     Click Element When Ready                  ${XPATH_CHECKBOX_WAIT_SHIPPING_FILTER_SEARCH_SELL_ORDER}
     Click Element When Ready                  ${XPATH_BUTTON_SEARCH_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     3s
     Wait Until Page Contains                  รอโอน
     Capture Page Screenshot

TC_033_Test Click Check Box Filter By Shipment Status And Click Search Button
     [Documentation]   ทดสอบเลือก check box กรองข้อมูลจากสถานะการจัดส่ง แล้วกดปุ่มค้นหา
     Click Element When Ready                  ${XPATH_BUTTON_OPEN_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     5s
     Click Element When Ready                  ${XPATH_CHECKBOX_WAIT_SHIPPING_FILTER_SEARCH_SELL_ORDER}
     Click Element When Ready                  ${XPATH_CHECKBOX_WAIT_SENT_FILTER_SEARCH_SELL_ORDER}
     Click Element When Ready                  ${XPATH_BUTTON_SEARCH_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     3s
     Wait Until Page Contains                  สร้างรายการขายได้ที่นี่
     Capture Page Screenshot

TC_034_Test Click Check Box Filter By Payment Status And Click Search Button
     [Documentation]   ทดสอบเลือก check box กรองข้อมูลจากสถานะการชำระเงิน แล้วกดปุ่มค้นหา
     Click Element When Ready                  ${XPATH_BUTTON_OPEN_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     5s
     Click Element When Ready                  ${XPATH_CHECKBOX_WAIT_SENT_FILTER_SEARCH_SELL_ORDER}
     Click Element When Ready                  ${XPATH_CHECKBOX_WAIT_PAY_FILTER_SEARCH_SELL_ORDER}
     Click Element When Ready                  ${XPATH_BUTTON_SEARCH_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     3s
     Wait Until Page Contains                  รอโอน
     Capture Page Screenshot

TC_035_Test Choose Data In Dropdown Filter By Warehouse And Click Search Button
     [Documentation]   ทดสอบกรองข้อมูลจากคลังสินค้า
     Click Element When Ready                  ${XPATH_BUTTON_OPEN_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     5s
     Click Element When Ready                  ${XPATH_CHECKBOX_WAIT_PAY_FILTER_SEARCH_SELL_ORDER}
     Click Element When Ready                  ${XPATH_DROPDOWN_WAREHOUSE_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_SAMPLE_WAREHOUSE_FROM_DROPDOWN_WAREHOUSE2}
     Click Element When Ready                  ${XPATH_BUTTON_SEARCH_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     3s
     Wait Until Page Contains                  สร้างรายการขายได้ที่นี่
     Capture Page Screenshot

TC_036_Test Input Data In Text Box Filter By Product Name (Patial) And Click Search Button
     [Documentation]   ทดสอบกรองข้อมูลจากชื่อสินค้าแบบ patial
     Click Element When Ready                  ${XPATH_BUTTON_OPEN_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     5s
     Click Element When Ready                  ${XPATH_DROPDOWN_WAREHOUSE_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_SAMPLE_WAREHOUSE_FROM_DROPDOWN_WAREHOUSE}
     Input Text To Element When Ready          ${XPATH_FIELD_SEARCH_PRODUCT_FILTER_SEARCH_SELL_ORDER}           P000
     Click Element When Ready                  ${XPATH_BUTTON_SEARCH_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     3s
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 6
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 8
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 3
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 4
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 1
     Capture Page Screenshot

TC_037_Test Input Data In Text Box Filter By Product Name (Full) And Click Search Button
     [Documentation]   ทดสอบกรองข้อมูลจากชื่อสินค้าแบบ full
     Click Element When Ready                  ${XPATH_BUTTON_OPEN_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     5s
     Clear Text When Ready                     ${XPATH_FIELD_SEARCH_PRODUCT_FILTER_SEARCH_SELL_ORDER}
     Input Text To Element When Ready          ${XPATH_FIELD_SEARCH_PRODUCT_FILTER_SEARCH_SELL_ORDER}           P0007
     Click Element When Ready                  ${XPATH_BUTTON_SEARCH_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     3s
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 8
     Capture Page Screenshot

TC_038_Test Input Data In Text Box Filter By Serial NO And Click Search Button
     [Documentation]   ทดสอบกรองข้อมูลจาก serial no
     Click Element When Ready                  ${XPATH_BUTTON_OPEN_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     5s
     Clear Text When Ready                     ${XPATH_FIELD_SEARCH_PRODUCT_FILTER_SEARCH_SELL_ORDER}
     Input Text To Element When Ready          ${XPATH_FIELD_SEARCH_SERIAL_NUMBER_FILTER_SEARCH_SELL_ORDER}           TX005678911
     Click Element When Ready                  ${XPATH_BUTTON_SEARCH_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     3s
     Wait Until Page Contains                  สร้างรายการขายได้ที่นี่
     Capture Page Screenshot

TC_039_Test Input Data In Text Box Filter By Amount And Click Search Button
     [Documentation]   ทดสอบกรองข้อมูลจาก มูลค่าเริ่มต้น จนถึงมูลค่า
     Click Element When Ready                  ${XPATH_BUTTON_OPEN_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     5s
     Clear Text When Ready                     ${XPATH_FIELD_SEARCH_SERIAL_NUMBER_FILTER_SEARCH_SELL_ORDER}
     Input Text To Element When Ready          ${XPATH_FIELD_SEARCH_PRICE_START_FILTER_SEARCH_SELL_ORDER}           60000
     Input Text To Element When Ready          ${XPATH_FIELD_SEARCH_PRICE_END_FILTER_SEARCH_SELL_ORDER}             60000
     Click Element When Ready                  ${XPATH_BUTTON_SEARCH_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     3s
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 6
     Capture Page Screenshot

TC_040_Test Click Check Box Show Only COD Orders And Click Search Button
     [Documentation]   ทดสอบเลือก check box แสดงเฉพาะรายการเก็บเงินปลายทาง แล้วกดปุ่มค้นหา
     Click Element When Ready                  ${XPATH_BUTTON_OPEN_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     5s
     Clear Text When Ready                     ${XPATH_FIELD_SEARCH_PRICE_START_FILTER_SEARCH_SELL_ORDER}      
     Clear Text When Ready                     ${XPATH_FIELD_SEARCH_PRICE_END_FILTER_SEARCH_SELL_ORDER}
     Click Element When Ready                  ${XPATH_CHECKBOX_SHOW_ONLY_COD_FILTER_SEARCH_SELL_ORDER}       
     Click Element When Ready                  ${XPATH_BUTTON_SEARCH_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     3s
     Wait Until Page Contains                  สร้างรายการขายได้ที่นี่
     Capture Page Screenshot

TC_041_Test Click Check Box Show Hidden And Click Search Button
     [Documentation]   ทดสอบเลือก check box แสดงรายการที่ถูกซ่อน แล้วกดปุ่มค้นหา
     Click Element When Ready                  ${XPATH_BUTTON_OPEN_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     5s
     Click Element When Ready                  ${XPATH_CHECKBOX_SHOW_ONLY_COD_FILTER_SEARCH_SELL_ORDER}  
     Click Element When Ready                  ${XPATH_CHECKBOX_SHOW_HIDDEN_SO_FILTER_SEARCH_SELL_ORDER}      
     Click Element When Ready                  ${XPATH_BUTTON_SEARCH_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     3s
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 6
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 8
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 3
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 4
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 1
     Capture Page Screenshot

TC_042_Test Input Data In Text Box Filter By Agent Name (Patial) And Click Search Button
     [Documentation]   ทดสอบกรองข้อมูลจากชื่อตัวแทนแบบ patial
     Click Element When Ready                  ${XPATH_BUTTON_OPEN_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     5s
     Click Element When Ready                  ${XPATH_CHECKBOX_SHOW_HIDDEN_SO_FILTER_SEARCH_SELL_ORDER}
     Input Text To Element When Ready          ${XPATH_FIELD_SEARCH_AGENT_NAME_FILTER_SEARCH_SELL_ORDER}           ข้อมูลตัวอย่าง
     Click Element When Ready                  ${XPATH_BUTTON_SEARCH_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     3s
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 6
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 8
     Capture Page Screenshot

TC_043_Test Input Data In Text Box Filter By Agent Name (Full) And Click Search Button
     [Documentation]   ทดสอบกรองข้อมูลจากชื่อตัวแทนแบบ full
     Click Element When Ready                  ${XPATH_BUTTON_OPEN_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     5s
     Clear Text When Ready                     ${XPATH_FIELD_SEARCH_AGENT_NAME_FILTER_SEARCH_SELL_ORDER}
     Input Text To Element When Ready          ${XPATH_FIELD_SEARCH_AGENT_NAME_FILTER_SEARCH_SELL_ORDER}           ข้อมูลตัวอย่าง 1
     Click Element When Ready                  ${XPATH_BUTTON_SEARCH_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     3s
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 6
     Wait Until Page Contains                  ข้อมูลตัวอย่าง 8
     Capture Page Screenshot


TC_044_Test Choose Data In Dropdown Filter By User And Click Search Button
     [Documentation]   ทดสอบกรองข้อมูลจาก dropdown ผู้ใช้งาน
     Click Element When Ready                  ${XPATH_BUTTON_OPEN_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     5s
     Clear Text When Ready                     ${XPATH_FIELD_SEARCH_AGENT_NAME_FILTER_SEARCH_SELL_ORDER}
     Click Element When Ready                  ${XPATH_DROPDOWN_SEARCH_USER_FILTER_SEARCH_SELL_ORDER}
     Click Element When Ready                  ${XPATH_BUTTON_SEARCH_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     3s
     Wait Until Page Contains                  SO-202209003
     Wait Until Page Contains                  SO-202209002
     Capture Page Screenshot

TC_045_Test Choose All Data In Filter And Click Search Button
     [Documentation]   ทดสอบกรองข้อมูลจาก filter ทั้งหมด
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_SELL_ORDER}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_SELL_ORDER_DATE}
     Click Element When Ready                  ${XPATH_DROPDOWN_DATE_LENGTH}
     Click Element When Ready                  ${XPATH_SAMPLE_DROPDOWN_DATE_LENGTH_TODAY}
     Click Element When Ready                  ${XPATH_BUTTON_OPEN_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_BUTTON_OPEN_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     3s
     Click Element When Ready                  ${XPATH_DROPDOWN_SEARCH_USER_FILTER_SEARCH_SELL_ORDER}
     Click Element When Ready                  ${XPATH_SAMPLE_USER_FROM_DROPDOWN_SEARCH_AGENT}
     Click Element When Ready                  ${XPATH_CHECKBOX_WAIT_SHIPPING_FILTER_SEARCH_SELL_ORDER}
     Click Element When Ready                  ${XPATH_CHECKBOX_WAIT_SENT_FILTER_SEARCH_SELL_ORDER}
     Click Element When Ready                  ${XPATH_CHECKBOX_PARTIAL_PAY_FILTER_SEARCH_SELL_ORDER}
     Click Element When Ready                  ${XPATH_BUTTON_SEARCH_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     3s
     Wait Until Page Contains                  รอโอน
     Wait Until Page Contains                  ชำระบางส่วน
     Wait Until Page Contains                  30,000
     Capture Page Screenshot

TC_046_Test Choose All Data In Filter, Slide Switch Remember This Filter And Then Close Browser And Reopen Browser
     [Documentation]   ทดสอบเลือกข้อมูลใน filter แล้ว slide เปิด switch จำตัวกรองนี้ แล้วกดปิดหน้า zort แล้วเปิดใหม่
     Click Element When Ready                  ${XPATH_BUTTON_OPEN_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     5s
     Click Element When Ready                  ${XPATH_SWITCH_REMEMBER_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     30s
     Click Element When Ready                  ${XPATH_BUTTON_SEARCH_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     3s
     Wait Until Page Contains                  รอโอน
     Wait Until Page Contains                  ชำระบางส่วน
     Wait Until Page Contains                  30,000
     Close All Browsers
     Open Browser      ${URL}      ${BROWSER}
     Set Window Size    1920   1080            
     Input Text To Element When Ready          ${XPATH_FIELD_USERNAME}       ${EMAIL}           
     Input Text To Element When Ready          ${XPATH_FIELD_PASSWORD}       ${PASSWORD}     
     Click Element When Ready                  ${XPATH_BUTTON_LOGIN}
     Sleep                                     10s
     ${popup1}  Run Keyword And Return Status    Element Should Be Visible           ${XPATH_CLOSE_POPUP_EXPIRE}
       Log  ${popup1}
       Run Keyword If    '${popup1}'=='True'       Click Element                     ${XPATH_CLOSE_POPUP_EXPIRE}
       Run Keyword If    '${popup1}'=='False'      Pop Up Not Avalible
     ${popup2}  Run Keyword And Return Status    Element Should Be Visible           ${XPATH_CLOSE_POPUP}
       Log  ${popup2}
       Run Keyword If    '${popup2}'=='True'       Click Element                     ${XPATH_CLOSE_POPUP}
       Run Keyword If    '${popup2}'=='False'      Pop Up Not Avalible
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU}
     Click Element When Ready                  ${XPATH_SELL_ORDER_MENU_SELL_ORDER}
     Sleep                                     3s
     Wait Until Page Contains                  รอโอน
     Wait Until Page Contains                  ชำระบางส่วน
     Wait Until Page Contains                  30,000
     


TC_047_Test Choose All Data In Filter And Click Clear Button In Filter
     [Documentation]   ทดสอบกดปุ่มเคลียร์ในตัวกรอง
     Click Element When Ready                  ${XPATH_BUTTON_OPEN_MODAL_FILTER_SEARCH_SELL_ORDER}
     Click Element When Ready                  ${XPATH_BUTTON_CLEAR_FILTER_SETTING_SEARCH_SELL_ORDER}
     Click Element When Ready                  ${XPATH_BUTTON_SEARCH_MODAL_FILTER_SEARCH_SELL_ORDER}
     Sleep                                     3s
     Wait Until Page Contains                  6,000
     Wait Until Page Contains                  30,000
     Wait Until Page Contains                  28,500
     Wait Until Page Contains                  27,000